$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gId = "";

    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút Lưu lại
    $("#btn-save").on("click", onBtnSaveClick);

    //Gán sự kiện cho nút Hủy bỏ
    $("#btn-cancel").on("click", onBtnCancelClick);

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        const vUrlParams = new URLSearchParams(window.location.search);
        gId = vUrlParams.get("id");

        $(".select2bs4").select2({
            theme: "bootstrap4"
        });

        if (gId != "" || gId != null) {
            $.ajax({
                url: "http://localhost:8080/provinces/" + gId,
                method: "GET",
                success: function (res) {
                    loadDetailData(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }

    //Hàm xử lý khi ấn nút Lưu lại
    function onBtnSaveClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vProvince = {
                name: "",
                code: ""
            }
            //Lấy dữ liệu
            getProvinceData(vProvince);
            //Kiểm tra dữ liệu
            var vIsCheck = validateProvinceData(vProvince);

            if (vIsCheck) {
                $.ajax({
                    url: "http://localhost:8080/provinces/" + gId,
                    method: "PUT",
                    headers: {
                        Authorization: "Token " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vProvince),
                    success: function (res) {
                        window.location.href = "provinces.html";
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            $("#modal-login").modal("show");
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }
    }

    //Hàm xử lý khi ấn nút Hủy bỏ
    function onBtnCancelClick() {
        "use strict";
        window.history.back();
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm xử lý hiển thị chi tiết Tỉnh thành phố
    function loadDetailData(paramData) {
        "use strict";
        $("#inp-name").val(paramData.name);
        $("#inp-code").val(paramData.code);
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm lấy dữ liệu Tỉnh thành phố
    function getProvinceData(paramProvince) {
        paramProvince.name = $("#inp-name").val().trim();
        paramProvince.code = $("#inp-code").val().trim();
    }

    //Hàm lấy kiểm tra dữ liệu Tỉnh thành phố
    function validateProvinceData(paramProvince) {
        "use strict";
        if (paramProvince.name == "") {
            gToast.fire({
                icon: "warning",
                title: "Nhập tên Tỉnh thành phố."
            });
            return false;
        }
        if (paramProvince.code == "") {
            gToast.fire({
                icon: "warning",
                title: "Nhập mã Tỉnh thành phố."
            });
            return false;
        }
        return true;
    }
});