"use strict";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gTotalPages = 0;
    var gPage = 0;
    var gPageSize = 10;
    var gId = "";
    const gTable = $("#tbl-design-units");
    const gNameCol = ["action", "id", "name", "address", "phone", "phone2", "fax", "email", "website"];
    const gACTION_COL = 0;
    const gID_COL = 1;
    const gNAME_COL = 2;
    const gADDRESS_COL = 3;
    const gPHONE_COL = 4;
    const gPHONE2_COL = 5;
    const gFAX_COL = 6;
    const gEMAIL_COL = 7;
    const gWEBSITE_COL = 8;
    var gDataTable = gTable.DataTable({
        columns: [
            { data: gNameCol[gACTION_COL] },
            { data: gNameCol[gID_COL] },
            { data: gNameCol[gNAME_COL] },
            { data: gNameCol[gADDRESS_COL] },
            { data: gNameCol[gPHONE_COL] },
            { data: gNameCol[gPHONE2_COL] },
            { data: gNameCol[gFAX_COL] },
            { data: gNameCol[gEMAIL_COL] },
            { data: gNameCol[gWEBSITE_COL] }
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: "<a class='btn detail-design-unit' title='Chi tiết'><i class='text-info fas fa-info'></i></a><a class='btn edit-design-unit' title='Sửa'><i class='text-primary fas fa-edit'></i></a><a class='btn delete-design-unit' title='Xóa'><i class='text-danger fas fa-trash'></i></a>",
                className: "text-nowrap"

            },
            {
                targets: gNAME_COL,
                className: "text-nowrap"
            },
            {
                targets: gADDRESS_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.address;
                    }
                    return "";
                },
                width: 300
            }
        ],
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        scrollY: 350,
        scrollX: true,
    });

    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút add
    $("#add-design-unit").on("click", function () {
        onAddDesignUnitClick();
    });

    //Gán sự kiện cho nút chi tiết
    $("#tbl-design-units tbody").on("click", ".detail-design-unit", function () {
        onDetailDesignUnitClick(this);
    });

    //Gán sự kiện cho nút edit
    $("#tbl-design-units tbody").on("click", ".edit-design-unit", function () {
        onEditDesignUnitClick(this);
    });

    //Gán sự kiện cho nút xóa
    $("#tbl-design-units tbody").on("click", ".delete-design-unit", function () {
        onDeleteDesignUnitClick(this);
    });

    //Gán sự kiện cho nút Xóa trên modal
    $("#btn-delete-confirm").on("click", onBtnDeleteConfirmClick);

    //Gán sự kiện cho các nút phân trang
    $("#number-page").on("keypress", function (event) {
        if (event.which == 13) {
            onNumberPageEnter();
        }
    });
    $("#sel-page-size").on("change", onChangePageSize);
    $("#start-page").on("click", onStartPageClick);
    $("#previous-page").on("click", onPreviousPageClick);
    $("#next-page").on("click", onNextPageClick);
    $("#end-page").on("click", onEndPageClick);

    //Điều chỉnh lại các cột table khi thay đổi kích thước window
    $(window).resize(function () {
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });


    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        loadDesignUnitDataToTable();
    }

    // Hàm xử lý khi click nút add
    function onAddDesignUnitClick() {
        "use strict";
        window.location.href = "add-design-unit.html"
    }

    // Hàm xử lý khi click nút chi tiết
    function onDetailDesignUnitClick(paramBtnDetail) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnDetail.closest("tr")).data().id;
        var token = getCookie("token");
        if (token) {
            var headers = {
                Authorization: "Token " + token
            };
            $.ajax({
                url: "http://localhost:8080/design-units/" + gId,
                method: "GET",
                headers: headers,
                success: function (res) {
                    loadDetailData(res);
                    $("#modal-detail").modal("show");
                }, error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }


    // Hàm xử lý khi click nút edit
    function onEditDesignUnitClick(paramBtnEdit) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnEdit.closest("tr")).data().id;
        var token = getCookie("token");
        if (token) {
            window.location.href = "edit-design-unit.html?id=" + gId;
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }
    }

    // Hàm xử lý khi click nút xóa
    function onDeleteDesignUnitClick(paramBtnDelete) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnDelete.closest("tr")).data().id;
        var token = getCookie("token");
        if (token) {
            $("#modal-delete").modal("show");
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }
    }

    // Hàm xử lý khi click nút xóa trên modal
    function onBtnDeleteConfirmClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: "http://localhost:8080/design-units/" + gId,
                method: "DELETE",
                headers: {
                    Authorization: "Token " + token
                },
                success: function (res) {
                    $("#modal-delete").modal("hide");
                    loadDesignUnitDataToTable();
                }, error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }
    }

    function onNumberPageEnter() {
        var vPage = $("#number-page").val();
        if (vPage < 1) {
            gPage = 0;
            loadDesignUnitDataToTable();
        } else if (vPage > gTotalPages && gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadDesignUnitDataToTable();
        } else {
            gPage = vPage - 1;
            loadDesignUnitDataToTable();
        }
    }

    function onChangePageSize() {
        gPage = 0;
        gPageSize = $("#sel-page-size").val();
        loadDesignUnitDataToTable();
    }

    //Hàm xử lý khi ấn nút Trang đầu
    function onStartPageClick() {
        if (gTotalPages > 0) {
            gPage = 0;
            loadDesignUnitDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang trước
    function onPreviousPageClick() {
        if (gTotalPages > 0 && gPage > 0) {
            gPage--;
            loadDesignUnitDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang sau
    function onNextPageClick() {
        if (gTotalPages > 0 && gPage < gTotalPages - 1) {
            gPage++;
            loadDesignUnitDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang cuối
    function onEndPageClick() {
        if (gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadDesignUnitDataToTable();
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm load dữ liệu customer vào bảng
    function loadDesignUnitDataToTable() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var headers = {
                Authorization: "Token " + token
            };
            $.ajax({
                url: "http://localhost:8080/design-units?page=" + gPage + "&size=" + gPageSize,
                method: "GET",
                headers: headers,
                success: function (res) {
                    var vDataTable = gTable.DataTable();
                    vDataTable.clear();
                    vDataTable.rows.add(res.content);
                    vDataTable.draw();
                    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
                    gTotalPages = res.totalPages;
                    $("#number-page").val(gPage + 1);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm xử lý hiển thị chi tiết thông tin đơn vị thiết kế
    function loadDetailData(paramData) {
        "use strict";
        $("#p-id").html(paramData.id);
        $("#p-name").html(paramData.name);
        $("#p-description").html(paramData.description);
        if (paramData.address != null) {
            $("#p-address").html(paramData.address.address);
        } else {
            $("#p-address").html("");
        }
        $("#p-phone").html(paramData.phone);
        $("#p-phone2").html(paramData.phone2);
        $("#p-fax").html(paramData.fax);
        $("#p-email").html(paramData.email);
        $("#p-website").html(paramData.website);
        $("#p-note").html(paramData.note);
    }
});