$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gAddressId = "";

    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút Lưu lại
    $("#btn-save").on("click", onBtnSaveClick);

    //Gán sự kiện cho nút Hủy bỏ
    $("#btn-cancel").on("click", onBtnCancelClick);

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";

        $(".select2bs4").select2({
            theme: "bootstrap4"
        });

        $.ajax({
            url: "http://localhost:8080/address-maps/all",
            method: "GET",
            async: false,
            success: function (res) {
                loadDataToSelectAddress(res);
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });
    }

    //Hàm xử lý khi ấn nút Lưu lại
    function onBtnSaveClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vInvestor = {
                name: "",
                description: "",
                phone: "",
                phone2: "",
                fax: "",
                email: "",
                website: "",
                note: ""
            }
            //Lấy dữ liệu
            getInvestorData(vInvestor);
            //Kiểm tra dữ liệu
            var vIsCheck = validateInvestorData(vInvestor);

            if (vIsCheck) {
                $.ajax({
                    url: "http://localhost:8080/investors" + "?addressId=" + gAddressId,
                    method: "POST",
                    headers: {
                        Authorization: "Token " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vInvestor),
                    success: function (res) {
                        window.location.href = "investors.html";
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            $("#modal-login").modal("show");
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đang nhập."
            });
        }
    }

    //Hàm xử lý khi ấn nút Hủy bỏ
    function onBtnCancelClick() {
        "use strict";
        window.history.back();
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm load dữ liệu vào Select Địa chỉ
    function loadDataToSelectAddress(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            console.log(bI);
            var bAddress = paramData[bI].address;
            console.log(bAddress);
            $("#sel-address").append($("<option>").val(bId).text(bAddress));
        }
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm lấy dữ liệu chủ đầu tư
    function getInvestorData(paramInvestor) {
        gAddressId = $("#sel-address").val();
        paramInvestor.name = $("#inp-name").val();
        paramInvestor.description = $("#inp-description").val();
        paramInvestor.phone = $("#inp-phone").val();
        paramInvestor.phone2 = $("#inp-phone2").val();
        paramInvestor.fax = $("#inp-fax").val();
        paramInvestor.email = $("#inp-email").val();
        paramInvestor.website = $("#inp-website").val();
        paramInvestor.note = $("#inp-note").val();
    }

    //Hàm lấy kiểm tra dữ liệu chủ đầu tư
    function validateInvestorData(paramInvestor) {
        "use strict";
        if (paramInvestor.name.trim() == "") {
            gToast.fire({
                icon: "warning",
                title: "Nhập tên chủ đầu tư."
            });
            return false;
        }
        return true;
    }
});