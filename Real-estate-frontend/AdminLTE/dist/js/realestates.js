"use strict";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gUserId = "";
    var gRole = "";
    var gSelRealestate = "1";
    var gTotalPages = 0;
    var gPage = 0;
    var gPageSize = 10;
    var gId = "";
    const gTable = $("#tbl-realestates");
    const gNameCol = ["action", "id", "address", "photo", "province", "district", "ward", "street", "project", "title", "type", "request", "customer", "price", "priceMin", "priceTime"];
    const gACTION_COL = 0;
    const gID_COL = 1;
    const gADDRESS_COL = 2;
    const gPHOTO_COL = 3;
    const gPROVINCE_COL = 4;
    const gDISTRICT_COL = 5;
    const gWARD_COL = 6;
    const gSTREET_COL = 7;
    const gPROJECT_COL = 8;
    const gTITLE_COL = 9;
    const gTYPE_COL = 10;
    const gREQUEST_COL = 11;
    const gCUSTOMER_COL = 12;
    const gPRICE_COL = 13;
    const gPRICE_MIN_COL = 14;
    const gPRICE_TIME_COL = 15;
    var gDataTable = gTable.DataTable({
        columns: [
            { data: gNameCol[gACTION_COL] },
            { data: gNameCol[gID_COL] },
            { data: gNameCol[gADDRESS_COL] },
            { data: gNameCol[gPHOTO_COL] },
            { data: gNameCol[gPROVINCE_COL] },
            { data: gNameCol[gDISTRICT_COL] },
            { data: gNameCol[gWARD_COL] },
            { data: gNameCol[gSTREET_COL] },
            { data: gNameCol[gPROJECT_COL] },
            { data: gNameCol[gTITLE_COL] },
            { data: gNameCol[gTYPE_COL] },
            { data: gNameCol[gREQUEST_COL] },
            { data: gNameCol[gCUSTOMER_COL] },
            { data: gNameCol[gPRICE_COL] },
            { data: gNameCol[gPRICE_MIN_COL] },
            { data: gNameCol[gPRICE_TIME_COL] }
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: "<a class='btn detail-realestate' title='Chi tiết'><i class='text-info fas fa-info'></i></a>",
                className: "text-nowrap",
                render: function (data, type, row, meta) {
                    var vRender = "";
                    var vApprove = "<a class='btn approve-realestate' title='Cho phép đăng'><i class='text-success fas fa-check-circle'></i></a>";
                    var vDetail = "<a class='btn detail-realestate' title='Chi tiết'><i class='text-info fas fa-info'></i></a>";
                    var vEdit = "<a class='btn edit-realestate' title='Sửa'><i class='text-primary fas fa-edit'></i></a>";
                    var vDelete = "<a class='btn delete-realestate' title='Xóa'><i class='text-danger fas fa-trash'></i></a>";
                    if (gRole == "1") {
                        vRender = vDetail + vEdit + vDelete;
                        if (gSelRealestate == "3") {
                            vRender = vApprove + vRender;
                        }
                    } else if (gRole == "2") {
                        if (gSelRealestate == "1") {
                            vRender = vDetail + vEdit;
                            if (gUserId == row["createdBy"]) {
                                vRender += vDelete;
                            }
                        } else if (gSelRealestate == "2") {
                            vRender = vDetail;
                            if (gUserId == row["createdBy"]) {
                                vRender += vEdit + vDelete;
                            }
                        } else if (gSelRealestate == "3") {
                            vRender = vApprove + vDetail + vEdit;
                        }
                    }
                    return vRender;
                }
            },
            {
                targets: gADDRESS_COL,
                width: 300
            },
            {
                targets: gPHOTO_COL,
                className: "text-center",
                width: 150,
                render: function (data, type) {
                    if (data != "" && data != null) {
                        var vPhotoNames = data.split(",");
                        var vImages = "<div class='carousel-item active'><img src='http://localhost:8080/download/realestate-photo/" + vPhotoNames[0] + "' style='height: 100px; object-fit: contain;'/></div>";
                        for (var bI = 1; bI < vPhotoNames.length; bI++) {
                            vImages += "<div class='carousel-item'><img src='http://localhost:8080/download/realestate-photo/" + vPhotoNames[bI] + "' style='height: 100px; object-fit: contain;'/></div>";
                        }
                        return "<div class='carousel slide' data-ride='carousel'><div class='carousel-inner'>" + vImages + "</div></div>";
                    }
                    return "";
                }
            },
            {
                targets: gPROVINCE_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.name;
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gDISTRICT_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.name;
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gWARD_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.name;
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gSTREET_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.name;
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gPROJECT_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.name;
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gTITLE_COL,
                width: 300
            },
            {
                targets: gTYPE_COL,
                render: function (data, type) {
                    if (data != null) {
                        return stringType(data);
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gREQUEST_COL,
                render: function (data, type) {
                    if (data != null) {
                        return stringRequest(data);
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gCUSTOMER_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.contactName;
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gPRICE_TIME_COL,
                render: function (data, type) {
                    if (data != null) {
                        return stringPriceTime(data);
                    }
                    return "";
                },
                className: "text-nowrap"
            }
        ],
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        scrollY: 350,
        scrollX: true,
    });

    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút add
    $("#add-realestate").on("click", function () {
        onAddRealestateClick();
    });

    $("#sel-realestate").on("change", onSelectRealestateChange);

    //Gán sự kiện cho nút cho phép đăng tin
    $("#tbl-realestates tbody").on("click", ".approve-realestate", function () {
        onApproveRealestateClick(this);
    });

    //Gán sự kiện cho nút chi tiết
    $("#tbl-realestates tbody").on("click", ".detail-realestate", function () {
        onDetailRealestateClick(this);
    });

    //Gán sự kiện cho nút edit
    $("#tbl-realestates tbody").on("click", ".edit-realestate", function () {
        onEditRealestateClick(this);
    });

    //Gán sự kiện cho nút xóa
    $("#tbl-realestates tbody").on("click", ".delete-realestate", function () {
        onDeleteRealestateClick(this);
    });

    //Gán sự kiện cho nút Xóa trên modal
    $("#btn-delete-confirm").on("click", onBtnDeleteConfirmClick);

    //Gán sự kiện cho các nút phân trang
    $("#number-page").on("keypress", function (event) {
        if (event.which == 13) {
            onNumberPageEnter();
        }
    });
    $("#sel-page-size").on("change", onChangePageSize);
    $("#start-page").on("click", onStartPageClick);
    $("#previous-page").on("click", onPreviousPageClick);
    $("#next-page").on("click", onNextPageClick);
    $("#end-page").on("click", onEndPageClick);

    //Điều chỉnh lại các cột table khi thay đổi kích thước window
    $(window).resize(function () {
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });


    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var headers = {
                Authorization: "Token " + token
            };
            $.ajax({
                url: "http://localhost:8080/admin-seller/users/me",
                method: "GET",
                headers: headers,
                success: function (res) {
                    gUserId = res.id;
                    gRole = res.role;
                    loadRealestateDataToTable();
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }

    // Hàm xử lý khi click nút add
    function onAddRealestateClick() {
        "use strict";
        window.location.href = "add-realestate.html"
    }

    function onSelectRealestateChange() {
        gPage = 0;
        gSelRealestate = $("#sel-realestate").val();
        loadRealestateDataToTable();
    }

    // Hàm xử lý khi click nút cho phép đăng tin
    function onApproveRealestateClick(paramBtnApprove) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnApprove.closest("tr")).data().id;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: "http://localhost:8080/realestates/" + gId,
                method: "GET",
                success: function (res) {
                    var vProvinceId = "-1";
                    var vDistrictId = "-1";
                    var vWardId = "-1";
                    var vStreetId = "-1";
                    var vProjectId = "-1";
                    var vCustomerId = "-1";
                    if (res.province != null)
                        vProvinceId = res.province.id;
                    if (res.district != null)
                        vDistrictId = res.district.id;
                    if (res.ward != null)
                        vWardId = res.ward.id;
                    if (res.street != null)
                        vStreetId = res.street.id;
                    if (res.project != null)
                        vProjectId = res.project.id;
                    if (res.customer != null)
                        vCustomerId = res.customer.id;
                    $.ajax({
                        url: "http://localhost:8080/realestates/" + gId + "?provinceId=" + vProvinceId + "&districtId=" + vDistrictId + "&wardId=" + vWardId + "&streetId=" + vStreetId + "&projectId=" + vProjectId + "&customerId=" + vCustomerId,
                        method: "PUT",
                        headers: {
                            Authorization: "Token " + token,
                            "Content-Type": "application/json"
                        },
                        data: JSON.stringify(res),
                        success: function (response) {
                            loadRealestateDataToTable();
                        },
                        error: function (err) {
                            console.log(err.responseText);
                        }
                    });
                }, error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }
    }

    // Hàm xử lý khi click nút chi tiết
    function onDetailRealestateClick(paramBtnDetail) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnDetail.closest("tr")).data().id;
        $.ajax({
            url: "http://localhost:8080/realestates/" + gId,
            method: "GET",
            success: function (res) {
                loadDetailData(res);
                $("#modal-detail").modal("show");
            }, error: function (err) {
                console.log(err.responseText);
            }
        });
    }


    // Hàm xử lý khi click nút edit
    function onEditRealestateClick(paramBtnEdit) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnEdit.closest("tr")).data().id;
        if (checkEditable(gId)) {
            window.location.href = "edit-realestate.html?id=" + gId;
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập hoặc không có quyền chỉnh sửa."
            });
        }
    }

    // Hàm xử lý khi click nút xóa
    function onDeleteRealestateClick(paramBtnDelete) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnDelete.closest("tr")).data().id;
        if (checkDeleteable(gId)) {
            $("#modal-delete").modal("show");
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập hoặc không có quyền xóa."
            });
        }
    }

    // Hàm xử lý khi click nút xóa trên modal
    function onBtnDeleteConfirmClick() {
        "use strict";
        if (checkDeleteable(gId)) {
            var token = getCookie("token");
            $.ajax({
                url: "http://localhost:8080/realestates/" + gId,
                method: "DELETE",
                headers: {
                    Authorization: "Token " + token
                },
                success: function (res) {
                    $("#modal-delete").modal("hide");
                    loadRealestateDataToTable();
                }, error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập hoặc không có quyền xóa."
            });
        }
    }

    function onNumberPageEnter() {
        var vPage = $("#number-page").val();
        if (vPage < 1) {
            gPage = 0;
            loadRealestateDataToTable();
        } else if (vPage > gTotalPages && gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadRealestateDataToTable();
        } else {
            gPage = vPage - 1;
            loadRealestateDataToTable();
        }
    }

    function onChangePageSize() {
        gPage = 0;
        gPageSize = $("#sel-page-size").val();
        loadRealestateDataToTable();
    }

    //Hàm xử lý khi ấn nút Trang đầu
    function onStartPageClick() {
        if (gTotalPages > 0) {
            gPage = 0;
            loadRealestateDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang trước
    function onPreviousPageClick() {
        if (gTotalPages > 0 && gPage > 0) {
            gPage--;
            loadRealestateDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang sau
    function onNextPageClick() {
        if (gTotalPages > 0 && gPage < gTotalPages - 1) {
            gPage++;
            loadRealestateDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang cuối
    function onEndPageClick() {
        if (gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadRealestateDataToTable();
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm load dữ liệu customer vào bảng
    function loadRealestateDataToTable() {
        "use strict";
        var vUrl = "";
        switch (gSelRealestate) {
            case "1":
                vUrl = "http://localhost:8080/realestates?page=" + gPage + "&size=" + gPageSize;
                break;
            case "2":
                vUrl = "http://localhost:8080/realestates/posts?page=" + gPage + "&size=" + gPageSize;
                break;
            case "3":
                vUrl = "http://localhost:8080/realestates/approve?page=" + gPage + "&size=" + gPageSize;
                break;
            default:
                vUrl = "http://localhost:8080/realestates?page=" + gPage + "&size=" + gPageSize;
        }
        var token = getCookie("token");
        if (token) {
            var headers = {
                Authorization: "Token " + token
            };
            $.ajax({
                url: vUrl,
                method: "GET",
                headers: headers,
                success: function (res) {
                    var vDataTable = gTable.DataTable();
                    vDataTable.clear();
                    vDataTable.rows.add(res.content);
                    vDataTable.draw();
                    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
                    gTotalPages = res.totalPages;
                    $("#number-page").val(gPage + 1);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }

    //Hàm kiểm tra được phép chỉnh sửa
    function checkEditable(paramId) {
        "use strict";
        var token = getCookie("token");
        var vIsCheck = false;
        if (token) {
            var headers = {
                Authorization: "Token " + token
            };
            $.ajax({
                url: "http://localhost:8080/realestates/" + paramId + "/editable",
                method: "GET",
                headers: headers,
                async: false,
                success: function (res) {
                    vIsCheck = res;
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
        return vIsCheck;
    }

    //Hàm kiểm tra được phép xóa
    function checkDeleteable(paramId) {
        "use strict";
        var token = getCookie("token");
        var vIsCheck = false;
        if (token) {
            var headers = {
                Authorization: "Token " + token
            };
            $.ajax({
                url: "http://localhost:8080/realestates/" + paramId + "/deleteable",
                method: "GET",
                headers: headers,
                async: false,
                success: function (res) {
                    vIsCheck = res;
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
        return vIsCheck;
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm xử lý hiển thị chi tiết bất động sản
    function loadDetailData(paramData) {
        "use strict";
        $("#p-id").html(paramData.id);
        $("#p-created-by").html(paramData.createdBy);
        $("#p-updated-by").html(paramData.updatedBy);
        $("#p-created-at").html(paramData.createdAt);
        $("#p-updated-at").html(paramData.updatedAt);
        $("#carousel-images").html("");
        $("#carouselIndicators").prop("hidden", true);
        if (paramData.photo != "" && paramData.photo != null) {
            var vListPhotos = paramData.photo.split(",");
            $("#carousel-images").append(
                "<div class='carousel-item active'>"
                + "<img src='http://localhost:8080/download/realestate-photo/" + vListPhotos[0] + "' class='d-block w-100' style='height: 500px; object-fit: contain;' alt='...'>"
                + "</div>"
            );
            for (var bI = 1; bI < vListPhotos.length; bI++) {
                $("#carousel-images").append(
                    "<div class='carousel-item'>"
                    + "<img src='http://localhost:8080/download/realestate-photo/" + vListPhotos[bI] + "' class='d-block w-100' style='height: 500px; object-fit: contain;' alt='...'>"
                    + "</div>"
                );
            }
            if (vListPhotos.length > 1) {
                $("#btn-carousel-control-prev").prop("hidden", false);
                $("#btn-carousel-control-next").prop("hidden", false);
            } else {
                $("#btn-carousel-control-prev").prop("hidden", true);
                $("#btn-carousel-control-next").prop("hidden", true);
            }
            $("#carouselIndicators").prop("hidden", false);
        } else {
            $("#carousel-images").html("");
        }
        $("#p-address").html(paramData.address);
        if (paramData.province != null) {
            $("#p-province").html(paramData.province.name);
        } else {
            $("#p-province").html("");
        }
        if (paramData.district != null) {
            $("#p-district").html(paramData.district.name);
        } else {
            $("#p-district").html("");
        }
        if (paramData.ward != null) {
            $("#p-ward").html(paramData.ward.name);
        } else {
            $("#p-ward").html("");
        }
        if (paramData.street != null) {
            $("#p-street").html(paramData.street.name);
        } else {
            $("#p-street").html("");
        }
        if (paramData.project != null) {
            $("#p-project").html(paramData.project.name);
        } else {
            $("#p-project").html("");
        }
        $("#p-title").html(paramData.title);
        $("#p-type").html(stringType(paramData.type));
        $("#p-request").html(stringRequest(paramData.request));
        if (paramData.customer != null) {
            $("#p-customer").html(paramData.customer.contactName + " - " + paramData.customer.mobile);
        } else {
            $("#p-customer").html("");
        }
        $("#p-price").html(paramData.price);
        $("#p-price-min").html(paramData.priceMin);
        $("#p-price-time").html(stringPriceTime(paramData.priceTime));
        $("#p-apart-code").html(paramData.apartCode);
        $("#p-wall-area").html(paramData.wallArea);
        $("#p-bedroom").html(paramData.bedroom);
        $("#p-balcony").html(stringBalcony(paramData.balcony));
        $("#p-landscape-view").html(paramData.landscapeView);
        $("#p-apart-loca").html(stringApartLoca(paramData.apartLoca));
        $("#p-apart-type").html(stringApartType(paramData.apartType));
        $("#p-furniture-type").html(stringFurnitureType(paramData.furnitureType));
        $("#p-price-rent").html(paramData.priceRent);
        $("#p-return-rate").html(paramData.returnRate);
        $("#p-acreage").html(paramData.acreage);
        $("#p-direction").html(stringDirection(paramData.direction));
        $("#p-total-floors").html(paramData.totalFloors);
        $("#p-number-floors").html(paramData.numberFloors);
        $("#p-bath").html(paramData.bath);
        $("#p-legal-doc").html(paramData.legalDoc);
        $("#p-description").html(paramData.description);
        $("#p-width-y").html(paramData.widthY);
        $("#p-long-x").html(paramData.longX);
        $("#p-street-house").html(stringStreetHouse(paramData.streetHouse));
        $("#p-fsbo").html(stringFsbo(paramData.fsbo));
        $("#p-shape").html(paramData.shape);
        $("#p-distance2facade").html(paramData.distance2Facade);
        $("#p-adjacent-facade-num").html(paramData.adjacentFacadeNum);
        $("#p-adjacent-road").html(paramData.adjacentRoad);
        $("#p-ctxd-price").html(paramData.clxdPrice);
        $("#p-ctxd-value").html(paramData.ctxdValue);
        $("#p-alley-min-width").html(paramData.alleyMinWidth);
        $("#p-adjacent-alley-min-width").html(paramData.adjacentAlleyMinWidth);
        $("#p-factor").html(paramData.factor);
        $("#p-structure").html(paramData.structure);
        $("#p-dtxd").html(paramData.dtxd);
        $("#p-clcl").html(paramData.clcl);
    }

    function stringType(paramData) {
        "use strict";
        var vString = "";
        switch (paramData) {
            case 0:
                vString = "Đất";
                break;
            case 1:
                vString = "Nhà ở";
                break;
            case 2:
                vString = "Căn hộ/Chung cư";
                break;
            case 3:
                vString = "Văn phòng/Mặt bằng";
                break;
            case 4:
                vString = "Kinh doanh";
                break;
            case 5:
                vString = "Phòng trọ";
                break;
            default:
                vString = "";
        }
        return vString;
    }

    function stringRequest(paramData) {
        "use strict";
        var vString = "";
        switch (paramData) {
            case 0:
                vString = "Cần bán";
                break;
            case 2:
                vString = "Cần mua";
                break;
            case 3:
                vString = "Cho thuê";
                break;
            case 4:
                vString = "Cần thuê";
                break;
            default:
                vString = "";
        }
        return vString;
    }

    function stringPriceTime(paramData) {
        var vString = "";
        switch (paramData) {
            case 0:
                vString = "Bán thường";
                break;
            case 1:
                vString = "Bán nhanh 24h";
                break;
            case 2:
                vString = "Bán nhanh 72h";
                break;
            case 3:
                vString = "Bán nhanh 1 tuần";
                break;
            default:
                vString = "";
        }
        return vString;
    }

    function stringBalcony(paramData) {
        var vString = "";
        switch (paramData) {
            case 0:
                vString = "Không có";
                break;
            case 1:
                vString = "Có ban công";
                break;
            case 2:
                vString = "Lô gia";
                break;
            default:
                vString = "";
        }
        return vString;
    }

    function stringApartLoca(paramData) {
        var vString = "";
        switch (paramData) {
            case 0:
                vString = "Căn góc";
                break;
            case 1:
                vString = "Căn thường";
                break;
            default:
                vString = "";
        }
        return vString;
    }

    function stringApartType(paramData) {
        var vString = "";
        switch (paramData) {
            case 0:
                vString = "Cao cấp";
                break;
            case 1:
                vString = "Văn phòng";
                break;
            case 2:
                vString = "Bình dân";
                break;
            default:
                vString = "";
        }
        return vString;
    }

    function stringFurnitureType(paramData) {
        var vString = "";
        switch (paramData) {
            case 0:
                vString = "Cơ bản";
                break;
            case 1:
                vString = "Đầy đủ";
                break;
            case 3:
                vString = "Chưa biết";
                break;
            default:
                vString = "";
        }
        return vString;
    }

    function stringDirection(paramData) {
        var vString = "";
        switch (paramData) {
            case 0:
                vString = "Tây Bắc";
                break;
            case 1:
                vString = "Tây";
                break;
            case 2:
                vString = "Bắc";
                break;
            case 3:
                vString = "Đông";
                break;
            case 4:
                vString = "Nam";
                break;
            case 5:
                vString = "Tây Nam";
                break;
            case 6:
                vString = "Đông Bắc";
                break;
            case 7:
                vString = "Đông Nam";
                break;
            case 8:
                vString = "Tây Nam, Đông Nam";
                break;
            case 9:
                vString = "Tây Nam, Tây Bắc";
                break;
            case 10:
                vString = "Không rõ";
                break;
            case 11:
                vString = "Đông Bắc, Đông Nam";
                break;
            case 12:
                vString = "Đông Bắc, Tây Bắc";
                break;
            default:
                vString = "";
        }
        return vString;
    }

    function stringStreetHouse(paramData) {
        var vString = "";
        switch (paramData) {
            case true:
                vString = "Có";
                break;
            default:
                vString = "Không";
        }
        return vString;
    }

    function stringFsbo(paramData) {
        var vString = "";
        switch (paramData) {
            case true:
                vString = "Phải";
                break;
            default:
                vString = "Không phải";
        }
        return vString;
    }
});