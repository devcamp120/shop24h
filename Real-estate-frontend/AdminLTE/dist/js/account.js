$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện các tag li
    $("#li-login").on("click", onLoginClick);
    $("#li-change-password").on("click", onChangePasswordClick);
    $("#li-logout").on("click", onLogoutClick);

    //Kiểm tra dữ liệu nhập vào trên form login
    $("#form-login").validate({
        submitHandler: function () {
            loginFunction();
        },
        rules: {
            inp_login_username: {
                required: true
            },
            inp_login_password: {
                required: true,
                minlength: 6
            }
        },
        messages: {
            inp_login_username: {
                required: "Nhập Tên người dùng.",
            },
            inp_login_password: {
                required: "Nhập Mật khẩu.",
                minlength: "Mật khẩu gồm ít nhất 6 ký tự."
            }
        },
        errorElement: 'span',
        errorClass: 'invalid-feedback',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    //Kiểm tra dữ liệu nhập vào trên form register
    $("#form-register").validate({
        submitHandler: function () {
            registerFunction();
        },
        rules: {
            inp_register_username: {
                required: true,
            },
            inp_register_password: {
                required: true,
                minlength: 6
            },
            inp_register_re_password: {
                required: true,
                minlength: 6,
                equalTo: "#inp-register-password"
            },
            check_agree: "required"
        },
        messages: {
            inp_register_username: {
                required: "Nhập Tên người dùng."
            },
            inp_register_password: {
                required: "Nhập Mật khẩu.",
                minlength: "Mật khẩu gồm ít nhất 6 ký tự."
            },
            inp_register_re_password: {
                required: "Nhập lại mật khẩu.",
                minlength: "Mật khẩu gồm ít nhất 6 ký tự.",
                equalTo: "Mật khẩu không khớp"
            },
            check_agree: {
                required: "Vui lòng đồng ý với điều khoản và điều kiện của chúng tôi."
            }
        },
        errorElement: 'span',
        errorClass: 'invalid-feedback',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    //Kiểm tra dữ liệu nhập vào trên form đổi mật khẩu
    $("#form-change-password").validate({
        submitHandler: function () {
            changePasswordFunction();
        },
        rules: {
            inp_old_password: {
                required: true,
                minlength: 6
            },
            inp_new_password: {
                required: true,
                minlength: 6
            },
            inp_re_new_password: {
                required: true,
                minlength: 6,
                equalTo: "#inp-new-password"
            }
        },
        messages: {
            inp_old_password: {
                required: "Nhập Mật khẩu hiện tại.",
                minlength: "Mật khẩu gồm ít nhất 6 ký tự."
            },
            inp_new_password: {
                required: "Nhập Mật khẩu mới.",
                minlength: "Mật khẩu gồm ít nhất 6 ký tự."
            },
            inp_re_new_password: {
                required: "Nhập lại mật khẩu mới.",
                minlength: "Mật khẩu gồm ít nhất 6 ký tự.",
                equalTo: "Mật khẩu không khớp"
            }
        },
        errorElement: 'span',
        errorClass: 'invalid-feedback',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        checkCookie();
    }

    //Hàm xử lý khi ấn Đăng nhập
    function onLoginClick() {
        "use strict";
        $("#form-login")[0].reset();
        $("#modal-login").modal("show");
    }

    //Hàm xử lý khi ấn Đổi mật khẩu
    function onChangePasswordClick() {
        "use strict";
        $("#form-change-password")[0].reset();
        $("#modal-change-password").modal("show");
    }

    //Hàm xử lý Đăng nhập
    function loginFunction() {
        "use strict";
        var vSignInData = {
            username: $("#inp-login-username").val().trim(),
            password: $("#inp-login-password").val().trim()
        }
        var vRemember = $("#check-remember").is(":checked");
        $.ajax({
            url: "http://localhost:8080/admin-seller/login",
            method: "POST",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(vSignInData),
            success: function (pRes) {
                setCookie("token", pRes, 10, vRemember);
                checkCookie();
                $("#modal-login").modal("hide");
            },
            error: function (pAjaxContext) {
                gToast.fire({
                    icon: "error",
                    title: "Tên người dùng hoặc mật khẩu không chính xác, hoặc không có quyền truy cập."
                });
                console.log(pAjaxContext.responseText);
            }
        });
    }

    //Hàm xử lý Đổi mật khẩu
    function changePasswordFunction() {
        "use strict";
        var vOldPassword = $("#inp-old-password").val().trim();
        var vNewPassword = $("#inp-new-password").val().trim();
        $.ajax({
            url: "http://localhost:8080/users/change-password?oldPassword=" + vOldPassword + "&newPassword=" + vNewPassword,
            method: "PUT",
            headers: {
                Authorization: "Token " + getCookie("token")
            },
            success: function (res) {
                if (res) {
                    setCookie("token", "", 10, false);
                    checkCookie();
                    $("#modal-change-password").modal("hide");
                    $("#modal-login").modal("show");
                    gToast.fire({
                        icon: "success",
                        title: "Đã đổi mật khẩu, vui lòng đăng nhập lại."
                    });
                } else {
                    gToast.fire({
                        icon: "warning",
                        title: "Mật khẩu hiện tại không đúng, vui lòng nhập lại."
                    });
                }
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });
    }

    //Hàm xử lý khi ấn Đăng xuất
    function onLogoutClick() {
        "use strict";
        //Xóa cookie
        setCookie("token", "", 10, false);
        checkCookie();
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm setCookie
    function setCookie(cname, cvalue, exdays, remeber) {
        "use strict";
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        if (remeber) {
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        } else {
            document.cookie = cname + "=" + cvalue + ";path=/";
        }
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm kiểm tra cookie
    function checkCookie() {
        "use strict";
        const token = getCookie("token");
        if (token) {
            checkToken(token);
        } else {
            if (!window.location.pathname.endsWith("address-maps.html")) {
                window.location.href = "address-maps.html";
            }
            $("#li-account").hide();
            $("#li-change-password").hide();
            $("#b-username").html("");
            $("#li-login").show();
            $("#li-register").show();
            $("#li-hr").hide();
            $("#li-logout").hide();
            $("#item-realestate").prop("hidden", true);
            $("#item-project").prop("hidden", true);
            $("#item-investor").prop("hidden", true);
            $("#item-construction-contractor").prop("hidden", true);
            $("#item-design-unit").prop("hidden", true);
            $("#item-master-layout").prop("hidden", true);
            $("#item-region-link").prop("hidden", true);
            $("#item-utility").prop("hidden", true);
            $("#item-province").prop("hidden", true);
            $("#item-employee").prop("hidden", true);
            $("#item-customer").prop("hidden", true);
        }
    }

    //Hàm kiểm tra token
    function checkToken(token) {
        "use strict";
        var headers = {
            Authorization: "Token " + token
        };
        $.ajax({
            url: "http://localhost:8080/admin-seller/users/me",
            method: "GET",
            headers: headers,
            success: function (res) {
                $("#li-account").show();
                $("#li-change-password").show();
                $("#b-username").html(res.username);
                $("#li-login").hide();
                $("#li-register").hide();
                $("#li-hr").show();
                $("#li-logout").show();
                $("#item-realestate").prop("hidden", false);
                $("#item-customer").prop("hidden", false);
                if (res.role == 1) {
                    $("#item-project").prop("hidden", false);
                    $("#item-investor").prop("hidden", false);
                    $("#item-construction-contractor").prop("hidden", false);
                    $("#item-design-unit").prop("hidden", false);
                    $("#item-master-layout").prop("hidden", false);
                    $("#item-region-link").prop("hidden", false);
                    $("#item-utility").prop("hidden", false);
                    $("#item-province").prop("hidden", false);
                    $("#item-employee").prop("hidden", false);
                } else if (res.role == 2) {
                    $("#item-project").prop("hidden", true);
                    $("#item-investor").prop("hidden", true);
                    $("#item-construction-contractor").prop("hidden", true);
                    $("#item-design-unit").prop("hidden", true);
                    $("#item-master-layout").prop("hidden", true);
                    $("#item-region-link").prop("hidden", true);
                    $("#item-province").prop("hidden", true);
                    $("#item-master-layout").prop("hidden", true);
                    $("#item-region-link").prop("hidden", true);
                    $("#item-utility").prop("hidden", true);
                    $("#item-province").prop("hidden", true);
                    $("#item-employee").prop("hidden", true);
                }
            },
            error: function (err) {
                if (!window.location.pathname.endsWith("address-maps.html")) {
                    window.location.href = "address-maps.html";
                }
                $("#li-account").hide();
                $("#li-change-password").hide();
                $("#b-username").html("");
                console.log(err.responseText);
                setCookie("token", "", 10, false);
                $("#li-login").show();
                $("#li-register").show();
                $("#li-hr").hide();
                $("#li-logout").hide();
                $("#item-realestate").prop("hidden", true);
                $("#item-project").prop("hidden", true);
                $("#item-investor").prop("hidden", true);
                $("#item-construction-contractor").prop("hidden", true);
                $("#item-design-unit").prop("hidden", true);
                $("#item-master-layout").prop("hidden", true);
                $("#item-region-link").prop("hidden", true);
                $("#item-utility").prop("hidden", true);
                $("#item-province").prop("hidden", true);
                $("#item-employee").prop("hidden", true);
                $("#item-customer").prop("hidden", true);
            }
        });
    }
});