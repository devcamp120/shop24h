$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gId = "";
    var gProvinceId = "";

    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút Lưu lại
    $("#btn-save").on("click", onBtnSaveClick);

    //Gán sự kiện cho nút Hủy bỏ
    $("#btn-cancel").on("click", onBtnCancelClick);

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        const vUrlParams = new URLSearchParams(window.location.search);
        gId = vUrlParams.get("id");

        $(".select2bs4").select2({
            theme: "bootstrap4"
        });

        $.ajax({
            url: "http://localhost:8080/provinces/all",
            method: "GET",
            async: false,
            success: function (res) {
                loadDataToSelectProvince(res);
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });

        if (gId != "" || gId != null) {
            $.ajax({
                url: "http://localhost:8080/districts/" + gId,
                method: "GET",
                success: function (res) {
                    loadDetailData(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }

    //Hàm xử lý khi ấn nút Lưu lại
    function onBtnSaveClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vDistrict = {
                name: "",
                prefix: ""
            }
            //Lấy dữ liệu
            getDistrictData(vDistrict);
            //Kiểm tra dữ liệu
            var vIsCheck = validateDistrictData(vDistrict);

            if (vIsCheck) {
                $.ajax({
                    url: "http://localhost:8080/districts/" + gId + "?provinceId=" + gProvinceId,
                    method: "PUT",
                    headers: {
                        Authorization: "Token " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vDistrict),
                    success: function (res) {
                        window.location.href = "districts.html";
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            $("#modal-login").modal("show");
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }
    }

    //Hàm xử lý khi ấn nút Hủy bỏ
    function onBtnCancelClick() {
        "use strict";
        window.history.back();
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm xử lý hiển thị chi tiết Quận huyện
    function loadDetailData(paramData) {
        "use strict";
        $("#inp-name").val(paramData.name);
        $("#inp-code").val(paramData.code);
    }

    //Hàm load dữ liệu vào Select Tỉnh / Thành phố
    function loadDataToSelectProvince(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-province").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm lấy dữ liệu Quận huyện
    function getDistrictData(paramDistrict) {
        gProvinceId = $("#sel-province").val();
        paramDistrict.name = $("#inp-name").val().trim();
        paramDistrict.prefix = $("#inp-prefix").val().trim();
    }

    //Hàm lấy kiểm tra dữ liệu Quận huyện
    function validateDistrictData(paramDistrict) {
        "use strict";
        if (paramDistrict.name == "") {
            gToast.fire({
                icon: "warning",
                title: "Nhập tên Quận huyện."
            });
            return false;
        }
        if (gProvinceId == "-1") {
            gToast.fire({
                icon: "warning",
                title: "Chọn Tỉnh thành phố."
            });
            return false
        }
        return true;
    }
});