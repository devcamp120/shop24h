"use strict";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gTotalPages = 0;
    var gPage = 0;
    var gPageSize = 10;
    var gId = "";
    const gTable = $("#tbl-region-links");
    const gNameCol = ["action", "id", "name", "description", "address", "lat", "lng"];
    const gACTION_COL = 0;
    const gID_COL = 1;
    const gNAME_COL = 2;
    const gDESCRIPTION_COL = 3;
    const gADDRESS_COL = 4;
    const gLAT_COL = 5;
    const gLNG_COL = 6;
    var gDataTable = gTable.DataTable({
        columns: [
            { data: gNameCol[gACTION_COL] },
            { data: gNameCol[gID_COL] },
            { data: gNameCol[gNAME_COL] },
            { data: gNameCol[gDESCRIPTION_COL] },
            { data: gNameCol[gADDRESS_COL] },
            { data: gNameCol[gLAT_COL] },
            { data: gNameCol[gLNG_COL] }
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: "<a class='btn detail-region-link' title='Chi tiết'><i class='text-info fas fa-info'></i></a><a class='btn edit-region-link' title='Sửa'><i class='text-primary fas fa-edit'></i></a><a class='btn delete-region-link' title='Xóa'><i class='text-danger fas fa-trash'></i></a>",
                className: "text-nowrap"
            },
            {
                targets: gNAME_COL,
                className: "text-nowrap"
            },
            {
                targets: gDESCRIPTION_COL,
                width: 300
            },
            {
                targets: gADDRESS_COL,
                width: 300
            }
        ],
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        scrollY: 350,
        scrollX: true,
    });

    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút add
    $("#add-region-link").on("click", function () {
        onAddRegionLinkClick();
    });

    //Gán sự kiện cho nút chi tiết
    $("#tbl-region-links tbody").on("click", ".detail-region-link", function () {
        onDetailRegionLinkClick(this);
    });

    //Gán sự kiện cho nút edit
    $("#tbl-region-links tbody").on("click", ".edit-region-link", function () {
        onEditRegionLinkClick(this);
    });

    //Gán sự kiện cho nút xóa
    $("#tbl-region-links tbody").on("click", ".delete-region-link", function () {
        onDeleteRegionLinkClick(this);
    });

    //Gán sự kiện cho nút Xóa trên modal
    $("#btn-delete-confirm").on("click", onBtnDeleteConfirmClick);

    //Gán sự kiện cho các nút phân trang
    $("#number-page").on("keypress", function (event) {
        if (event.which == 13) {
            onNumberPageEnter();
        }
    });
    $("#sel-page-size").on("change", onChangePageSize);
    $("#start-page").on("click", onStartPageClick);
    $("#previous-page").on("click", onPreviousPageClick);
    $("#next-page").on("click", onNextPageClick);
    $("#end-page").on("click", onEndPageClick);

    //Điều chỉnh lại các cột table khi thay đổi kích thước window
    $(window).resize(function () {
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });


    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        loadRegionLinkDataToTable()
    }

    // Hàm xử lý khi click nút add
    function onAddRegionLinkClick() {
        "use strict";
        window.location.href = "add-region-link.html"
    }

    // Hàm xử lý khi click nút chi tiết
    function onDetailRegionLinkClick(paramBtnDetail) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnDetail.closest("tr")).data().id;
        $.ajax({
            url: "http://localhost:8080/region-links/" + gId,
            method: "GET",
            success: function (res) {
                loadDetailData(res);
                $("#modal-detail").modal("show");
            }, error: function (err) {
                console.log(err.responseText);
            }
        });
    }


    // Hàm xử lý khi click nút edit
    function onEditRegionLinkClick(paramBtnEdit) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnEdit.closest("tr")).data().id;
        var token = getCookie("token");
        if (token) {
            window.location.href = "edit-region-link.html?id=" + gId;
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }
    }

    // Hàm xử lý khi click nút xóa
    function onDeleteRegionLinkClick(paramBtnDelete) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnDelete.closest("tr")).data().id;
        var token = getCookie("token");
        if (token) {
            $("#modal-delete").modal("show");
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }
    }

    // Hàm xử lý khi click nút xóa trên modal
    function onBtnDeleteConfirmClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: "http://localhost:8080/region-links/" + gId,
                method: "DELETE",
                headers: {
                    Authorization: "Token " + token
                },
                success: function (res) {
                    $("#modal-delete").modal("hide");
                    loadRegionLinkDataToTable();
                }, error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }
    }

    function onNumberPageEnter() {
        var vPage = $("#number-page").val();
        if (vPage < 1) {
            gPage = 0;
            loadRegionLinkDataToTable();
        } else if (vPage > gTotalPages && gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadRegionLinkDataToTable();
        } else {
            gPage = vPage - 1;
            loadRegionLinkDataToTable();
        }
    }

    function onChangePageSize() {
        gPage = 0;
        gPageSize = $("#sel-page-size").val();
        loadRegionLinkDataToTable();
    }

    //Hàm xử lý khi ấn nút Trang đầu
    function onStartPageClick() {
        if (gTotalPages > 0) {
            gPage = 0;
            loadRegionLinkDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang trước
    function onPreviousPageClick() {
        if (gTotalPages > 0 && gPage > 0) {
            gPage--;
            loadRegionLinkDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang sau
    function onNextPageClick() {
        if (gTotalPages > 0 && gPage < gTotalPages - 1) {
            gPage++;
            loadRegionLinkDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang cuối
    function onEndPageClick() {
        if (gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadRegionLinkDataToTable();
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm load dữ liệu customer vào bảng
    function loadRegionLinkDataToTable() {
        "use strict";
        $.ajax({
            url: "http://localhost:8080/region-links?page=" + gPage + "&size=" + gPageSize,
            method: "GET",
            success: function (res) {
                var vDataTable = gTable.DataTable();
                vDataTable.clear();
                vDataTable.rows.add(res.content);
                vDataTable.draw();
                $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
                gTotalPages = res.totalPages;
                $("#number-page").val(gPage + 1);
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm xử lý hiển thị chi tiết vị trí liên kết
    function loadDetailData(paramData) {
        "use strict";
        $("#p-id").html(paramData.id);
        $("#p-name").html(paramData.name);
        $("#carousel-images").html("");
        $("#carouselIndicators").prop("hidden", true);
        if (paramData.photo != "" && paramData.photo != null) {
            var vListPhotos = paramData.photo.split(",");
            $("#carousel-images").append(
                "<div class='carousel-item active'>"
                + "<img src='http://localhost:8080/download/region-link-photo/" + vListPhotos[0] + "' class='d-block w-100' style='height: 500px; object-fit: contain;' alt='...'>"
                + "</div>"
            );
            for (var bI = 1; bI < vListPhotos.length; bI++) {
                $("#carousel-images").append(
                    "<div class='carousel-item'>"
                    + "<img src='http://localhost:8080/download/region-link-photo/" + vListPhotos[bI] + "' class='d-block w-100' style='height: 500px; object-fit: contain;' alt='...'>"
                    + "</div>"
                );
            }
            if (vListPhotos.length > 1) {
                $("#btn-carousel-control-prev").prop("hidden", false);
                $("#btn-carousel-control-next").prop("hidden", false);
            } else {
                $("#btn-carousel-control-prev").prop("hidden", true);
                $("#btn-carousel-control-next").prop("hidden", true);
            }
            $("#carouselIndicators").prop("hidden", false);
        } else {
            $("#carousel-images").html("");
        }
        $("#p-description").html(paramData.description);
        $("#p-address").html(paramData.address);
        $("#p-lat").html(paramData.lat);
        $("#p-lng").html(paramData.lng);
    }
});