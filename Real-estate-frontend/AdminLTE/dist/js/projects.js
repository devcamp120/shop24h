"use strict";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gTotalPages = 0;
    var gPage = 0;
    var gPageSize = 10;
    var gId = "";
    const gTable = $("#tbl-projects");
    const gNameCol = ["action", "id", "name", "province", "district", "ward", "street", "address", "slogan", "description", "acreage", "constructArea", "numBlock", "numFloors", "numApartment", "apartmenttArea", "investor", "constructionContractor", "designUnit"];
    const gACTION_COL = 0;
    const gID_COL = 1;
    const gNAME_COL = 2;
    const gPROVINCE_COL = 3;
    const gDISTRICT_COL = 4;
    const gWARD_COL = 5;
    const gSTREET_COL = 6;
    const gADDRESS_COL = 7;
    const gSLOGAN_COL = 8;
    const gDESCRIPTION_COL = 9;
    const gACREAGE_COL = 10;
    const gCONSTRUCTAREA_COL = 11;
    const gNUMBLOCK_COL = 12;
    const gNUMFLOORS_COL = 13;
    const gNUMAPARTMENT_COL = 14;
    const gAPARTMENTTAREA_COL = 15;
    const gINVESTOR_COL = 16;
    const gCONSTRUCTIONCONTRACTOR_COL = 17;
    const gDESIGNUNIT_COL = 18;
    var gDataTable = gTable.DataTable({
        columns: [
            { data: gNameCol[gACTION_COL] },
            { data: gNameCol[gID_COL] },
            { data: gNameCol[gNAME_COL] },
            { data: gNameCol[gPROVINCE_COL] },
            { data: gNameCol[gDISTRICT_COL] },
            { data: gNameCol[gWARD_COL] },
            { data: gNameCol[gSTREET_COL] },
            { data: gNameCol[gADDRESS_COL] },
            { data: gNameCol[gSLOGAN_COL] },
            { data: gNameCol[gDESCRIPTION_COL] },
            { data: gNameCol[gACREAGE_COL] },
            { data: gNameCol[gCONSTRUCTAREA_COL] },
            { data: gNameCol[gNUMBLOCK_COL] },
            { data: gNameCol[gNUMFLOORS_COL] },
            { data: gNameCol[gNUMAPARTMENT_COL] },
            { data: gNameCol[gAPARTMENTTAREA_COL] },
            { data: gNameCol[gINVESTOR_COL] },
            { data: gNameCol[gCONSTRUCTIONCONTRACTOR_COL] },
            { data: gNameCol[gDESIGNUNIT_COL] }
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: "<a class='btn detail-project' title='Chi tiết'><i class='text-info fas fa-info'></i></a><a class='btn edit-project' title='Sửa'><i class='text-primary fas fa-edit'></i></a><a class='btn delete-project' title='Xóa'><i class='text-danger fas fa-trash'></i></a>",
                className: "text-nowrap"
            },
            {
                targets: gNAME_COL,
                className: "text-nowrap"
            },
            {
                targets: gPROVINCE_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.name;
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gDISTRICT_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.name;
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gWARD_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.name;
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gSTREET_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.name;
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gADDRESS_COL,
                width: 300
            },
            {
                targets: gSLOGAN_COL,
                width: 300
            },
            {
                targets: gINVESTOR_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.name;
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gCONSTRUCTIONCONTRACTOR_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.name;
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gDESIGNUNIT_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.name;
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gDESCRIPTION_COL,
                width: 300
            }
        ],
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        scrollY: 350,
        scrollX: true,
    });

    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút add
    $("#add-project").on("click", function () {
        onAddProjectClick();
    });

    //Gán sự kiện cho nút chi tiết
    $("#tbl-projects tbody").on("click", ".detail-project", function () {
        onDetailProjectClick(this);
    });

    //Gán sự kiện cho nút edit
    $("#tbl-projects tbody").on("click", ".edit-project", function () {
        onEditProjectClick(this);
    });

    //Gán sự kiện cho nút xóa
    $("#tbl-projects tbody").on("click", ".delete-project", function () {
        onDeleteProjectClick(this);
    });

    //Gán sự kiện cho nút Xóa trên modal
    $("#btn-delete-confirm").on("click", onBtnDeleteConfirmClick);

    //Gán sự kiện cho các nút phân trang
    $("#number-page").on("keypress", function (event) {
        if (event.which == 13) {
            onNumberPageEnter();
        }
    });
    $("#sel-page-size").on("change", onChangePageSize);
    $("#start-page").on("click", onStartPageClick);
    $("#previous-page").on("click", onPreviousPageClick);
    $("#next-page").on("click", onNextPageClick);
    $("#end-page").on("click", onEndPageClick);

    //Điều chỉnh lại các cột table khi thay đổi kích thước window
    $(window).resize(function () {
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });


    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        loadProjectDataToTable()
    }

    // Hàm xử lý khi click nút add
    function onAddProjectClick() {
        "use strict";
        window.location.href = "add-project.html"
    }

    // Hàm xử lý khi click nút chi tiết
    function onDetailProjectClick(paramBtnDetail) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnDetail.closest("tr")).data().id;
        $.ajax({
            url: "http://localhost:8080/projects/" + gId,
            method: "GET",
            success: function (res) {
                loadDetailData(res);
                $("#modal-detail").modal("show");
            }, error: function (err) {
                console.log(err.responseText);
            }
        });
    }


    // Hàm xử lý khi click nút edit
    function onEditProjectClick(paramBtnEdit) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnEdit.closest("tr")).data().id;
        var token = getCookie("token");
        if (token) {
            window.location.href = "edit-project.html?id=" + gId;
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }
    }

    // Hàm xử lý khi click nút xóa
    function onDeleteProjectClick(paramBtnDelete) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnDelete.closest("tr")).data().id;
        var token = getCookie("token");
        if (token) {
            $("#modal-delete").modal("show");
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }
    }

    // Hàm xử lý khi click nút xóa trên modal
    function onBtnDeleteConfirmClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: "http://localhost:8080/projects/" + gId,
                method: "DELETE",
                headers: {
                    Authorization: "Token " + token
                },
                success: function (res) {
                    $("#modal-delete").modal("hide");
                    loadProjectDataToTable();
                }, error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }
    }

    function onNumberPageEnter() {
        var vPage = $("#number-page").val();
        if (vPage < 1) {
            gPage = 0;
            loadProjectDataToTable();
        } else if (vPage > gTotalPages && gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadProjectDataToTable();
        } else {
            gPage = vPage - 1;
            loadProjectDataToTable();
        }
    }

    function onChangePageSize() {
        gPage = 0;
        gPageSize = $("#sel-page-size").val();
        loadProjectDataToTable();
    }

    //Hàm xử lý khi ấn nút Trang đầu
    function onStartPageClick() {
        if (gTotalPages > 0) {
            gPage = 0;
            loadProjectDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang trước
    function onPreviousPageClick() {
        if (gTotalPages > 0 && gPage > 0) {
            gPage--;
            loadProjectDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang sau
    function onNextPageClick() {
        if (gTotalPages > 0 && gPage < gTotalPages - 1) {
            gPage++;
            loadProjectDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang cuối
    function onEndPageClick() {
        if (gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadProjectDataToTable();
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm load dữ liệu customer vào bảng
    function loadProjectDataToTable() {
        "use strict";
        $.ajax({
            url: "http://localhost:8080/projects?page=" + gPage + "&size=" + gPageSize,
            method: "GET",
            success: function (res) {
                var vDataTable = gTable.DataTable();
                vDataTable.clear();
                vDataTable.rows.add(res.content);
                vDataTable.draw();
                $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
                gTotalPages = res.totalPages;
                $("#number-page").val(gPage + 1);
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm xử lý hiển thị chi tiết Dự án
    function loadDetailData(paramData) {
        "use strict";
        $("#p-id").html(paramData.id);
        $("#p-name").html(paramData.name);
        $("#p-address").html(paramData.address);
        $("#carousel-images").html("");
        $("#carouselIndicators").prop("hidden", true);
        if (paramData.photo != "" && paramData.photo != null) {
            var vListPhotos = paramData.photo.split(",");
            $("#carousel-images").append(
                "<div class='carousel-item active'>"
                + "<img src='http://localhost:8080/download/project-photo/" + vListPhotos[0] + "' class='d-block w-100' style='height: 500px; object-fit: contain;' alt='...'>"
                + "</div>"
            );
            for (var bI = 1; bI < vListPhotos.length; bI++) {
                $("#carousel-images").append(
                    "<div class='carousel-item'>"
                    + "<img src='http://localhost:8080/download/project-photo/" + vListPhotos[bI] + "' class='d-block w-100' style='height: 500px; object-fit: contain;' alt='...'>"
                    + "</div>"
                );
            }
            if (vListPhotos.length > 1) {
                $("#btn-carousel-control-prev").prop("hidden", false);
                $("#btn-carousel-control-next").prop("hidden", false);
            } else {
                $("#btn-carousel-control-prev").prop("hidden", true);
                $("#btn-carousel-control-next").prop("hidden", true);
            }
            $("#carouselIndicators").prop("hidden", false);
        } else {
            $("#carousel-images").html("");
        }
        if (paramData.province != null) {
            $("#p-province").html(paramData.province.name);
        } else {
            $("#p-province").html("");
        }
        if (paramData.district != null) {
            $("#p-district").html(paramData.district.name);
        } else {
            $("#p-district").html("");
        }
        if (paramData.ward != null) {
            $("#p-ward").html(paramData.ward.name);
        } else {
            $("#p-ward").html("");
        }
        if (paramData.street != null) {
            $("#p-street").html(paramData.street.name);
        } else {
            $("#p-street").html("");
        }
        $("#p-slogan").html(paramData.slogan);
        $("#p-description").html(paramData.description);
        $("#p-acreage").html(paramData.acreage);
        $("#p-constructArea").html(paramData.constructArea);
        $("#p-numBlock").html(paramData.numBlock);
        $("#p-numFloors").html(paramData.numFloors);
        $("#p-numApartment").html(paramData.numApartment);
        $("#p-apartmenttArea").html(paramData.apartmenttArea);
        if (paramData.investor != null) {
            $("#p-investor").html(paramData.investor.name);
        } else {
            $("#p-investor").html("");
        }
        if (paramData.constructionContractor != null) {
            $("#p-constructionContractor").html(paramData.constructionContractor.name);
        } else {
            $("#p-constructionContractor").html("");
        }
        if (paramData.designUnit != null) {
            $("#p-designUnit").html(paramData.designUnit.name);
        } else {
            $("#p-designUnit").html("");
        }
        $("#p-utilities").html(stringUtility(paramData.utilities));
        $("#p-regionLink").html(stringRegionLink(paramData.regionLink));
        $("#p-lat").html(paramData.lat);
        $("#p-lng").html(paramData.lng);
    }

    function stringUtility(paramData) {
        "use strict";
        var vString = "<ul>";
        if (paramData != "") {
            var vArrayData = paramData.split(",");
            for (var bI = 0; bI < vArrayData.length; bI++) {
                $.ajax({
                    url: "http://localhost:8080/utilities/" + vArrayData[bI],
                    method: "GET",
                    async: false,
                    success: function (res) {
                        if (res != null) {
                            vString += "<li>" + res.name + "</li>";
                        }
                    }, error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        }
        vString += "</ul>"
        return vString;
    }

    function stringRegionLink(paramData) {
        "use strict";
        var vString = "<ul>";
        if (paramData != "") {
            var vArrayData = paramData.split(",");
            for (var bI = 0; bI < vArrayData.length; bI++) {
                $.ajax({
                    url: "http://localhost:8080/region-links/" + vArrayData[bI],
                    method: "GET",
                    async: false,
                    success: function (res) {
                        if (res != null) {
                            vString += "<li>" + res.name + "</li>";
                        }
                    }, error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        }
        vString += "</ul>"
        return vString;
    }
});