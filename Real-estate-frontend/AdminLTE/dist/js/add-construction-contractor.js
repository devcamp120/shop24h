$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gAddressId = "";

    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút Lưu lại
    $("#btn-save").on("click", onBtnSaveClick);

    //Gán sự kiện cho nút Hủy bỏ
    $("#btn-cancel").on("click", onBtnCancelClick);

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";

        $(".select2bs4").select2({
            theme: "bootstrap4"
        });

        $.ajax({
            url: "http://localhost:8080/address-maps/all",
            method: "GET",
            async: false,
            success: function (res) {
                loadDataToSelectAddress(res);
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });
    }

    //Hàm xử lý khi ấn nút Lưu lại
    function onBtnSaveClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vConstructionContractor = {
                name: "",
                description: "",
                phone: "",
                phone2: "",
                fax: "",
                email: "",
                website: "",
                note: ""
            }
            //Lấy dữ liệu
            getConstructionContractorData(vConstructionContractor);
            //Kiểm tra dữ liệu
            var vIsCheck = validateConstructionContractorData(vConstructionContractor);

            if (vIsCheck) {
                $.ajax({
                    url: "http://localhost:8080/construction-contractors" + "?addressId=" + gAddressId,
                    method: "POST",
                    headers: {
                        Authorization: "Token " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vConstructionContractor),
                    success: function (res) {
                        window.location.href = "construction-contractors.html";
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            $("#modal-login").modal("show");
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đang nhập."
            });
        }
    }

    //Hàm xử lý khi ấn nút Hủy bỏ
    function onBtnCancelClick() {
        "use strict";
        window.history.back();
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm load dữ liệu vào Select Địa chỉ
    function loadDataToSelectAddress(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            console.log(bI);
            var bAddress = paramData[bI].address;
            console.log(bAddress);
            $("#sel-address").append($("<option>").val(bId).text(bAddress));
        }
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm lấy dữ liệu nhà thầu xây dựng
    function getConstructionContractorData(paramConstructionContractor) {
        gAddressId = $("#sel-address").val();
        paramConstructionContractor.name = $("#inp-name").val();
        paramConstructionContractor.description = $("#inp-description").val();
        paramConstructionContractor.phone = $("#inp-phone").val();
        paramConstructionContractor.phone2 = $("#inp-phone2").val();
        paramConstructionContractor.fax = $("#inp-fax").val();
        paramConstructionContractor.email = $("#inp-email").val();
        paramConstructionContractor.website = $("#inp-website").val();
        paramConstructionContractor.note = $("#inp-note").val();
    }

    //Hàm lấy kiểm tra dữ liệu nhà thầu xây dựng
    function validateConstructionContractorData(paramConstructionContractor) {
        "use strict";
        if (paramConstructionContractor.name.trim() == "") {
            gToast.fire({
                icon: "warning",
                title: "Nhập tên nhà thầu xây dựng."
            });
            return false;
        }
        return true;
    }
});