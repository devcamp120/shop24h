"use strict";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gTotalPages = 0;
    var gPage = 0;
    var gPageSize = 10;
    var gId = "";
    const gTable = $("#tbl-address-maps");
    const gNameCol = ["action", "id", "address", "lat", "lng"];
    const gACTION_COL = 0;
    const gID_COL = 1;
    const gADDRESS_COL = 2;
    const gLAT_COL = 3;
    const gLNG_COL = 4;
    var gDataTable = gTable.DataTable({
        columns: [
            { data: gNameCol[gACTION_COL] },
            { data: gNameCol[gID_COL] },
            { data: gNameCol[gADDRESS_COL] },
            { data: gNameCol[gLAT_COL] },
            { data: gNameCol[gLNG_COL] }
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: "<a class='btn edit-address-map' title='Sửa'><i class='text-primary fas fa-edit'></i></a>",
                className: "text-center"
            },
            {
                targets: gADDRESS_COL,
                width: 300
            }
        ],
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        scrollY: 350,
        scrollX: true,
    });

    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút add
    $("#add-address-map").on("click", function () {
        onAddAddressMapClick();
    });

    //Gán sự kiện cho nút edit
    $("#tbl-address-maps tbody").on("click", ".edit-address-map", function () {
        onEditAddressMapClick(this);
    });

    //Gán sự kiện click cho nút Thêm trên Modal
    $("#form-add").on("submit", function (event) {
        onBtnAddClick();
        event.preventDefault();
    });

    //Gán sự kiện click cho nút Cập nhật trên Modal
    $("#form-edit").on("submit", function (event) {
        onBtnUpdateClick();
        event.preventDefault();
    });

    //Gán sự kiện cho các nút phân trang
    $("#number-page").on("keypress", function (event) {
        if (event.which == 13) {
            onNumberPageEnter();
        }
    });
    $("#sel-page-size").on("change", onChangePageSize);
    $("#start-page").on("click", onStartPageClick);
    $("#previous-page").on("click", onPreviousPageClick);
    $("#next-page").on("click", onNextPageClick);
    $("#end-page").on("click", onEndPageClick);

    //Điều chỉnh lại các cột table khi thay đổi kích thước window
    $(window).resize(function () {
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });


    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        loadAddressMapDataToTable();
    }

    // Hàm xử lý khi click nút add
    function onAddAddressMapClick() {
        "use strict";
        $("#modal-add .inp-address").val("");
        $("#modal-add .inp-lat").val("");
        $("#modal-add .inp-lng").val("");
        $("#modal-add").modal("show");
    }

    // Hàm xử lý khi click nút edit
    function onEditAddressMapClick(paramBtnEdit) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnEdit.closest("tr")).data().id;
        $.ajax({
            url: "http://localhost:8080/address-maps/" + gId,
            method: "GET",
            success: function (res) {
                $("#modal-edit .inp-address").val(res.address);
                $("#modal-edit .inp-lat").val(res.lat);
                $("#modal-edit .inp-lng").val(res.lng);
                $("#modal-edit").modal("show");
            }, error: function (err) {
                console.log(err.responseText);
            }
        });
    }

    //Hàm xử lý khi click nút Thêm trên Modal
    function onBtnAddClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vAddressMap = {
                address: "",
                lat: 0,
                lng: 0
            }
            getDataFromAddModal(vAddressMap);
            var vJsonAddressMap = JSON.stringify(vAddressMap);
            $.ajax({
                url: "http://localhost:8080/address-maps",
                method: "POST",
                async: false,
                headers: {
                    Authorization: "Token " + token,
                    "Content-Type": "application/json"
                },
                data: vJsonAddressMap,
                success: function (res) {
                    $("#modal-add").modal("hide");
                    gToast.fire({
                        icon: "success",
                        title: "Đã thêm địa chỉ tọa độ."
                    });
                }, error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập, vui lòng đăng nhập."
            });
        }
    }

    //Hàm xử lý khi click nút Cập nhật trên Modal
    function onBtnUpdateClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vAddressMap = {
                address: "",
                lat: 0,
                lng: 0
            }
            getDataFromEditModal(vAddressMap);
            var vJsonAddressMap = JSON.stringify(vAddressMap);
            $.ajax({
                url: "http://localhost:8080/address-maps/" + gId,
                method: "PUT",
                headers: {
                    Authorization: "Token " + token,
                    "Content-Type": "application/json"
                },
                data: vJsonAddressMap,
                success: function (res) {
                    $("#modal-edit").modal("hide");
                    loadAddressMapDataToTable(gPage);
                }, error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập, vui lòng đăng nhập."
            });
        }
    }

    function onNumberPageEnter() {
        var vPage = $("#number-page").val();
        if (vPage < 1) {
            gPage = 0;
            loadAddressMapDataToTable();
        } else if (vPage > gTotalPages && gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadAddressMapDataToTable();
        } else {
            gPage = vPage - 1;
            loadAddressMapDataToTable();
        }
    }

    function onChangePageSize() {
        gPage = 0;
        gPageSize = $("#sel-page-size").val();
        loadAddressMapDataToTable();
    }

    //Hàm xử lý khi ấn nút Trang đầu
    function onStartPageClick() {
        if (gTotalPages > 0) {
            gPage = 0;
            loadAddressMapDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang trước
    function onPreviousPageClick() {
        if (gTotalPages > 0 && gPage > 0) {
            gPage--;
            loadAddressMapDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang sau
    function onNextPageClick() {
        if (gTotalPages > 0 && gPage < gTotalPages - 1) {
            gPage++;
            loadAddressMapDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang cuối
    function onEndPageClick() {
        if (gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadAddressMapDataToTable();
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm load dữ liệu customer vào bảng
    function loadAddressMapDataToTable(pPage) {
        "use strict";
        $.ajax({
            url: "http://localhost:8080/address-maps?page=" + gPage + "&size=" + gPageSize,
            method: "GET",
            success: function (res) {
                var vDataTable = gTable.DataTable();
                vDataTable.clear();
                vDataTable.rows.add(res.content);
                vDataTable.draw();
                $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
                gTotalPages = res.totalPages;
                $("#number-page").val(gPage + 1);
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    // Hàm lấy dữ liệu từ Modal add
    function getDataFromAddModal(paramAddressMap) {
        "use strict";
        paramAddressMap.address = $("#modal-add .inp-address").val().trim();
        paramAddressMap.lat = $("#modal-add .inp-lat").val();
        paramAddressMap.lng = $("#modal-add .inp-lng").val();
    }

    // Hàm lấy dữ liệu từ Modal edit
    function getDataFromEditModal(paramAddressMap) {
        "use strict";
        paramAddressMap.address = $("#modal-edit .inp-address").val().trim();
        paramAddressMap.lat = $("#modal-edit .inp-lat").val();
        paramAddressMap.lng = $("#modal-edit .inp-lng").val();
    }
});