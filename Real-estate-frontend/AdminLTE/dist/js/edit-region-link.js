$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gId = "";
    var gPhoto = "";
    var gDeletePhoto = [];

    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút Lưu lại
    $("#btn-save").on("click", onBtnSaveClick);

    //Gán sự kiện cho nút Hủy bỏ
    $("#btn-cancel").on("click", onBtnCancelClick);

    $("#card-images").on("click", ".delete-server-image", function () {
        var vListPhotos = gPhoto.split(",");
        var vPhotoName = $(this)[0].dataset.photoName;
        var index = vListPhotos.indexOf(vPhotoName);
        if (index !== -1) {
            vListPhotos.splice(index, 1);
        }
        gDeletePhoto.push(vPhotoName);
        gPhoto = vListPhotos.join(",");
        $(this).parent().parent().remove();
    });

    $("#card-images").on("click", ".delete-local-image", function () {
        $("#file-photo").val("");
        $("#card-images .delete-local-image").parent().parent().remove();
    });

    $("#file-photo").on("change", function () {
        $("#card-images .delete-local-image").parent().parent().remove();
        var vFiles = this.files;
        for (var bI = 0; bI < vFiles.length; bI++) {
            if (vFiles[bI]) {
                let reader = new FileReader();
                reader.onload = function (event) {
                    $("#card-images").append(
                        "<div class='col'>"
                        + "<div class='card border-0'>"
                        + "<img src='" + event.target.result + "' class='card-img-top' style='object-fit: contain;' alt='...'>"
                        + "<button class='btn border-0 delete-local-image' style='position: absolute; top: 0%; left: 85%;'><i class='text-danger fas fa-window-close'></i></button>"
                        + "</div>"
                        + "</div>"
                    );
                }
                reader.readAsDataURL(vFiles[bI]);
            }
        }
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        const vUrlParams = new URLSearchParams(window.location.search);
        gId = vUrlParams.get("id");

        $(".select2bs4").select2({
            theme: "bootstrap4"
        });

        if (gId != "" || gId != null) {
            $.ajax({
                url: "http://localhost:8080/region-links/" + gId,
                method: "GET",
                success: function (res) {
                    loadDetailData(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }

    //Hàm xử lý khi ấn nút Lưu lại
    function onBtnSaveClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vRegionLink = {
                name: "",
                description: "",
                address: "",
                lat: "",
                lng: ""
            }
            //Lấy dữ liệu
            getRegionLinkData(vRegionLink);
            //Kiểm tra dữ liệu
            var vIsCheck = validateRegionLinkData(vRegionLink);

            if (vIsCheck) {
                if (gDeletePhoto.length > 0) {
                    for (var bI = 0; bI < gDeletePhoto.length; bI++) {
                        $.ajax({
                            url: "http://localhost:8080/delete/region-link-photo/" + gDeletePhoto[bI] + "?id=" + gId,
                            method: "DELETE",
                            headers: {
                                Authorization: "Token " + token
                            },
                            success: function (res) {

                            },
                            error: function (err) {
                                console.log(err.responseText);
                            }
                        });
                    }
                }
                if ($("#file-photo")[0].files.length > 0) {
                    var vForm = getPhotoInputData();
                    $.ajax({
                        url: "http://localhost:8080/upload/region-link-photo",
                        method: "POST",
                        headers: {
                            Authorization: "Token " + token
                        },
                        async: false,
                        processData: false,
                        mimeType: "multipart/form-data",
                        contentType: false,
                        data: vForm,
                        success: function (res) {
                            var vNames = JSON.parse(res);
                            gPhoto += gPhoto == "" ? vNames.join(",") : "," + vNames.join(",");
                            vRegionLink.photo = gPhoto;
                        },
                        error: function (err) {
                            console.log(err.responseText);
                        }
                    });
                } else {
                    vRegionLink.photo = gPhoto;
                }

                $.ajax({
                    url: "http://localhost:8080/region-links/" + gId,
                    method: "PUT",
                    headers: {
                        Authorization: "Token " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vRegionLink),
                    success: function (res) {
                        window.location.href = "region-links.html";
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            $("#modal-login").modal("show");
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đang nhập, vui lòng đăng nhập."
            });
        }
    }

    //Hàm xử lý khi ấn nút Hủy bỏ
    function onBtnCancelClick() {
        "use strict";
        window.history.back();
    }


    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm xử lý hiển thị chi tiết mặt bằng điển hình
    function loadDetailData(paramData) {
        "use strict";
        $("#inp-name").val(paramData.name);
        if (paramData.photo != "" && paramData.photo != null) {
            gPhoto = paramData.photo;
            var vListPhotos = gPhoto.split(",");
            for (var bI = 0; bI < vListPhotos.length; bI++) {
                $("#card-images").append(
                    "<div class='col'>"
                    + "<div class='card border-0'>"
                    + "<img src='http://localhost:8080/download/region-link-photo/" + vListPhotos[bI] + "' class='card-img-top' style='object-fit: contain;' alt='...'>"
                    + "<button data-photo-name='" + vListPhotos[bI] + "' class='btn border-0 delete-server-image' style='position: absolute; top: 0%; left: 80%;'><i class='text-danger fas fa-window-close'></i></button>"
                    + "</div>"
                    + "</div>"
                );
            }
        }
        $("#inp-description").val(paramData.description);
        $("#inp-address").val(paramData.address);
        $("#inp-lat").val(paramData.lat);
        $("#inp-lng").val(paramData.lng);
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm lấy dữ liệu hình ảnh để upload
    function getPhotoInputData() {
        var formData = new FormData();
        var vFiles = $("#file-photo")[0].files;
        for (var bI = 0; bI < vFiles.length; bI++) {
            formData.append("files", vFiles[bI]);
        }
        return formData;
    }

    //Hàm lấy dữ liệu vị trí liên kết
    function getRegionLinkData(paramRegionLink) {
        paramRegionLink.name = $("#inp-name").val();
        paramRegionLink.description = $("#inp-description").val();
        paramRegionLink.address = $("#inp-address").val();
        paramRegionLink.lat = $("#inp-lat").val();
        paramRegionLink.lng = $("#inp-lng").val();
    }

    //Hàm lấy kiểm tra dữ liệu vị trí liên kết
    function validateRegionLinkData(paramRegionLink) {
        "use strict";
        if (paramRegionLink.name.trim() == "") {
            gToast.fire({
                icon: "warning",
                title: "Nhập tên vị trí liên kết."
            });
            return false;
        }
        return true;
    }
});