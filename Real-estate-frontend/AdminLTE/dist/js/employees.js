"use strict";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gTotalPages = 0;
    var gPage = 0;
    var gPageSize = 10;
    var gId = "";
    const gTable = $("#tbl-employees");
    const gNameCol = ["action", "id", "lastName", "firstName", "title", "titleOfCourtesy", "birthDate", "hireDate", "address", "city", "region", "postalCode", "country", "homePhone", "extension", "reportsTo", "username", "password", "email", "activated", "userLevel"];
    const gACTION_COL = 0;
    const gID_COL = 1;
    const gLAST_NAME_COL = 2;
    const gFIRST_NAME_COL = 3;
    const gTITLE_COL = 4;
    const gTITLE_OF_COURTESY_COL = 5;
    const gBIRTH_DATE_COL = 6;
    const gHIRE_DATE_COL = 7;
    const gADDRESS_COL = 8;
    const gCITY_COL = 9;
    const gREGION_COL = 10;
    const gPOSTAL_CODE_COL = 11;
    const gCOUNTRY_COL = 12;
    const gHOME_PHONE_COL = 13;
    const gEXTENSION_COL = 14;
    const gREPORTS_TO_COL = 15;
    const gUSERNAME_COL = 16;
    const gPASSWORD_COL = 17;
    const gEMAIL_COL = 18;
    const gACTIVATED_COL = 19;
    const gUSER_LEVEL_COL = 20;
    var gDataTable = gTable.DataTable({
        columns: [
            { data: gNameCol[gACTION_COL] },
            { data: gNameCol[gID_COL] },
            { data: gNameCol[gLAST_NAME_COL] },
            { data: gNameCol[gFIRST_NAME_COL] },
            { data: gNameCol[gTITLE_COL] },
            { data: gNameCol[gTITLE_OF_COURTESY_COL] },
            { data: gNameCol[gBIRTH_DATE_COL] },
            { data: gNameCol[gHIRE_DATE_COL] },
            { data: gNameCol[gADDRESS_COL] },
            { data: gNameCol[gCITY_COL] },
            { data: gNameCol[gREGION_COL] },
            { data: gNameCol[gPOSTAL_CODE_COL] },
            { data: gNameCol[gCOUNTRY_COL] },
            { data: gNameCol[gHOME_PHONE_COL] },
            { data: gNameCol[gEXTENSION_COL] },
            { data: gNameCol[gREPORTS_TO_COL] },
            { data: gNameCol[gUSERNAME_COL] },
            { data: gNameCol[gPASSWORD_COL] },
            { data: gNameCol[gEMAIL_COL] },
            { data: gNameCol[gACTIVATED_COL] },
            { data: gNameCol[gUSER_LEVEL_COL] }
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: "<a class='btn detail-employee' title='Chi tiết'><i class='text-info fas fa-info'></i></a><a class='btn edit-employee' title='Sửa'><i class='text-primary fas fa-edit'></i></a><a class='btn delete-employee' title='Xóa'><i class='text-danger fas fa-trash'></i></a>",
                className: "text-nowrap"

            },
            {
                targets: gTITLE_COL,
                className: "text-nowrap"
            },
            {
                targets: gBIRTH_DATE_COL,
                className: "text-nowrap"
            },
            {
                targets: gHIRE_DATE_COL,
                className: "text-nowrap"
            },
            {
                targets: gHOME_PHONE_COL,
                className: "text-nowrap"
            },
            {
                targets: gADDRESS_COL,
                width: 300
            },
            {
                targets: gREPORTS_TO_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.id + " - " + data.username;
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gUSER_LEVEL_COL,
                render: function (data, type) {
                    return stringUserLevel(data);
                }
            }
        ],
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        scrollY: 350,
        scrollX: true,
    });

    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút add
    $("#add-employee").on("click", function () {
        onAddEmployeeClick();
    });

    //Gán sự kiện cho nút chi tiết
    $("#tbl-employees tbody").on("click", ".detail-employee", function () {
        onDetailEmployeeClick(this);
    });

    //Gán sự kiện cho nút edit
    $("#tbl-employees tbody").on("click", ".edit-employee", function () {
        onEditEmployeeClick(this);
    });

    //Gán sự kiện cho nút xóa
    $("#tbl-employees tbody").on("click", ".delete-employee", function () {
        onDeleteEmployeeClick(this);
    });

    //Gán sự kiện cho nút Xóa trên modal
    $("#btn-delete-confirm").on("click", onBtnDeleteConfirmClick);

    //Gán sự kiện cho các nút phân trang
    $("#number-page").on("keypress", function (event) {
        if (event.which == 13) {
            onNumberPageEnter();
        }
    });
    $("#sel-page-size").on("change", onChangePageSize);
    $("#start-page").on("click", onStartPageClick);
    $("#previous-page").on("click", onPreviousPageClick);
    $("#next-page").on("click", onNextPageClick);
    $("#end-page").on("click", onEndPageClick);

    //Điều chỉnh lại các cột table khi thay đổi kích thước window
    $(window).resize(function () {
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });


    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        loadEmployeeDataToTable();
    }

    // Hàm xử lý khi click nút add
    function onAddEmployeeClick() {
        "use strict";
        window.location.href = "add-employee.html"
    }

    // Hàm xử lý khi click nút chi tiết
    function onDetailEmployeeClick(paramBtnDetail) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnDetail.closest("tr")).data().id;
        var token = getCookie("token");
        if (token) {
            var headers = {
                Authorization: "Token " + token
            };
            $.ajax({
                url: "http://localhost:8080/employees/" + gId,
                method: "GET",
                headers: headers,
                success: function (res) {
                    loadDetailData(res);
                    $("#modal-detail").modal("show");
                }, error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }


    // Hàm xử lý khi click nút edit
    function onEditEmployeeClick(paramBtnEdit) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnEdit.closest("tr")).data().id;
        var token = getCookie("token");
        if (token) {
            window.location.href = "edit-employee.html?id=" + gId;
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }
    }

    // Hàm xử lý khi click nút xóa
    function onDeleteEmployeeClick(paramBtnDelete) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnDelete.closest("tr")).data().id;
        var token = getCookie("token");
        if (token) {
            $("#modal-delete").modal("show");
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }
    }

    // Hàm xử lý khi click nút xóa trên modal
    function onBtnDeleteConfirmClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: "http://localhost:8080/employees/" + gId,
                method: "DELETE",
                headers: {
                    Authorization: "Token " + token
                },
                success: function (res) {
                    $("#modal-delete").modal("hide");
                    loadEmployeeDataToTable();
                }, error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }
    }

    function onNumberPageEnter() {
        var vPage = $("#number-page").val();
        if (vPage < 1) {
            gPage = 0;
            loadEmployeeDataToTable();
        } else if (vPage > gTotalPages && gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadEmployeeDataToTable();
        } else {
            gPage = vPage - 1;
            loadEmployeeDataToTable();
        }
    }

    function onChangePageSize() {
        gPage = 0;
        gPageSize = $("#sel-page-size").val();
        loadEmployeeDataToTable();
    }

    //Hàm xử lý khi ấn nút Trang đầu
    function onStartPageClick() {
        if (gTotalPages > 0) {
            gPage = 0;
            loadEmployeeDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang trước
    function onPreviousPageClick() {
        if (gTotalPages > 0 && gPage > 0) {
            gPage--;
            loadEmployeeDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang sau
    function onNextPageClick() {
        if (gTotalPages > 0 && gPage < gTotalPages - 1) {
            gPage++;
            loadEmployeeDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang cuối
    function onEndPageClick() {
        if (gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadEmployeeDataToTable();
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm load dữ liệu customer vào bảng
    function loadEmployeeDataToTable() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var headers = {
                Authorization: "Token " + token
            };
            $.ajax({
                url: "http://localhost:8080/employees?page=" + gPage + "&size=" + gPageSize,
                method: "GET",
                headers: headers,
                success: function (res) {
                    var vDataTable = gTable.DataTable();
                    vDataTable.clear();
                    vDataTable.rows.add(res.content);
                    vDataTable.draw();
                    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
                    gTotalPages = res.totalPages;
                    $("#number-page").val(gPage + 1);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm xử lý hiển thị chi tiết bất động sản
    function loadDetailData(paramData) {
        "use strict";
        $("#p-id").html(paramData.id);
        $("#p-last-name").html(paramData.lastName);
        $("#p-first-name").html(paramData.firstName);
        $("#p-title").html(paramData.title);
        $("#p-title-of-courtesy").html(paramData.titleOfCourtesy);
        $("#p-birth-date").html(paramData.birthDate);
        $("#p-hire-date").html(paramData.hireDate);
        $("#p-address").html(paramData.address);
        $("#p-city").html(paramData.city);
        $("#p-region").html(paramData.region);
        $("#p-postal-code").html(paramData.postalCode);
        $("#p-country").html(paramData.country);
        $("#p-home-phone").html(paramData.homePhone);
        $("#p-extension").html(paramData.extension);
        if (paramData.photo != "" && paramData.photo != null) {
            $("#p-photo").html("<img src='http://localhost:8080/download/employee-photo/" + paramData.photo + "' class='d-block w-100' style='height: 100px; object-fit: contain;' alt='...'>");
        } else {
            $("#p-photo").html("");
        }
        $("#p-notes").html(paramData.notes);
        if (paramData.reportsTo != null) {
            $("#p-reports-to").html(paramData.reportsTo.id + " - " + paramData.reportsTo.username + " - " + paramData.reportsTo.firstName + " " + paramData.reportsTo.lastName);
        } else {
            $("#p-reports-to").html("");
        }
        $("#p-username").html(paramData.username);
        $("#p-password").html(paramData.password);
        $("#p-email").html(paramData.email);
        $("#p-activated").html(paramData.activated);
        $("#p-user-level").html(stringUserLevel(paramData.userLevel));
    }

    function stringUserLevel(paramData) {
        "use strict";
        var vString = "";
        switch (paramData) {
            case 1:
                vString = "Administrator";
                break;
            case 2:
                vString = "HomeSeller";
                break;
            case 3:
                vString = "Default";
                break;
            default:
                vString = "";
        }
        return vString;
    }
});