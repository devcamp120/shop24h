$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gId = "";
    var gProvinceId = "";
    var gDistrictId = "";
    var gWardId = "";
    var gStreetId = "";
    var gInvestorId = "";
    var gConstructionContractorId = "";
    var gDesignUnitId = "";
    var gPhoto = "";
    var gDeletePhoto = [];

    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho Select Tỉnh / Thành phố
    $("#sel-province").on("change", onSelectProvinceChange);
    //Gán sự kiện cho Select Quận / Huyện
    $("#sel-district").on("change", onSelectDistrictChange);

    //Gán sự kiện cho nút Lưu lại
    $("#btn-save").on("click", onBtnSaveClick);

    //Gán sự kiện cho nút Hủy bỏ
    $("#btn-cancel").on("click", onBtnCancelClick);

    $("#card-images").on("click", ".delete-server-image", function () {
        var vListPhotos = gPhoto.split(",");
        var vPhotoName = $(this)[0].dataset.photoName;
        var index = vListPhotos.indexOf(vPhotoName);
        if (index !== -1) {
            vListPhotos.splice(index, 1);
        }
        gDeletePhoto.push(vPhotoName);
        gPhoto = vListPhotos.join(",");
        $(this).parent().parent().remove();
    });

    $("#card-images").on("click", ".delete-local-image", function () {
        $("#file-photo").val("");
        $("#card-images .delete-local-image").parent().parent().remove();
    });

    $("#file-photo").on("change", function () {
        $("#card-images .delete-local-image").parent().parent().remove();
        var vFiles = this.files;
        for (var bI = 0; bI < vFiles.length; bI++) {
            if (vFiles[bI]) {
                let reader = new FileReader();
                reader.onload = function (event) {
                    $("#card-images").append(
                        "<div class='col'>"
                        + "<div class='card border-0'>"
                        + "<img src='" + event.target.result + "' class='card-img-top' style='object-fit: contain;' alt='...'>"
                        + "<button class='btn border-0 delete-local-image' style='position: absolute; top: 0%; left: 80%;'><i class='text-danger fas fa-window-close'></i></button>"
                        + "</div>"
                        + "</div>"
                    );
                }
                reader.readAsDataURL(vFiles[bI]);
            }
        }
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        const vUrlParams = new URLSearchParams(window.location.search);
        gId = vUrlParams.get("id");

        $(".select2bs4").select2({
            theme: "bootstrap4"
        });

        $.ajax({
            url: "http://localhost:8080/provinces/all",
            method: "GET",
            async: false,
            success: function (res) {
                loadDataToSelectProvince(res);
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });

        $.ajax({
            url: "http://localhost:8080/investors/all",
            method: "GET",
            async: false,
            success: function (res) {
                loadDataToSelectInvestor(res);
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });

        $.ajax({
            url: "http://localhost:8080/construction-contractors/all",
            method: "GET",
            async: false,
            success: function (res) {
                loadDataToSelectConstructionContractor(res);
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });

        $.ajax({
            url: "http://localhost:8080/design-units/all",
            method: "GET",
            async: false,
            success: function (res) {
                loadDataToSelectDesignUnit(res);
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });

        $.ajax({
            url: "http://localhost:8080/utilities/all",
            method: "GET",
            async: false,
            success: function (res) {
                loadDataToSelectUtility(res);
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });

        $.ajax({
            url: "http://localhost:8080/region-links/all",
            method: "GET",
            async: false,
            success: function (res) {
                loadDataToSelectRegionLink(res);
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });

        if (gId != "" || gId != null) {
            $.ajax({
                url: "http://localhost:8080/projects/" + gId,
                method: "GET",
                success: function (res) {
                    loadDetailData(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }

    //Hàm xử lý khi ấn nút Lưu lại
    function onBtnSaveClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vProject = {
                name: "",
                address: "",
                slogan: "",
                description: "",
                acreage: "",
                constructArea: "",
                numBlock: "",
                numFloors: "",
                numApartment: "",
                apartmenttArea: "",
                utilities: "",
                regionLink: "",
                lat: "",
                lng: ""
            }
            //Lấy dữ liệu
            getProjectData(vProject);
            //Kiểm tra dữ liệu
            var vIsCheck = validateProjectData(vProject);

            if (vIsCheck) {
                if (gDeletePhoto.length > 0) {
                    for (var bI = 0; bI < gDeletePhoto.length; bI++) {
                        $.ajax({
                            url: "http://localhost:8080/delete/project-photo/" + gDeletePhoto[bI] + "?id=" + gId,
                            method: "DELETE",
                            headers: {
                                Authorization: "Token " + token
                            },
                            success: function (res) {

                            },
                            error: function (err) {
                                console.log(err.responseText);
                            }
                        });
                    }
                }
                if ($("#file-photo")[0].files.length > 0) {
                    var vForm = getPhotoInputData();
                    $.ajax({
                        url: "http://localhost:8080/upload/project-photo",
                        method: "POST",
                        headers: {
                            Authorization: "Token " + token
                        },
                        async: false,
                        processData: false,
                        mimeType: "multipart/form-data",
                        contentType: false,
                        data: vForm,
                        success: function (res) {
                            var vNames = JSON.parse(res);
                            gPhoto += gPhoto == "" ? vNames.join(",") : "," + vNames.join(",");
                            vProject.photo = gPhoto;
                        },
                        error: function (err) {
                            console.log(err.responseText);
                        }
                    });
                } else {
                    vProject.photo = gPhoto;
                }

                $.ajax({
                    url: "http://localhost:8080/projects/" + gId + "?provinceId=" + gProvinceId + "&districtId=" + gDistrictId + "&wardId=" + gWardId + "&streetId=" + gStreetId + "&investorId=" + gInvestorId + "&constructionContractorId=" + gConstructionContractorId + "&designUnitId=" + gDesignUnitId,
                    method: "PUT",
                    headers: {
                        Authorization: "Token " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vProject),
                    success: function (res) {
                        window.location.href = "projects.html";
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }

    }

    //Hàm xử lý khi ấn nút Hủy bỏ
    function onBtnCancelClick() {
        "use strict";
        window.history.back();
    }

    //Hàm xử lý khi chọn Tỉnh / Thành phố
    function onSelectProvinceChange() {
        var vProvinceId = $("#sel-province").val();
        $("#sel-district").html("");
        $("#sel-ward").html("");
        $("#sel-street").html("");
        $("#sel-district").append($("<option>").val("-1").text("-- Quận huyện --"));
        $("#sel-ward").append($("<option>").val("-1").text("-- Phường xã --"));
        $("#sel-street").append($("<option>").val("-1").text("-- Đường phố --"));
        if (vProvinceId != "-1") {
            $.ajax({
                url: "http://localhost:8080/provinces/" + vProvinceId + "/districts",
                method: "GET",
                async: false,
                success: function (res) {
                    loadDataToSelectDistrict(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }

    //Hàm xử lý khi chọn Quận / Huyện
    function onSelectDistrictChange() {
        var vDistrictId = $("#sel-district").val();
        $("#sel-ward").html("");
        $("#sel-street").html("");
        $("#sel-ward").append($("<option>").val("-1").text("-- Phường xã --"));
        $("#sel-street").append($("<option>").val("-1").text("-- Đường phố --"));
        if (vDistrictId != "-1") {
            $.ajax({
                url: "http://localhost:8080/districts/" + vDistrictId + "/wards",
                method: "GET",
                async: false,
                success: function (res) {
                    loadDataToSelectWard(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
            $.ajax({
                url: "http://localhost:8080/districts/" + vDistrictId + "/streets",
                method: "GET",
                async: false,
                success: function (res) {
                    loadDataToSelectStreet(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm xử lý hiển thị chi tiết dự án
    function loadDetailData(paramData) {
        "use strict";
        $("#inp-name").val(paramData.name);
        if (paramData.photo != "" && paramData.photo != null) {
            gPhoto = paramData.photo;
            var vListPhotos = gPhoto.split(",");
            for (var bI = 0; bI < vListPhotos.length; bI++) {
                $("#card-images").append(
                    "<div class='col'>"
                    + "<div class='card border-0'>"
                    + "<img src='http://localhost:8080/download/project-photo/" + vListPhotos[bI] + "' class='card-img-top' style='object-fit: contain;' alt='...'>"
                    + "<button data-photo-name='" + vListPhotos[bI] + "' class='btn border-0 delete-server-image' style='position: absolute; top: 0%; left: 80%;'><i class='text-danger fas fa-window-close'></i></button>"
                    + "</div>"
                    + "</div>"
                );
            }
        }
        if (paramData.province != null) {
            $("#sel-province").val(paramData.province.id).trigger("change");
            if (paramData.district != null) {
                $("#sel-district").val(paramData.district.id).trigger("change");
                if (paramData.ward != null) {
                    $("#sel-ward").val(paramData.ward.id).trigger("change");
                } else {
                    $("#sel-ward").val("-1").trigger("change");
                }
                if (paramData.street != null) {
                    $("#sel-street").val(paramData.street.id).trigger("change");
                } else {
                    $("#sel-street").val("-1").trigger("change");
                }
            } else {
                $("#sel-district").val("-1").trigger("change");
            }
        } else {
            $("#sel-province").val("-1").trigger("change");
        }
        if (paramData.investor != null) {
            $("#sel-investor").val(paramData.investor.id).trigger("change");
        } else {
            $("#sel-investor").val("-1").trigger("change");
        }
        if (paramData.constructionContractor != null) {
            $("#sel-constructionContractor").val(paramData.constructionContractor.id).trigger("change");
        } else {
            $("#sel-constructionContractor").val("-1").trigger("change");
        }
        if (paramData.designUnit != null) {
            $("#sel-designUnit").val(paramData.designUnit.id).trigger("change");
        } else {
            $("#sel-designUnit").val("-1").trigger("change");
        }
        $("#inp-address").val(paramData.address);
        $("#inp-slogan").val(paramData.slogan);
        $("#inp-description").val(paramData.description);
        $("#inp-acreage").val(paramData.acreage);
        $("#inp-constructArea").val(paramData.constructArea);
        $("#inp-numBlock").val(paramData.numBlock);
        $("#inp-numFloors").val(paramData.numFloors);
        $("#inp-numApartment").val(paramData.numApartment);
        $("#inp-apartmenttArea").val(paramData.apartmenttArea);
        $("#sel-utilities").val(paramData.utilities.split(",")).trigger("change");
        $("#sel-regionLink").val(paramData.regionLink.split(",")).trigger("change");
        $("#inp-lng").val(paramData.lng);
        $("#inp-lat").val(paramData.lat);
    }

    //Hàm load dữ liệu vào Select Tỉnh / Thành phố
    function loadDataToSelectProvince(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-province").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Quận / Huyện
    function loadDataToSelectDistrict(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-district").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Phường / Xã
    function loadDataToSelectWard(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-ward").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Đường phố
    function loadDataToSelectStreet(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-street").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Chủ đầu tư
    function loadDataToSelectInvestor(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-investor").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Nhà thầu xây dựng
    function loadDataToSelectConstructionContractor(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-constructionContractor").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Đơn vị thiết kế
    function loadDataToSelectDesignUnit(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-designUnit").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Tiện ích chung cư
    function loadDataToSelectUtility(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-utilities").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Vị trí liên kết vùng
    function loadDataToSelectRegionLink(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-regionLink").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm lấy dữ liệu hình ảnh để upload
    function getPhotoInputData() {
        var formData = new FormData();
        var vFiles = $("#file-photo")[0].files;
        for (var bI = 0; bI < vFiles.length; bI++) {
            formData.append("files", vFiles[bI]);
        }
        return formData;
    }

    //Hàm lấy dữ liệu dự án
    function getProjectData(paramProject) {
        gProvinceId = $("#sel-province").val();
        gDistrictId = $("#sel-district").val();
        gWardId = $("#sel-ward").val();
        gStreetId = $("#sel-street").val();
        gInvestorId = $("#sel-investor").val();
        gConstructionContractorId = $("#sel-constructionContractor").val();
        gDesignUnitId = $("#sel-designUnit").val();
        paramProject.name = $("#inp-name").val();
        paramProject.address = $("#inp-address").val();
        paramProject.slogan = $("#inp-slogan").val();
        paramProject.description = $("#inp-description").val();
        paramProject.acreage = $("#inp-acreage").val();
        paramProject.constructArea = $("#inp-constructArea").val();
        paramProject.numBlock = $("#inp-numBlock").val();
        paramProject.numFloors = $("#inp-numFloors").val();
        paramProject.numApartment = $("#inp-numApartment").val();
        paramProject.apartmenttArea = $("#inp-apartmenttArea").val();
        paramProject.utilities = $("#sel-utilities").val().join(",");
        paramProject.regionLink = $("#sel-regionLink").val().join(",");
        paramProject.lat = $("#inp-lat").val();
        paramProject.lng = $("#inp-lng").val();
    }

    //Hàm lấy kiểm tra dữ liệu dự án
    function validateProjectData(paramProject) {
        "use strict";
        if (paramProject.name.trim() == "") {
            gToast.fire({
                icon: "warning",
                title: "Nhập tên dự án."
            });
            return false;
        }
        if (paramProject.numApartment.trim() == "") {
            gToast.fire({
                icon: "warning",
                title: "Nhập tổng số căn hộ."
            });
            return false;
        }
        if (gInvestorId == "-1") {
            gToast.fire({
                icon: "warning",
                title: "Chọn chủ đầu tư."
            });
            return false;
        }
        if (paramProject.utilities == "") {
            gToast.fire({
                icon: "warning",
                title: "Chọn tiện ích chung cư."
            });
            return false;
        }
        if (paramProject.regionLink == "") {
            gToast.fire({
                icon: "warning",
                title: "Chọn vị trí liên kết vùng."
            });
            return false;
        }
        return true;
    }
});