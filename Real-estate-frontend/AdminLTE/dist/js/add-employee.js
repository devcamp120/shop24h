$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gReportsTo = "";

    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút Lưu lại
    $("#btn-save").on("click", onBtnSaveClick);

    //Gán sự kiện cho nút Hủy bỏ
    $("#btn-cancel").on("click", onBtnCancelClick);

    $("#card-images").on("click", ".delete-local-image", function () {
        $("#file-photo").val("");
        $("#card-images .delete-local-image").parent().parent().remove();
    });

    $("#file-photo").on("change", function () {
        $("#card-images .delete-local-image").parent().parent().remove();
        var vFiles = this.files;
        for (var bI = 0; bI < vFiles.length; bI++) {
            if (vFiles[bI]) {
                let reader = new FileReader();
                reader.onload = function (event) {
                    $("#card-images").append(
                        "<div class='col'>"
                        + "<div class='card border-0'>"
                        + "<img src='" + event.target.result + "' class='card-img-top' style='object-fit: contain;' alt='...'>"
                        + "<button class='btn border-0 delete-local-image' style='position: absolute; top: 0%; left: 85%;'><i class='text-danger fas fa-window-close'></i></button>"
                        + "</div>"
                        + "</div>"
                    );
                }
                reader.readAsDataURL(vFiles[bI]);
            }
        }
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";

        $(".select2bs4").select2({
            theme: "bootstrap4"
        });

        $("#sel-reports-to").select2({
            placeholder: "-- Enter full name or username --",
            minimumInputLength: 2,
            allowClear: true,
            ajax: {
                url: "http://localhost:8080/employees/search",
                type: "GET",
                dataType: "json",
                headers: {
                    Authorization: "Token " + getCookie("token")
                },
                delay: 250,
                data: function (params) {
                    return {
                        keyword: params.term
                    };
                },
                processResults: function (data, params) {
                    return {
                        results: $.map(data, function (employee) {
                            return {
                                text: employee.id + " - " + employee.firstName + " " + employee.lastName + " - " + employee.username,
                                id: employee.id,
                                data: employee
                            };
                        })
                    };
                }
            }
        });
    }

    //Hàm xử lý khi ấn nút Lưu lại
    function onBtnSaveClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vEmployee = {
                lastName: "",
                firstName: "",
                title: "",
                titleOfCourtesy: "",
                birthDate: "",
                hireDate: "",
                address: "",
                city: "",
                region: "",
                postalCode: "",
                country: "",
                homePhone: "",
                extension: "",
                photo: "",
                notes: "",
                username: "",
                password: "",
                email: "",
                activated: "",
                profile: "",
                userLevel: ""
            }
            //Lấy dữ liệu
            getEmployeeData(vEmployee);
            var vForm = getPhotoInputData();
            //Kiểm tra dữ liệu
            var vIsCheck = validateEmployeeData(vEmployee);

            if (vIsCheck) {
                if ($("#file-photo")[0].files.length > 0) {
                    var vForm = getPhotoInputData();
                    $.ajax({
                        url: "http://localhost:8080/upload/employee-photo",
                        method: "POST",
                        headers: {
                            Authorization: "Token " + token
                        },
                        async: false,
                        processData: false,
                        mimeType: "multipart/form-data",
                        contentType: false,
                        data: vForm,
                        success: function (res) {
                            vEmployee.photo = res;
                        },
                        error: function (err) {
                            console.log(err.responseText);
                        }
                    });
                }
                $.ajax({
                    url: "http://localhost:8080/employees" + "?reportsTo=" + gReportsTo,
                    method: "POST",
                    headers: {
                        Authorization: "Token " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vEmployee),
                    success: function (res) {
                        window.location.href = "employees.html";
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            $("#modal-login").modal("show");
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập, vui lòng đăng nhập."
            });
        }
    }

    //Hàm xử lý khi ấn nút Hủy bỏ
    function onBtnCancelClick() {
        "use strict";
        window.history.back();
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm lấy dữ liệu hình ảnh để upload
    function getPhotoInputData() {
        var formData = new FormData();
        var vFiles = $("#file-photo")[0].files;
        formData.append("file", vFiles[0]);
        return formData;
    }

    //Hàm lấy dữ liệu nhân viên
    function getEmployeeData(paramEmployee) {
        paramEmployee.lastName = $("#inp-last-name").val().trim();
        paramEmployee.firstName = $("#inp-first-name").val().trim();
        paramEmployee.title = $("#inp-title").val();
        paramEmployee.titleOfCourtesy = $("#inp-title-of-courtesy").val();
        paramEmployee.birthDate = $("#inp-birth-date").val();
        paramEmployee.hireDate = $("#inp-hire-date").val();
        paramEmployee.address = $("#inp-address").val();
        paramEmployee.city = $("#inp-city").val();
        paramEmployee.region = $("#inp-region").val();
        paramEmployee.postalCode = $("#inp-postal-code").val();
        paramEmployee.country = $("#inp-country").val();
        paramEmployee.homePhone = $("#inp-home-phone").val();
        paramEmployee.extension = $("#inp-extension").val();
        paramEmployee.notes = $("#inp-notes").val();
        if ($("#sel-reports-to").val() == null) {
            gReportsTo = "-1";
        } else {
            gReportsTo = $("#sel-reports-to").val();
        }
        paramEmployee.username = $("#inp-username").val().trim();
        paramEmployee.password = $("#inp-password").val().trim();
        paramEmployee.email = $("#inp-email").val().trim();
        var vActivated = $("#cbx-activated").is(":checked");
        if (vActivated) {
            paramEmployee.activated = "Y";
        } else {
            paramEmployee.activated = "N";
        }
        paramEmployee.profile = $("#inp-profile").val();
        paramEmployee.userLevel = $("#sel-user-level").val();
    }

    //Hàm lấy kiểm tra dữ liệu nhân viên
    function validateEmployeeData(paramEmployee) {
        "use strict";
        if (paramEmployee.lastName == "") {
            gToast.fire({
                icon: "warning",
                title: "Nhập họ."
            });
            return false;
        }
        if (paramEmployee.firstName == "") {
            gToast.fire({
                icon: "warning",
                title: "Nhật tên."
            });
            return false;
        }
        if (paramEmployee.username == "") {
            gToast.fire({
                icon: "warning",
                title: "Nhập username."
            });
            return false;
        }
        if (!checkUsername(paramEmployee.username)) {
            gToast.fire({
                icon: "warning",
                title: "Tên người dùng đã tồn tại"
            });
            return false;
        }
        if (paramEmployee.password == "") {
            gToast.fire({
                icon: "warning",
                title: "Nhập mật khẩu."
            });
            return false;
        }
        if (paramEmployee.email == "") {
            gToast.fire({
                icon: "warning",
                title: "Nhập email."
            });
            return false;
        }
        return true;
    }

    //Hàm kiểm tra username
    function checkUsername(pramUsername) {
        "use strict";
        var vIsCheck = false;
        $.ajax({
            url: "http://localhost:8080/users/" + pramUsername + "/exists",
            method: "GET",
            async: false,
            success: function (res) {
                vIsCheck = !res;
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });
        return vIsCheck;
    }
});