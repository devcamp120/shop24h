"use strict";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gTotalPages = 0;
    var gPage = 0;
    var gPageSize = 10;
    var gId = "";
    const gTable = $("#tbl-streets");
    const gNameCol = ["action", "id", "name", "prefix", "province", "district"];
    const gACTION_COL = 0;
    const gID_COL = 1;
    const gNAME_COL = 2;
    const gPREFIX_COL = 3;
    const gPROVINCE_COL = 4;
    const gDISTRICT_COL = 5;
    var gDataTable = gTable.DataTable({
        columns: [
            { data: gNameCol[gACTION_COL] },
            { data: gNameCol[gID_COL] },
            { data: gNameCol[gNAME_COL] },
            { data: gNameCol[gPREFIX_COL] },
            { data: gNameCol[gPROVINCE_COL] },
            { data: gNameCol[gDISTRICT_COL] }
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: "<a class='btn edit-street' title='Sửa'><i class='text-primary fas fa-edit'></i></a><a class='btn delete-street' title='Xóa'><i class='text-danger fas fa-trash'></i></a>",
                className: "text-nowrap"

            },
            {
                targets: gNAME_COL,
                className: "text-nowrap"
            },
            {
                targets: gPROVINCE_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.name;
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gDISTRICT_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.name;
                    }
                    return "";
                },
                className: "text-nowrap"
            }
        ],
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        scrollY: 350,
        scrollX: true,
    });

    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút add
    $("#add-street").on("click", function () {
        onAddStreetClick();
    });

    //Gán sự kiện cho nút edit
    $("#tbl-streets tbody").on("click", ".edit-street", function () {
        onEditStreetClick(this);
    });

    //Gán sự kiện cho nút xóa
    $("#tbl-streets tbody").on("click", ".delete-street", function () {
        onDeleteStreetClick(this);
    });

    //Gán sự kiện cho nút Xóa trên modal
    $("#btn-delete-confirm").on("click", onBtnDeleteConfirmClick);

    //Gán sự kiện cho các nút phân trang
    $("#number-page").on("keypress", function (event) {
        if (event.which == 13) {
            onNumberPageEnter();
        }
    });
    $("#sel-page-size").on("change", onChangePageSize);
    $("#start-page").on("click", onStartPageClick);
    $("#previous-page").on("click", onPreviousPageClick);
    $("#next-page").on("click", onNextPageClick);
    $("#end-page").on("click", onEndPageClick);

    //Điều chỉnh lại các cột table khi thay đổi kích thước window
    $(window).resize(function () {
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });


    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        loadStreetDataToTable();
    }

    // Hàm xử lý khi click nút add
    function onAddStreetClick() {
        "use strict";
        window.location.href = "add-street.html"
    }


    // Hàm xử lý khi click nút edit
    function onEditStreetClick(paramBtnEdit) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnEdit.closest("tr")).data().id;
        var token = getCookie("token");
        if (token) {
            window.location.href = "edit-street.html?id=" + gId;
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }
    }

    // Hàm xử lý khi click nút xóa
    function onDeleteStreetClick(paramBtnDelete) {
        "use strict";
        gId = gTable.DataTable().row(paramBtnDelete.closest("tr")).data().id;
        var token = getCookie("token");
        if (token) {
            $("#modal-delete").modal("show");
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }
    }

    // Hàm xử lý khi click nút xóa trên modal
    function onBtnDeleteConfirmClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: "http://localhost:8080/streets/" + gId,
                method: "DELETE",
                headers: {
                    Authorization: "Token " + token
                },
                success: function (res) {
                    $("#modal-delete").modal("hide");
                    loadStreetDataToTable();
                }, error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đăng nhập."
            });
        }
    }

    function onNumberPageEnter() {
        var vPage = $("#number-page").val();
        if (vPage < 1) {
            gPage = 0;
            loadStreetDataToTable();
        } else if (vPage > gTotalPages && gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadStreetDataToTable();
        } else {
            gPage = vPage - 1;
            loadStreetDataToTable();
        }
    }

    function onChangePageSize() {
        gPage = 0;
        gPageSize = $("#sel-page-size").val();
        loadStreetDataToTable();
    }

    //Hàm xử lý khi ấn nút Trang đầu
    function onStartPageClick() {
        if (gTotalPages > 0) {
            gPage = 0;
            loadStreetDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang trước
    function onPreviousPageClick() {
        if (gTotalPages > 0 && gPage > 0) {
            gPage--;
            loadStreetDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang sau
    function onNextPageClick() {
        if (gTotalPages > 0 && gPage < gTotalPages - 1) {
            gPage++;
            loadStreetDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang cuối
    function onEndPageClick() {
        if (gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadStreetDataToTable();
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm load dữ liệu Đường phố vào bảng
    function loadStreetDataToTable() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var headers = {
                Authorization: "Token " + token
            };
            $.ajax({
                url: "http://localhost:8080/streets?page=" + gPage + "&size=" + gPageSize,
                method: "GET",
                headers: headers,
                success: function (res) {
                    var vDataTable = gTable.DataTable();
                    vDataTable.clear();
                    vDataTable.rows.add(res.content);
                    vDataTable.draw();
                    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
                    gTotalPages = res.totalPages;
                    $("#number-page").val(gPage + 1);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
});