$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gProjectId = "";

    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút Lưu lại
    $("#btn-save").on("click", onBtnSaveClick);

    //Gán sự kiện cho nút Hủy bỏ
    $("#btn-cancel").on("click", onBtnCancelClick);

    $("#card-images").on("click", ".delete-local-image", function () {
        $("#file-photo").val("");
        $("#card-images .delete-local-image").parent().parent().remove();
    });

    $("#file-photo").on("change", function () {
        $("#card-images .delete-local-image").parent().parent().remove();
        var vFiles = this.files;
        for (var bI = 0; bI < vFiles.length; bI++) {
            if (vFiles[bI]) {
                let reader = new FileReader();
                reader.onload = function (event) {
                    $("#card-images").append(
                        "<div class='col'>"
                        + "<div class='card border-0'>"
                        + "<img src='" + event.target.result + "' class='card-img-top' style='object-fit: contain;' alt='...'>"
                        + "<button class='btn border-0 delete-local-image' style='position: absolute; top: 0%; left: 85%;'><i class='text-danger fas fa-window-close'></i></button>"
                        + "</div>"
                        + "</div>"
                    );
                }
                reader.readAsDataURL(vFiles[bI]);
            }
        }
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";

        $(".select2bs4").select2({
            theme: "bootstrap4"
        });

        $.ajax({
            url: "http://localhost:8080/projects/all",
            method: "GET",
            async: false,
            success: function (res) {
                loadDataToSelectProject(res);
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });
    }

    //Hàm xử lý khi ấn nút Lưu lại
    function onBtnSaveClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vMasterLayout = {
                name: "",
                description: "",
                acreage: "",
                apartmentList: ""
            }
            //Lấy dữ liệu
            getMasterLayoutData(vMasterLayout);
            //Kiểm tra dữ liệu
            var vIsCheck = validateMasterLayoutData(vMasterLayout);

            if (vIsCheck) {
                if ($("#file-photo")[0].files.length > 0) {
                    var vForm = getPhotoInputData();
                    $.ajax({
                        url: "http://localhost:8080/upload/master-layout-photo",
                        method: "POST",
                        headers: {
                            Authorization: "Token " + token
                        },
                        async: false,
                        processData: false,
                        mimeType: "multipart/form-data",
                        contentType: false,
                        data: vForm,
                        success: function (res) {
                            var vNames = JSON.parse(res);
                            vMasterLayout.photo = vNames.join(",");
                        },
                        error: function (err) {
                            console.log(err.responseText);
                        }
                    });
                }

                $.ajax({
                    url: "http://localhost:8080/master-layouts" + "?projectId=" + gProjectId,
                    method: "POST",
                    headers: {
                        Authorization: "Token " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vMasterLayout),
                    success: function (res) {
                        window.location.href = "master-layouts.html";
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            $("#modal-login").modal("show");
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đang nhập, vui lòng đăng nhập."
            });
        }
    }

    //Hàm xử lý khi ấn nút Hủy bỏ
    function onBtnCancelClick() {
        "use strict";
        window.history.back();
    }


    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm load dữ liệu vào Select Dự án
    function loadDataToSelectProject(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-project").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm lấy dữ liệu hình ảnh để upload
    function getPhotoInputData() {
        var formData = new FormData();
        var vFiles = $("#file-photo")[0].files;
        for (var bI = 0; bI < vFiles.length; bI++) {
            formData.append("files", vFiles[bI]);
        }
        return formData;
    }

    //Hàm lấy dữ liệu mặt bằng điển hình
    function getMasterLayoutData(paramMasterLayout) {
        gProjectId = $("#sel-project").val();
        paramMasterLayout.name = $("#inp-name").val();
        paramMasterLayout.description = $("#inp-description").val();
        paramMasterLayout.acreage = $("#inp-acreage").val();
        paramMasterLayout.apartmentList = $("#inp-apartmentList").val();
    }

    //Hàm lấy kiểm tra dữ liệu mặt bằng điển hình
    function validateMasterLayoutData(paramMasterLayout) {
        "use strict";
        if (paramMasterLayout.name.trim() == "") {
            gToast.fire({
                icon: "warning",
                title: "Nhập tên mặt bằng điển hình."
            });
            return false;
        }
        if (gProjectId == "-1") {
            gToast.fire({
                icon: "warning",
                title: "Chọn dự án."
            });
            return false;
        }
        return true;
    }
});