$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gId = "";
    var gAddressId = "";

    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút Lưu lại
    $("#btn-save").on("click", onBtnSaveClick);

    //Gán sự kiện cho nút Hủy bỏ
    $("#btn-cancel").on("click", onBtnCancelClick);

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        const vUrlParams = new URLSearchParams(window.location.search);
        gId = vUrlParams.get("id");

        $(".select2bs4").select2({
            theme: "bootstrap4"
        });

        $.ajax({
            url: "http://localhost:8080/address-maps/all",
            method: "GET",
            async: false,
            success: function (res) {
                loadDataToSelectAddress(res);
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });

        if (gId != "" || gId != null) {
            $.ajax({
                url: "http://localhost:8080/design-units/" + gId,
                method: "GET",
                success: function (res) {
                    loadDetailData(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }

    //Hàm xử lý khi ấn nút Lưu lại
    function onBtnSaveClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vDesignUnit = {
                name: "",
                description: "",
                phone: "",
                phone2: "",
                fax: "",
                email: "",
                website: "",
                note: ""
            }
            //Lấy dữ liệu
            getDesignUnitData(vDesignUnit);
            //Kiểm tra dữ liệu
            var vIsCheck = validateDesignUnitData(vDesignUnit);

            if (vIsCheck) {
                $.ajax({
                    url: "http://localhost:8080/design-units/" + gId + "?addressId=" + gAddressId,
                    method: "PUT",
                    headers: {
                        Authorization: "Token " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vDesignUnit),
                    success: function (res) {
                        window.location.href = "design-units.html";
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            $("#modal-login").modal("show");
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đang nhập."
            });
        }
    }

    //Hàm xử lý khi ấn nút Hủy bỏ
    function onBtnCancelClick() {
        "use strict";
        window.history.back();
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm xử lý hiển thị chi tiết đơn vị thiết kế
    function loadDetailData(paramData) {
        "use strict";
        $("#inp-name").val(paramData.name);
        if (paramData.address != null) {
            $("#sel-address").val(paramData.address.id).trigger("change");
        } else {
            $("#sel-address").val("-1").trigger("change");
        }
        $("#inp-description").val(paramData.description);
        $("#inp-phone").val(paramData.phone);
        $("#inp-phone2").val(paramData.phone2);
        $("#inp-fax").val(paramData.fax);
        $("#inp-email").val(paramData.email);
        $("#inp-website").val(paramData.website);
        $("#inp-note").val(paramData.note);
    }

    //Hàm load dữ liệu vào Select Địa chỉ
    function loadDataToSelectAddress(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            console.log(bI);
            var bAddress = paramData[bI].address;
            console.log(bAddress);
            $("#sel-address").append($("<option>").val(bId).text(bAddress));
        }
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm lấy dữ liệu đơn vị thiết kế
    function getDesignUnitData(paramDesignUnit) {
        gAddressId = $("#sel-address").val();
        paramDesignUnit.name = $("#inp-name").val();
        paramDesignUnit.description = $("#inp-description").val();
        paramDesignUnit.phone = $("#inp-phone").val();
        paramDesignUnit.phone2 = $("#inp-phone2").val();
        paramDesignUnit.fax = $("#inp-fax").val();
        paramDesignUnit.email = $("#inp-email").val();
        paramDesignUnit.website = $("#inp-website").val();
        paramDesignUnit.note = $("#inp-note").val();
    }

    //Hàm lấy kiểm tra dữ liệu đơn vị thiết kế
    function validateDesignUnitData(paramDesignUnit) {
        "use strict";
        if (paramDesignUnit.name.trim() == "") {
            gToast.fire({
                icon: "warning",
                title: "Nhập tên đơn vị thiết kế."
            });
            return false;
        }
        return true;
    }
});