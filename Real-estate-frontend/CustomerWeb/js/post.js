$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gTotalPages = 0;
    var gPage = 0;
    var gPageSize = 12;

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho các nút phân trang
    $("#number-page").on("keypress", function (event) {
        if (event.which == 13) {
            onNumberPageEnter();
        }
    });
    $("#sel-page-size").on("change", onChangePageSize);
    $("#start-page").on("click", onStartPageClick);
    $("#previous-page").on("click", onPreviousPageClick);
    $("#next-page").on("click", onNextPageClick);
    $("#end-page").on("click", onEndPageClick);

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        loadPageData();
    }

    //Hàm load dữ liệu
    function loadPageData() {
        var token = getCookie("token");
        $.ajax({
            url: "http://localhost:8080/realestates/posts" + "?page=" + gPage + "&size=" + gPageSize,
            method: "GET",
            headers: {
                Authorization: "Token " + token
            },
            success: function (res) {
                addCard($("#card-realestates"), res.content);
                gTotalPages = res.totalPages;
                $("#number-page").val(gPage + 1);
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });
    }

    function onNumberPageEnter() {
        var vPage = $("#number-page").val();
        if (vPage < 1) {
            gPage = 0;
            loadRealestateDataToTable();
        } else if (vPage > gTotalPages && gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadRealestateDataToTable();
        } else {
            gPage = vPage - 1;
            loadRealestateDataToTable();
        }
    }

    function onChangePageSize() {
        gPage = 0;
        gPageSize = $("#sel-page-size").val();
        loadRealestateDataToTable();
    }

    //Hàm xử lý khi ấn nút Trang đầu
    function onStartPageClick() {
        if (gTotalPages > 0) {
            gPage = 0;
            loadRealestateDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang trước
    function onPreviousPageClick() {
        if (gTotalPages > 0 && gPage > 0) {
            gPage--;
            loadRealestateDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang sau
    function onNextPageClick() {
        if (gTotalPages > 0 && gPage < gTotalPages - 1) {
            gPage++;
            loadRealestateDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang cuối
    function onEndPageClick() {
        if (gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadRealestateDataToTable();
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

    //Hàm thêm card bất động sản
    function addCard(vElement, vData) {
        "use strict";
        vElement.html("");
        vData.forEach(data => {
            var vTitle = data.title != null ? data.title : "";
            var vPrice = data.price != null ? data.price : "";
            var vAcreage = data.acreage != null ? "<span>" + data.acreage + " m<sup>2</sup></span>" : "";
            var vTypeRequest = stringTypeRequest(data.type, data.request).trim() == "" ? "" : "<div class='aa-tag bg-info'>" + stringTypeRequest(data.type, data.request) + "</div>";
            var vAddress = "";
            if (data.province != null) {
                vAddress += data.province.name;
                if (data.district != null) {
                    vAddress = data.district.name + " - " + vAddress;
                }
            }
            if (data.photo != "" && data.photo != null) {
                var vListPhotos = data.photo.split(",");
                vElement.append(
                    "<li class='col mb-4'>"
                    + "<article class='card aa-properties-item'>"
                    + "<a href='./properties-detail.html?id=" + data.id + "' class='aa-properties-item-img'>"
                    + "<img src='http://localhost:8080/download/realestate-photo/" + vListPhotos[0] + "' style='height: 220px; object-fit: cover;' alt='img'>"
                    + "</a>"
                    + vTypeRequest
                    + "<div class='aa-properties-item-content-body'>"
                    + "<div class='aa-properties-info'>"
                    + "<span><i class='far fa-clock'></i> " + data.createdAt + "</span>"
                    + vAcreage
                    + "</div>"
                    + "<div class='aa-properties-about'>"
                    + "<h4><a href='./properties-detail.html?id=" + data.id + "'>" + vAddress + "</a></h4>"
                    + "<p class='text'>" + vTitle + "</p>"
                    + "</div>"
                    + "</div>"
                    + "<div class='aa-properties-item-content'>"
                    + "<div class='aa-properties-detail'>"
                    + "<span class='aa-price'>" + vPrice + "</span>"
                    + "<a href='./properties-detail.html?id=" + data.id + "' class='aa-secondary-btn'>Xem chi tiết</a>"
                    + "</div>"
                    + "</div>"
                    + "</article>"
                    + "</li>"
                )
            } else {
                vElement.append(
                    "<li class='col mb-4'>"
                    + "<article class='card aa-properties-item'>"
                    + "<a href='./properties-detail.html?id=" + data.id + "' class='aa-properties-item-img'>"
                    + "<img src='./img/no-image.png' style='height: 220px; object-fit: cover;' alt='img'>"
                    + "</a>"
                    + vTypeRequest
                    + "<div class='aa-properties-item-content-body'>"
                    + "<div class='aa-properties-info'>"
                    + "<span><i class='far fa-clock'></i> " + data.createdAt + "</span>"
                    + vAcreage
                    + "</div>"
                    + "<div class='aa-properties-about'>"
                    + "<h4><a href='./properties-detail.html?id=" + data.id + "'>" + vAddress + "</a></h4>"
                    + "<p class='text'>" + vTitle + "</p>"
                    + "</div>"
                    + "</div>"
                    + "<div class='aa-properties-item-content'>"
                    + "<div class='aa-properties-detail'>"
                    + "<span class='aa-price'>" + vPrice + "</span>"
                    + "<a href='./properties-detail.html?id=" + data.id + "' class='aa-secondary-btn'>Xem chi tiết</a>"
                    + "</div>"
                    + "</div>"
                    + "</article>"
                    + "</li>"
                )
            }
        });
    }

    function stringTypeRequest(type, request) {
        "use strict";
        var typeString = "";
        switch (type) {
            case 0:
                typeString = "Đất";
                break;
            case 1:
                typeString = "Nhà ở";
                break;
            case 2:
                typeString = "Căn hộ/Chung cư";
                break;
            case 3:
                typeString = "Văn phòng/Mặt bằng";
                break;
            case 4:
                typeString = "Kinh doanh";
                break;
            case 5:
                typeString = "Phòng trọ";
                break;
            default:
                typeString = "";
        }
        var requestString = "";
        switch (request) {
            case 0:
                requestString = "Cần bán";
                break;
            case 2:
                requestString = "Cần mua";
                break;
            case 3:
                requestString = "Cho thuê";
                break;
            case 4:
                requestString = "Cần thuê";
                break;
            default:
                requestString = "";
        }
        return requestString + " " + typeString;
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
});