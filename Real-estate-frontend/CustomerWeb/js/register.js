$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Kiểm tra dữ liệu nhập vào trên form register
    $("#form-register").validate({
        submitHandler: function () {
            registerFunction();
        },
        rules: {
            inp_register_username: {
                required: true,
            },
            inp_register_password: {
                required: true,
                minlength: 6
            },
            inp_register_re_password: {
                required: true,
                minlength: 6,
                equalTo: "#inp-register-password"
            },
            inp_mobile: {
                required: true,
            },
            inp_email: {
                required: false,
                email: true
            },
            check_agree: "required"
        },
        messages: {
            inp_register_username: {
                required: "Nhập Tên người dùng."
            },
            inp_register_password: {
                required: "Nhập Mật khẩu.",
                minlength: "Mật khẩu gồm ít nhất 6 ký tự."
            },
            inp_register_re_password: {
                required: "Nhập lại mật khẩu.",
                minlength: "Mật khẩu gồm ít nhất 6 ký tự.",
                equalTo: "Mật khẩu không khớp"
            },
            inp_mobile: {
                required: "Nhập số điện thoại",
            },
            inp_email: {
                email: "Email không đúng định dạng"
            },
            check_agree: {
                required: "Vui lòng đồng ý với điều khoản và điều kiện của chúng tôi."
            }
        },
        errorElement: 'span',
        errorClass: 'invalid-feedback',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        checkCookie();
    }

    //Hàm xử lý Đăng ký
    function registerFunction() {
        "use strict";
        var vRegisterData = {
            username: $("#inp-register-username").val().trim(),
            password: $("#inp-register-password").val(),
            mobile: $("#inp-mobile").val().trim(),
            email: $("#inp-email").val().trim()
        }
        var vIsCheck = checkUsername(vRegisterData);
        if (vIsCheck) {
            $.ajax({
                url: "http://localhost:8080/register",
                method: "POST",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(vRegisterData),
                success: function (res) {
                    sessionStorage.setItem("registerSuccess", 1);
                    window.location.href = "login.html";
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            gToast.fire({
                icon: "warning",
                title: "Tên người dùng đã tồn tại, vui lòng nhập tên khác."
            });
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm kiểm tra cookie
    function checkCookie() {
        "use strict";
        const token = getCookie("token");
        if (token) {
            window.location.href = "index.html";
        }
    }

    //Hàm kiểm tra username
    function checkUsername(pRegisterData) {
        "use strict";
        var vIsCheck = false;
        $.ajax({
            url: "http://localhost:8080/users/" + pRegisterData.username + "/exists",
            method: "GET",
            async: false,
            success: function (res) {
                vIsCheck = !res;
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });
        return vIsCheck;
    }
});