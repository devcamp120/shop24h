$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút đăng xuất
    $("#li-logout").on("click", onLogoutClick);

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        checkCookie();
    }

    //Hàm xử lý khi ấn Đăng xuất
    function onLogoutClick() {
        "use strict";
        //Xóa cookie
        setCookie("token", "", 10, false);
        checkCookie();
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm setCookie
    function setCookie(cname, cvalue, exdays, remeber) {
        "use strict";
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        if (remeber) {
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        } else {
            document.cookie = cname + "=" + cvalue + ";path=/";
        }
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm kiểm tra cookie
    function checkCookie() {
        "use strict";
        const token = getCookie("token");
        if (token) {
            checkToken(token);
        } else {
            $("#li-account").prop("hidden", true);
            $("#li-post").prop("hidden", true);
            $("#li-change-password").prop("hidden", true);
            $("#b-username").html("");
            $("#li-login").prop("hidden", false);
            $("#li-register").prop("hidden", false);
            $("#li-hr").prop("hidden", true);
            $("#li-logout").prop("hidden", true);
        }
    }

    //Hàm kiểm tra token
    function checkToken(token) {
        "use strict";
        var headers = {
            Authorization: "Token " + token
        };
        $.ajax({
            url: "http://localhost:8080/users/me",
            method: "GET",
            headers: headers,
            success: function (res) {
                $("#li-account").prop("hidden", false);
                $("#li-post").prop("hidden", false);
                $("#li-change-password").prop("hidden", false);
                $("#b-username").html(res);
                $("#li-login").prop("hidden", true);
                $("#li-register").prop("hidden", true);
                $("#li-hr").prop("hidden", false);
                $("#li-logout").prop("hidden", false);
            },
            error: function (err) {
                $("#li-account").prop("hidden", true);
                $("#li-post").prop("hidden", true);
                $("#li-change-password").prop("hidden", true);
                $("#b-username").html("");
                setCookie("token", "", 10, false);
                $("#li-login").prop("hidden", false);
                $("#li-register").prop("hidden", false);
                $("#li-hr").prop("hidden", true);
                $("#li-logout").prop("hidden", true);
                console.log(err);
            }
        });
    }
});