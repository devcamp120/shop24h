"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gId = "";

var gToast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
onPageLoading();
//Gán sự kiện cho nút Chỉnh sửa
$("#edit-delete").on("click", "#btn-edit", onBtnEditClick);

//Gán sự kiện cho nút Xóa
$("#edit-delete").on("click", "#btn-delete", onBtnDeleteClick);

//Gán sự kiện cho nút Xóa trên modal
$("#btn-delete-confirm").on("click", onBtnDeleteConfirmClick);

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    "use strict";
    const vUrlParams = new URLSearchParams(window.location.search);
    gId = vUrlParams.get("id");
    if (gId != "" && gId != null) {
        $.ajax({
            url: "http://localhost:8080/realestates/" + gId,
            method: "GET",
            async: false,
            success: function (res) {
                var vEditable = checkEditable(gId);
                var vDeleteable = checkDeleteable(gId);
                if (!res.deleted || vEditable) {
                    loadDetailData(res);
                    if (res.deleted == true) {
                        $("#p-message").html("<p><b class='text-danger mb-2'>Tin đang chờ Ban quản trị phê duyệt !</b></p>");
                    }
                    if (vEditable) {
                        $("#edit-delete").append("<button id='btn-edit' class='btn btn-warning mb-2'>Chỉnh sửa</button>");
                    }
                    if (vDeleteable) {
                        $("#edit-delete").append(" <button id='btn-delete' class='btn btn-danger mb-2 ml-1'>Xóa</button>");
                    }
                }
            },
            error: function (err) {
                gToast.fire({
                    icon: "error",
                    title: "Trang không tồn tại."
                });
                console.log(err.responseText);
            }
        });
    }
}

//Hàm xử lý khi ấn nút Chỉnh sửa
function onBtnEditClick() {
    if (checkEditable(gId)) {
        window.location.href = "edit.html?id=" + gId;
    }
}

//Hàm xử lý khi ấn nút Xóa
function onBtnDeleteClick() {
    if (checkDeleteable(gId)) {
        $("#modal-delete").modal("show");
    }
}

//Hàm xử lý khi ấn nút Xóa trên modal
function onBtnDeleteConfirmClick() {
    "use strict";
    var token = getCookie("token");
    if (token) {
        $.ajax({
            url: "http://localhost:8080/realestates/" + gId,
            method: "DELETE",
            headers: {
                Authorization: "Token " + token
            },
            success: function (res) {
                $("#modal-delete").modal("hide");
                window.location.href = "post.html";
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm xử lý hiển thị chi tiết bất động sản
function loadDetailData(paramData) {
    "use strict";
    var vAddress = "";
    var vTypeRequest = stringTypeRequest(paramData.type, paramData.request).trim();
    if (paramData.province != null) {
        vAddress += paramData.province.name;
        if (paramData.district != null) {
            vAddress = paramData.district.name + " - " + vAddress;
            if (paramData.ward != null) {
                vAddress = paramData.ward.name + " - " + vAddress;
                if (paramData.ward.prefix != null) {
                    vAddress = paramData.ward.prefix + " " + vAddress;
                }
            }
        }
    }
    $("#h-title").html(vTypeRequest + (vTypeRequest == "" ? "" : " - ") + vAddress);
    if (paramData.price != null) {
        $("#h-price").html(paramData.price + " (VND)");
    }
    if (paramData.acreage != null) {
        $("#h-acreage").html(paramData.acreage + " (m<sup>2</sup>)");
    }
    if (paramData.customer != null) {
        $("#p-contactName").html("<b>" + paramData.customer.contactName + "</b>");
        $("#a-mobile").prop("href", "tel:" + paramData.customer.mobile).html("<i class='fas fa-phone-square'></i> " + "<b>" + paramData.customer.mobile + "</b>");
    }
    $("#p-title2").html(paramData.title);
    if (paramData.deleted == true) {
        $("#p-message").html("Tin đang chờ Ban quản trị phê duyệt!");
    }
    if (paramData.photo != "" && paramData.photo != null) {
        var vListPhotos = paramData.photo.split(",");
        for (var bI = 0; bI < vListPhotos.length; bI++) {
            $("#properties-images").append(
                "<img src='http://localhost:8080/download/realestate-photo/" + vListPhotos[bI] + "' style='height: 480px; object-fit: contain;' alt='img'>"
            );
        }
    }
    $("#p-created-at").html(paramData.createdAt);
    $("#p-address").html(paramData.address);
    if (paramData.province != null) {
        $("#p-province").html(paramData.province.name);
    }
    if (paramData.district != null) {
        $("#p-district").html(paramData.district.name);
    }
    if (paramData.ward != null) {
        $("#p-ward").html(paramData.ward.name);
    }
    if (paramData.street != null) {
        $("#p-street").html(paramData.street.name);
    }
    if (paramData.project != null) {
        $("#p-project").html(paramData.project.name);
    }
    $("#p-title").html(paramData.title);
    $("#p-type").html(stringType(paramData.type));
    $("#p-request").html(stringRequest(paramData.request));
    $("#p-price").html(paramData.price);
    $("#p-price-min").html(paramData.priceMin);
    $("#p-price-time").html(stringPriceTime(paramData.priceTime));
    $("#p-apart-code").html(paramData.apartCode);
    $("#p-wall-area").html(paramData.wallArea);
    $("#p-bedroom").html(paramData.bedroom);
    $("#p-balcony").html(stringBalcony(paramData.balcony));
    $("#p-landscape-view").html(paramData.landscapeView);
    $("#p-apart-loca").html(stringApartLoca(paramData.apartLoca));
    $("#p-apart-type").html(stringApartType(paramData.apartType));
    $("#p-furniture-type").html(stringFurnitureType(paramData.furnitureType));
    $("#p-price-rent").html(paramData.priceRent);
    $("#p-return-rate").html(paramData.returnRate);
    $("#p-acreage").html(paramData.acreage);
    $("#p-direction").html(stringDirection(paramData.direction));
    $("#p-total-floors").html(paramData.totalFloors);
    $("#p-number-floors").html(paramData.numberFloors);
    $("#p-bath").html(paramData.bath);
    $("#p-legal-doc").html(paramData.legalDoc);
    $("#p-description").html(paramData.description);
    $("#p-width-y").html(paramData.widthY);
    $("#p-long-x").html(paramData.longX);
    $("#p-street-house").html(stringStreetHouse(paramData.streetHouse));
    $("#p-fsbo").html(stringFsbo(paramData.fsbo));
    $("#p-shape").html(paramData.shape);
    $("#p-distance2facade").html(paramData.distance2Facade);
    $("#p-adjacent-facade-num").html(paramData.adjacentFacadeNum);
    $("#p-adjacent-road").html(paramData.adjacentRoad);
    $("#p-ctxd-price").html(paramData.clxdPrice);
    $("#p-ctxd-value").html(paramData.ctxdValue);
    $("#p-alley-min-width").html(paramData.alleyMinWidth);
    $("#p-adjacent-alley-min-width").html(paramData.adjacentAlleyMinWidth);
    $("#p-factor").html(paramData.factor);
    $("#p-structure").html(paramData.structure);
    $("#p-dtxd").html(paramData.dtxd);
    $("#p-clcl").html(paramData.clcl);
    if (paramData.lat != null && paramData.lng != null) {
        //initMap(paramData.lat, paramData.lng);
    }
}

function stringTypeRequest(type, request) {
    "use strict";
    var typeString = "";
    switch (type) {
        case 0:
            typeString = "Đất";
            break;
        case 1:
            typeString = "Nhà ở";
            break;
        case 2:
            typeString = "Căn hộ/Chung cư";
            break;
        case 3:
            typeString = "Văn phòng/Mặt bằng";
            break;
        case 4:
            typeString = "Kinh doanh";
            break;
        case 5:
            typeString = "Phòng trọ";
            break;
        default:
            typeString = "";
    }
    var requestString = "";
    switch (request) {
        case 0:
            requestString = "Cần bán";
            break;
        case 2:
            requestString = "Cần mua";
            break;
        case 3:
            requestString = "Cho thuê";
            break;
        case 4:
            requestString = "Cần thuê";
            break;
        default:
            requestString = "";
    }
    return requestString + " " + typeString;
}

function stringType(paramData) {
    "use strict";
    var vString = "";
    switch (paramData) {
        case 0:
            vString = "Đất";
            break;
        case 1:
            vString = "Nhà ở";
            break;
        case 2:
            vString = "Căn hộ/Chung cư";
            break;
        case 3:
            vString = "Văn phòng/Mặt bằng";
            break;
        case 4:
            vString = "Kinh doanh";
            break;
        case 5:
            vString = "Phòng trọ";
            break;
        default:
            vString = "";
    }
    return vString;
}

function stringRequest(paramData) {
    "use strict";
    var vString = "";
    switch (paramData) {
        case 0:
            vString = "Cần bán";
            break;
        case 2:
            vString = "Cần mua";
            break;
        case 3:
            vString = "Cho thuê";
            break;
        case 4:
            vString = "Cần thuê";
            break;
        default:
            vString = "";
    }
    return vString;
}

function stringPriceTime(paramData) {
    var vString = "";
    switch (paramData) {
        case 0:
            vString = "Bán thường";
            break;
        case 1:
            vString = "Bán nhanh 24h";
            break;
        case 2:
            vString = "Bán nhanh 72h";
            break;
        case 3:
            vString = "Bán nhanh 1 tuần";
            break;
        default:
            vString = "";
    }
    return vString;
}

function stringBalcony(paramData) {
    var vString = "";
    switch (paramData) {
        case 0:
            vString = "Không có";
            break;
        case 1:
            vString = "Có ban công";
            break;
        case 2:
            vString = "Lô gia";
            break;
        default:
            vString = "";
    }
    return vString;
}

function stringApartLoca(paramData) {
    var vString = "";
    switch (paramData) {
        case 0:
            vString = "Căn góc";
            break;
        case 1:
            vString = "Căn thường";
            break;
        default:
            vString = "";
    }
    return vString;
}

function stringApartType(paramData) {
    var vString = "";
    switch (paramData) {
        case 0:
            vString = "Cao cấp";
            break;
        case 1:
            vString = "Văn phòng";
            break;
        case 2:
            vString = "Bình dân";
            break;
        default:
            vString = "";
    }
    return vString;
}

function stringFurnitureType(paramData) {
    var vString = "";
    switch (paramData) {
        case 0:
            vString = "Cơ bản";
            break;
        case 1:
            vString = "Đầy đủ";
            break;
        case 3:
            vString = "Chưa biết";
            break;
        default:
            vString = "";
    }
    return vString;
}

function stringDirection(paramData) {
    var vString = "";
    switch (paramData) {
        case 0:
            vString = "Tây Bắc";
            break;
        case 1:
            vString = "Tây";
            break;
        case 2:
            vString = "Bắc";
            break;
        case 3:
            vString = "Đông";
            break;
        case 4:
            vString = "Nam";
            break;
        case 5:
            vString = "Tây Nam";
            break;
        case 6:
            vString = "Đông Bắc";
            break;
        case 7:
            vString = "Đông Nam";
            break;
        case 8:
            vString = "Tây Nam, Đông Nam";
            break;
        case 9:
            vString = "Tây Nam, Tây Bắc";
            break;
        case 10:
            vString = "Không rõ";
            break;
        case 11:
            vString = "Đông Bắc, Đông Nam";
            break;
        case 12:
            vString = "Đông Bắc, Tây Bắc";
            break;
        default:
            vString = "";
    }
    return vString;
}

function stringStreetHouse(paramData) {
    var vString = "";
    switch (paramData) {
        case true:
            vString = "Có";
            break;
        default:
            vString = "Không";
    }
    return vString;
}

function stringFsbo(paramData) {
    var vString = "";
    switch (paramData) {
        case true:
            vString = "Phải";
            break;
        default:
            vString = "Không phải";
    }
    return vString;
}

//Hàm kiểm tra được phép chỉnh sửa
function checkEditable(paramId) {
    "use strict";
    var token = getCookie("token");
    var vIsCheck = false;
    if (token) {
        var headers = {
            Authorization: "Token " + token
        };
        $.ajax({
            url: "http://localhost:8080/realestates/" + paramId + "/editable",
            method: "GET",
            headers: headers,
            async: false,
            success: function (res) {
                vIsCheck = res;
            },
            error: function (err) {
                vIsCheck = false;
                console.log(err.responseText);
            }
        });
    }
    return vIsCheck;
}

//Hàm kiểm tra được phép xóa
function checkDeleteable(paramId) {
    "use strict";
    var token = getCookie("token");
    var vIsCheck = false;
    if (token) {
        var headers = {
            Authorization: "Token " + token
        };
        $.ajax({
            url: "http://localhost:8080/realestates/" + paramId + "/deleteable",
            method: "GET",
            headers: headers,
            async: false,
            success: function (res) {
                vIsCheck = res;
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });
    }
    return vIsCheck;
}

//Hàm get Cookie
function getCookie(cname) {
    "use strict";
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
