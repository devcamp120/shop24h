$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Kiểm tra dữ liệu nhập vào trên form đổi mật khẩu
    $("#form-change-password").validate({
        submitHandler: function () {
            changePasswordFunction();
        },
        rules: {
            inp_old_password: {
                required: true,
                minlength: 6
            },
            inp_new_password: {
                required: true,
                minlength: 6
            },
            inp_re_new_password: {
                required: true,
                minlength: 6,
                equalTo: "#inp-new-password"
            }
        },
        messages: {
            inp_old_password: {
                required: "Nhập Mật khẩu hiện tại.",
                minlength: "Mật khẩu gồm ít nhất 6 ký tự."
            },
            inp_new_password: {
                required: "Nhập Mật khẩu mới.",
                minlength: "Mật khẩu gồm ít nhất 6 ký tự."
            },
            inp_re_new_password: {
                required: "Nhập lại mật khẩu mới.",
                minlength: "Mật khẩu gồm ít nhất 6 ký tự.",
                equalTo: "Mật khẩu không khớp"
            }
        },
        errorElement: 'span',
        errorClass: 'invalid-feedback',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        checkCookie();
    }

    //Hàm xử lý Đổi mật khẩu
    function changePasswordFunction() {
        "use strict";
        var vOldPassword = $("#inp-old-password").val().trim();
        var vNewPassword = $("#inp-new-password").val().trim();
        $.ajax({
            url: "http://localhost:8080/users/change-password?oldPassword=" + vOldPassword + "&newPassword=" + vNewPassword,
            method: "PUT",
            headers: {
                Authorization: "Token " + getCookie("token")
            },
            success: function (res) {
                if (res) {
                    setCookie("token", "", 10, false);
                    sessionStorage.setItem("changePasswordSuccess", 1);
                    window.location.href = "login.html";
                } else {
                    gToast.fire({
                        icon: "warning",
                        title: "Mật khẩu hiện tại không đúng, vui lòng nhập lại."
                    });
                }
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm setCookie
    function setCookie(cname, cvalue, exdays, remeber) {
        "use strict";
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        if (remeber) {
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        } else {
            document.cookie = cname + "=" + cvalue + ";path=/";
        }
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm kiểm tra cookie
    function checkCookie() {
        "use strict";
        const token = getCookie("token");
        if (!token) {
            window.location.href = "login.html";
        }
    }
});