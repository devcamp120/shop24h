$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gId = "";
    var gProvinceId = "-1";
    var gDistrictId = "-1";
    var gWardId = "-1";
    var gStreetId = "-1";
    var gProjectId = "-1";
    var gPhoto = "";
    var gDeletePhoto = [];

    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho Select Tỉnh / Thành phố
    $("#sel-province").on("change", onSelectProvinceChange);
    //Gán sự kiện cho Select Quận / Huyện
    $("#sel-district").on("change", onSelectDistrictChange);

    //Gán sự kiện cho nút Lưu lại
    $("#btn-save").on("click", onBtnSaveClick);

    //Gán sự kiện cho nút Hủy bỏ
    $("#btn-cancel").on("click", onBtnCancelClick);

    $("#card-images").on("click", ".delete-server-image", function () {
        var vListPhotos = gPhoto.split(",");
        var vPhotoName = $(this)[0].dataset.photoName;
        var index = vListPhotos.indexOf(vPhotoName);
        if (index !== -1) {
            vListPhotos.splice(index, 1);
        }
        gDeletePhoto.push(vPhotoName);
        gPhoto = vListPhotos.join(",");
        $(this).parent().parent().remove();
    });

    $("#card-images").on("click", ".delete-local-image", function () {
        $("#file-photo").val("");
        $("#card-images .delete-local-image").parent().parent().remove();
    });

    $("#file-photo").on("change", function () {
        $("#card-images .delete-local-image").parent().parent().remove();
        var vFiles = this.files;
        for (var bI = 0; bI < vFiles.length; bI++) {
            if (vFiles[bI]) {
                let reader = new FileReader();
                reader.onload = function (event) {
                    $("#card-images").append(
                        "<div class='col'>"
                        + "<div class='card border-0'>"
                        + "<img src='" + event.target.result + "' class='card-img-top' style='object-fit: contain;' alt='...'>"
                        + "<button class='btn border-0 delete-local-image' style='position: absolute; top: 0%; left: 80%;'><i class='text-danger fas fa-window-close'></i></button>"
                        + "</div>"
                        + "</div>"
                    );
                }
                reader.readAsDataURL(vFiles[bI]);
            }
        }
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        const vUrlParams = new URLSearchParams(window.location.search);
        gId = vUrlParams.get("id");

        $(".select2bs4").select2({
            theme: "bootstrap4"
        });

        if (checkEditable(gId)) {
            $.ajax({
                url: "http://localhost:8080/provinces/all",
                method: "GET",
                async: false,
                success: function (res) {
                    loadDataToSelectProvince(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });

            $.ajax({
                url: "http://localhost:8080/projects/all",
                method: "GET",
                async: false,
                success: function (res) {
                    loadDataToSelectProject(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });

            if (gId != "" || gId != null) {
                $.ajax({
                    url: "http://localhost:8080/realestates/" + gId,
                    method: "GET",
                    success: function (res) {
                        loadDetailData(res);
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đang nhập hoặc không có quyền chỉnh sửa."
            });
        }
    }

    //Hàm xử lý khi ấn nút Lưu lại
    function onBtnSaveClick() {
        "use strict";
        if (checkEditable(gId)) {
            var token = getCookie("token");
            var vRealestate = {
                title: "",
                type: "",
                request: "",
                address: "",
                price: "",
                priceMin: "",
                priceTime: "",
                acreage: "",
                direction: "",
                totalFloors: "",
                numberFloors: "",
                bath: "",
                apartCode: "",
                wallArea: "",
                bedroom: "",
                balcony: "",
                landscapeView: "",
                apartLoca: "",
                apartType: "",
                furnitureType: "",
                priceRent: "",
                returnRate: "",
                legalDoc: "",
                description: "",
                widthY: "",
                longX: "",
                streetHouse: "",
                fsbo: "",
                shape: "",
                distance2Facade: "",
                adjacentFacadeNum: "",
                adjacentRoad: "",
                alleyMinWidth: "",
                adjacentAlleyMinWidth: "",
                factor: "",
                structure: "",
                dtsxd: "",
                clcl: "",
                clxdPrice: "",
                ctxdValue: "",
                photo: "",
                lat: "",
                lng: ""
            }
            //Lấy dữ liệu
            getRealestateData(vRealestate);
            //Kiểm tra dữ liệu
            var vIsCheck = validateRealestateData(vRealestate);

            if (vIsCheck) {
                if (gDeletePhoto.length > 0) {
                    for (var bI = 0; bI < gDeletePhoto.length; bI++) {
                        $.ajax({
                            url: "http://localhost:8080/delete/reastate-photo/" + gDeletePhoto[bI] + "?id=" + gId,
                            method: "DELETE",
                            headers: {
                                Authorization: "Token " + token
                            },
                            success: function (res) {

                            },
                            error: function (err) {
                                console.log(err.responseText);
                            }
                        });
                    }
                }

                if ($("#file-photo")[0].files.length > 0) {
                    var vForm = getPhotoInputData();
                    $.ajax({
                        url: "http://localhost:8080/update/realestate-photo/" + gId,
                        method: "PUT",
                        headers: {
                            Authorization: "Token " + token
                        },
                        async: false,
                        processData: false,
                        mimeType: "multipart/form-data",
                        contentType: false,
                        data: vForm,
                        success: function (res) {
                            var vNames = JSON.parse(res);
                            gPhoto += gPhoto == "" ? vNames.join(",") : "," + vNames.join(",");
                            vRealestate.photo = gPhoto;
                        },
                        error: function (err) {
                            console.log(err.responseText);
                        }
                    });
                } else {
                    vRealestate.photo = gPhoto;
                }


                $.ajax({
                    url: "http://localhost:8080/realestates/" + gId + "?provinceId=" + gProvinceId + "&districtId=" + gDistrictId + "&wardId=" + gWardId + "&streetId=" + gStreetId + "&projectId=" + gProjectId,
                    method: "PUT",
                    headers: {
                        Authorization: "Token " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vRealestate),
                    success: function (res) {
                        window.location.href = "properties-detail.html?id=" + gId;
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            gToast.fire({
                icon: "warning",
                title: "Bạn chưa đang nhập hoặc không có quyền chỉnh sửa."
            });
        }

    }

    //Hàm xử lý khi ấn nút Hủy bỏ
    function onBtnCancelClick() {
        "use strict";
        window.history.back();
    }

    //Hàm xử lý khi chọn Tỉnh / Thành phố
    function onSelectProvinceChange() {
        var vProvinceId = $("#sel-province").val();
        $("#sel-district").html("");
        $("#sel-ward").html("");
        $("#sel-street").html("");
        $("#sel-district").append($("<option>").val("-1").text("-- Quận huyện --"));
        $("#sel-ward").append($("<option>").val("-1").text("-- Phường xã --"));
        $("#sel-street").append($("<option>").val("-1").text("-- Đường phố --"));
        if (vProvinceId != "-1") {
            $.ajax({
                url: "http://localhost:8080/provinces/" + vProvinceId + "/districts",
                method: "GET",
                async: false,
                success: function (res) {
                    loadDataToSelectDistrict(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }

    //Hàm xử lý khi chọn Quận / Huyện
    function onSelectDistrictChange() {
        var vDistrictId = $("#sel-district").val();
        $("#sel-ward").html("");
        $("#sel-street").html("");
        $("#sel-ward").append($("<option>").val("-1").text("-- Phường xã --"));
        $("#sel-street").append($("<option>").val("-1").text("-- Đường phố --"));
        if (vDistrictId != "-1") {
            $.ajax({
                url: "http://localhost:8080/districts/" + vDistrictId + "/wards",
                method: "GET",
                async: false,
                success: function (res) {
                    loadDataToSelectWard(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
            $.ajax({
                url: "http://localhost:8080/districts/" + vDistrictId + "/streets",
                method: "GET",
                async: false,
                success: function (res) {
                    loadDataToSelectStreet(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm xử lý hiển thị chi tiết bất động sản
    function loadDetailData(paramData) {
        "use strict";
        $("#inp-address").val(paramData.address);
        if (paramData.photo != "" && paramData.photo != null) {
            gPhoto = paramData.photo;
            var vListPhotos = gPhoto.split(",");
            for (var bI = 0; bI < vListPhotos.length; bI++) {
                $("#card-images").append(
                    "<div class='col'>"
                    + "<div class='card border-0'>"
                    + "<img src='http://localhost:8080/download/realestate-photo/" + vListPhotos[bI] + "' class='card-img-top' style='object-fit: contain;' alt='...'>"
                    + "<button data-photo-name='" + vListPhotos[bI] + "' class='btn border-0 delete-server-image' style='position: absolute; top: 0%; left: 80%;'><i class='text-danger fas fa-window-close'></i></button>"
                    + "</div>"
                    + "</div>"
                );
            }
        }
        if (paramData.province != null) {
            $("#sel-province").val(paramData.province.id).trigger("change");
            if (paramData.district != null) {
                $("#sel-district").val(paramData.district.id).trigger("change");
                if (paramData.ward != null) {
                    $("#sel-ward").val(paramData.ward.id).trigger("change");
                } else {
                    $("#sel-ward").val("-1").trigger("change");
                }
                if (paramData.street != null) {
                    $("#sel-street").val(paramData.street.id).trigger("change");
                } else {
                    $("#sel-street").val("-1").trigger("change");
                }
            } else {
                $("#sel-district").val("-1").trigger("change");
            }
        } else {
            $("#sel-province").val("-1").trigger("change");
        }
        if (paramData.project != null) {
            $("#sel-project").val(paramData.project.id).trigger("change");
        } else {
            $("#sel-project").val("-1").trigger("change");
        }
        $("#inp-title").val(paramData.title);
        if (paramData.type != null) {
            $("#sel-type").val(paramData.type).trigger("change");
        } else {
            $("#sel-type").val("-1").trigger("change");
        }
        if (paramData.request != null) {
            $("#sel-request").val(paramData.request).trigger("change");
        } else {
            $("#sel-request").val("-1").trigger("change");
        }
        $("#inp-price").val(paramData.price);
        $("#inp-price-min").val(paramData.priceMin);
        if (paramData.priceTime != null) {
            $("#sel-price-time").val(paramData.priceTime).trigger("change");
        } else {
            $("#sel-price-time").val("-1").trigger("change");
        }
        $("#inp-apart-code").val(paramData.apartCode);
        $("#inp-wall-area").val(paramData.wallArea);
        $("#inp-bedroom").val(paramData.bedroom);
        if (paramData.balcony != null) {
            $("#sel-balcony").val(paramData.balcony).trigger("change");
        } else {
            $("#sel-balcony").val("-1").trigger("change");
        }
        $("#inp-landscape-view").val(paramData.landscapeView);
        if (paramData.apartLoca != null) {
            $("#sel-apart-loca").val(paramData.apartLoca).trigger("change");
        } else {
            $("#sel-apart-loca").val("-1").trigger("change");
        }
        if (paramData.apartType != null) {
            $("#sel-apart-type").val(paramData.apartType).trigger("change");
        } else {
            $("#sel-apart-type").val("-1").trigger("change");
        }
        if (paramData.furnitureType != null) {
            $("#sel-furniture-type").val(paramData.furnitureType).trigger("change");
        } else {
            $("#sel-furniture-type").val("-1").trigger("change");
        }
        $("#inp-price-rent").val(paramData.priceRent);
        $("#inp-return-rate").val(paramData.returnRate);
        $("#inp-acreage").val(paramData.acreage);
        if (paramData.direction != null) {
            $("#sel-direction").val(paramData.direction).trigger("change");
        } else {
            $("#sel-direction").val("-1").trigger("change");
        }
        $("#inp-total-floors").val(paramData.totalFloors);
        $("#inp-number-floors").val(paramData.numberFloors);
        $("#inp-bath").val(paramData.bath);
        $("#inp-legal-doc").val(paramData.legalDoc);
        $("#inp-description").val(paramData.description);
        $("#inp-width-y").val(paramData.widthY);
        $("#inp-long-x").val(paramData.longX);
        if (paramData.streetHouse != null) {
            $("#cbx-street-house").prop("checked", paramData.streetHouse);
        }
        if (paramData.fsbo != null) {
            $("#cbx-fsbo").prop("checked", paramData.fsbo);
        }
        $("#inp-shape").val(paramData.shape);
        $("#inp-distance2facade").val(paramData.distance2Facade);
        $("#inp-adjacent-facade-num").val(paramData.adjacentFacadeNum);
        $("#inp-adjacent-road").val(paramData.adjacentRoad);
        $("#inp-ctxd-price").val(paramData.clxdPrice);
        $("#inp-ctxd-value").val(paramData.ctxdValue);
        $("#inp-alley-min-width").val(paramData.alleyMinWidth);
        $("#inp-adjacent-alley-min-width").val(paramData.adjacentAlleyMinWidth);
        $("#inp-factor").val(paramData.factor);
        $("#inp-structure").val(paramData.structure);
        $("#inp-dtsxd").val(paramData.dtsxd);
        $("#inp-clcl").val(paramData.clcl);
        $("#inp-lng").val(paramData.lng);
        $("#inp-lat").val(paramData.lat);
    }

    //Hàm load dữ liệu vào Select Tỉnh / Thành phố
    function loadDataToSelectProvince(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-province").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Quận / Huyện
    function loadDataToSelectDistrict(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-district").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Phường / Xã
    function loadDataToSelectWard(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-ward").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Đường phố
    function loadDataToSelectStreet(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-street").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Dự án
    function loadDataToSelectProject(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-project").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm kiểm tra được phép chỉnh sửa
    function checkEditable(paramId) {
        "use strict";
        var token = getCookie("token");
        var vIsCheck = false;
        if (token) {
            var headers = {
                Authorization: "Token " + token
            };
            $.ajax({
                url: "http://localhost:8080/realestates/" + paramId + "/editable",
                method: "GET",
                headers: headers,
                async: false,
                success: function (res) {
                    vIsCheck = res;
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
        return vIsCheck;
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm lấy dữ liệu hình ảnh để upload
    function getPhotoInputData() {
        var formData = new FormData();
        var vFiles = $("#file-photo")[0].files;
        for (var bI = 0; bI < vFiles.length; bI++) {
            formData.append("files", vFiles[bI]);
        }
        return formData;
    }

    //Hàm lấy dữ liệu bất động sản
    function getRealestateData(paramRealestate) {
        gProvinceId = $("#sel-province").val();
        gDistrictId = $("#sel-district").val();
        gWardId = $("#sel-ward").val();
        gStreetId = $("#sel-street").val();
        gProjectId = $("#sel-project").val();
        paramRealestate.title = $("#inp-title").val();
        if ($("#sel-type").val() == "-1") {
            paramRealestate.type = null;
        } else {
            paramRealestate.type = $("#sel-type").val();
        }
        if ($("#sel-request").val() == "-1") {
            paramRealestate.request = null;
        } else {
            paramRealestate.request = $("#sel-request").val();
        }
        paramRealestate.address = $("#inp-address").val().trim();
        paramRealestate.price = $("#inp-price").val();
        paramRealestate.priceMin = $("#inp-price-min").val();
        if ($("#sel-price-time").val() == "-1") {
            paramRealestate.priceTime = null;
        } else {
            paramRealestate.priceTime = $("#sel-price-time").val();
        }
        paramRealestate.acreage = $("#inp-acreage").val();
        if ($("#sel-direction").val() == "-1") {
            paramRealestate.direction = null;
        } else {
            paramRealestate.direction = $("#sel-direction").val();
        }
        paramRealestate.totalFloors = $("#inp-total-floors").val();
        paramRealestate.numberFloors = $("#inp-number-floors").val();
        paramRealestate.bath = $("#inp-bath").val();
        paramRealestate.apartCode = $("#inp-apart-code").val();
        paramRealestate.wallArea = $("#inp-wall-area").val();
        paramRealestate.bedroom = $("#inp-bedroom").val();
        if ($("#sel-balcony").val() == "-1") {
            paramRealestate.balcony = null;
        } else {
            paramRealestate.balcony = $("#sel-balcony").val();
        }
        paramRealestate.landscapeView = $("#inp-landscape-view").val();
        if ($("#sel-apart-loca").val() == "-1") {
            paramRealestate.apartLoca = null;
        } else {
            paramRealestate.apartLoca = $("#sel-apart-loca").val();
        }
        if ($("#sel-apart-type").val() == "-1") {
            paramRealestate.apartType = null;
        } else {
            paramRealestate.apartType = $("#sel-apart-type").val();
        }
        if ($("#sel-furniture-type").val() == "-1") {
            paramRealestate.furnitureType = null;
        } else {
            paramRealestate.furnitureType = $("#sel-furniture-type").val();
        }
        paramRealestate.priceRent = $("#inp-price-rent").val();
        paramRealestate.returnRate = $("#inp-return-rate").val();
        paramRealestate.legalDoc = $("#inp-legal-doc").val();
        paramRealestate.description = $("#inp-description").val();
        paramRealestate.widthY = $("#inp-width-y").val();
        paramRealestate.longX = $("#inp-long-x").val();
        paramRealestate.streetHouse = $("#cbx-street-house").is(":checked");
        paramRealestate.fsbo = $("#cbx-fsbo").is(":checked");
        paramRealestate.shape = $("#inp-shape").val();
        paramRealestate.distance2Facade = $("#inp-distance2facade").val();
        paramRealestate.adjacentFacadeNum = $("#inp-adjacent-facade-num").val();
        paramRealestate.adjacentRoad = $("#inp-adjacent-road").val();
        paramRealestate.alleyMinWidth = $("#inp-alley-min-width").val();
        paramRealestate.adjacentAlleyMinWidth = $("#inp-adjacent-alley-min-width").val();
        paramRealestate.factor = $("#inp-factor").val();
        paramRealestate.structure = $("#inp-structure").val();
        paramRealestate.dtsxd = $("#inp-dtsxd").val();
        paramRealestate.clcl = $("#inp-clcl").val();
        paramRealestate.clxdPrice = $("#inp-ctxd-price").val();
        paramRealestate.ctxdValue = $("#inp-ctxd-value").val();
        paramRealestate.lat = $("#inp-lat").val();
        paramRealestate.lng = $("#inp-lng").val();
    }

    //Hàm kiểm tra dữ liệu bất động sản
    function validateRealestateData(paramRealestate) {
        "use strict";
        if (gProvinceId == null) {
            gProvinceId = "-1";
        }
        if (gDistrictId == null) {
            gDistrictId = "-1";
        }
        if (gWardId == null) {
            gWardId = "-1";
        }
        if (gStreetId == null) {
            gStreetId = "-1";
        }
        if (paramRealestate.address == "") {
            gToast.fire({
                icon: "warning",
                title: "Nhập địa chỉ."
            });
            return false;
        }
        return true;
    }
});