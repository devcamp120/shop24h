$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gAreaSlider = $("#aa-sqrfeet-range")[0];
    var gPriceSlider = $("#aa-price-range")[0];

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    jQuery(function () {
        if (jQuery('body').is('.aa-price-range')) {
            // FOR AREA SECTION
            var valuesForAreaSlider = [0, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 100000000];
            var format = {
                to: function (value) {
                    var vValue = valuesForAreaSlider[Math.round(value)];
                    if (vValue > 100000) {
                        return "Trên 100000";
                    }
                    return vValue;
                },
                from: function (value) {
                    return valuesForAreaSlider.indexOf(Number(value));
                }
            };
            noUiSlider.create(gAreaSlider, {
                range: { min: 0, max: valuesForAreaSlider.length - 1 },
                step: 1,
                start: [0, 100000000],
                format: format
            });
            // for value print
            var skipValues = [
                document.getElementById('skip-value-lower'),
                document.getElementById('skip-value-upper')
            ];

            gAreaSlider.noUiSlider.on('update', function (values, handle) {
                skipValues[handle].innerHTML = values[handle];
            });

            // FOR PRICE SECTION

            var valuesForPriceSlider = [0, 1000000, 2000000, 5000000, 10000000, 20000000, 50000000, 100000000, 200000000, 500000000, 1000000000, 2000000000, 5000000000, 10000000000, 20000000000, 50000000000, 10000000000000];
            var format2 = {
                to: function (value) {
                    var vValue = valuesForPriceSlider[Math.round(value)];
                    if (vValue == 0) {
                        return vValue
                    } else if (vValue < 1000000000) {
                        return vValue / 1000000 + " triệu";
                    } else if (vValue <= 50000000000) {
                        return vValue / 1000000000 + " tỉ";
                    } else {
                        return "Trên 50 tỉ";
                    }
                },
                from: function (value) {
                    return valuesForPriceSlider.indexOf(Number(value));
                }
            };
            noUiSlider.create(gPriceSlider, {
                range: { min: 0, max: valuesForPriceSlider.length - 1 },
                step: 1,
                start: [0, 10000000000000],
                format: format2
            });
            // for value print
            var skipValues2 = [
                document.getElementById('skip-value-lower2'),
                document.getElementById('skip-value-upper2')
            ];

            gPriceSlider.noUiSlider.on('update', function (values, handle) {
                skipValues2[handle].innerHTML = values[handle];
            });
        }
    });

    //Gán sự kiện cho Select Tỉnh / Thành phố
    $("#sel-province").on("change", onSelectProvinceChange);
    //Gán sự kiện cho Select Quận / Huyện
    $("#sel-district").on("change", onSelectDistrictChange);

    //Gán sự kiện cho nút tìm kiếm
    $("#btn-search").on("click", onBtnSearchClick);

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";

        $.ajax({
            url: "http://localhost:8080/provinces/all",
            method: "GET",
            success: function (res) {
                loadDataToSelectProvince(res);
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });

        loadPageData();
    }

    //Hàm xử lý khi chọn Tỉnh / Thành phố
    function onSelectProvinceChange() {
        var vProvinceId = $("#sel-province").val();
        $("#sel-district").html("");
        $("#sel-ward").html("");
        $("#sel-district").append($("<option>").val("-1").text("Quận huyện"));
        $("#sel-ward").append($("<option>").val("-1").text("Phường xã"));
        if (vProvinceId != "-1") {
            $.ajax({
                url: "http://localhost:8080/provinces/" + vProvinceId + "/districts",
                method: "GET",
                success: function (res) {
                    loadDataToSelectDistrict(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }

    //Hàm xử lý khi chọn Quận / Huyện
    function onSelectDistrictChange() {
        var vDistrictId = $("#sel-district").val();
        $("#sel-ward").html("");
        $("#sel-ward").append($("<option>").val("-1").text("Phường xã"));
        if (vDistrictId != "-1") {
            $.ajax({
                url: "http://localhost:8080/districts/" + vDistrictId + "/wards",
                method: "GET",
                success: function (res) {
                    loadDataToSelectWard(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }

    //Hàm xử lý khi ấn nút tìm kiếm
    function onBtnSearchClick() {
        "use strict";
        var vProvinceId = $("#sel-province").val();
        var vDistrictId = $("#sel-district").val();
        var vWardId = $("#sel-ward").val();
        var vType = $("#sel-type").val();
        var vRequest = $("#sel-request").val();
        var vArea = gAreaSlider.noUiSlider.get();
        var vMinArea = areaValue(vArea[0]);
        var vMaxArea = areaValue(vArea[1]);
        var vPrice = gPriceSlider.noUiSlider.get();
        var vMinPrice = priceValue(vPrice[0]);
        var vMaxPrice = priceValue(vPrice[1]);
        sessionStorage.setItem("vProvinceId", vProvinceId);
        sessionStorage.setItem("vDistrictId", vDistrictId);
        sessionStorage.setItem("vWardId", vWardId);
        sessionStorage.setItem("vType", vType);
        sessionStorage.setItem("vRequest", vRequest);
        sessionStorage.setItem("vMinArea", vMinArea);
        sessionStorage.setItem("vMaxArea", vMaxArea);
        sessionStorage.setItem("vMinPrice", vMinPrice);
        sessionStorage.setItem("vMaxPrice", vMaxPrice);

        window.location.href = "properties.html";
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm load dữ liệu theo trang
    function loadPageData() {
        $.ajax({
            url: "http://localhost:8080/realestates/search?provinceId=-1&districtId=-1&wardId=-1&type=-1&request=-1&sort=1&page=0&size=12",
            method: "GET",
            success: function (res) {
                addCard($("#lastest-realestates"), res.content);
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });
    }

    //Hàm thêm card bất động sản
    function addCard(vElement, vData) {
        "use strict";
        vElement.html("");
        vData.forEach(data => {
            var vTitle = data.title != null ? data.title : "";
            var vPrice = data.price != null ? data.price : "";
            var vAcreage = data.acreage != null ? "<span>" + data.acreage + " m<sup>2</sup></span>" : "";
            var vTypeRequest = stringTypeRequest(data.type, data.request).trim() == "" ? "" : "<div class='aa-tag bg-info'>" + stringTypeRequest(data.type, data.request) + "</div>";
            var vAddress = "";
            if (data.province != null) {
                vAddress += data.province.name;
                if (data.district != null) {
                    vAddress = data.district.name + " - " + vAddress;
                }
            }
            if (data.photo != "" && data.photo != null) {
                var vListPhotos = data.photo.split(",");
                vElement.append(
                    "<div class='col mb-4'>"
                    + "<article class='card aa-properties-item'>"
                    + "<a href='./properties-detail.html?id=" + data.id + "' class='aa-properties-item-img'>"
                    + "<img src='http://localhost:8080/download/realestate-photo/" + vListPhotos[0] + "' style='height: 220px; object-fit: cover;' alt='img'>"
                    + "</a>"
                    + vTypeRequest
                    + "<div class='aa-properties-item-content-body'>"
                    + "<div class='aa-properties-info'>"
                    + "<span><i class='far fa-clock'></i> " + data.createdAt + "</span>"
                    + vAcreage
                    + "</div>"
                    + "<div class='aa-properties-about'>"
                    + "<h4><a href='./properties-detail.html?id=" + data.id + "'>" + vAddress + "</a></h4>"
                    + "<p class='text'>" + vTitle + "</p>"
                    + "</div>"
                    + "</div>"
                    + "<div class='aa-properties-item-content'>"
                    + "<div class='aa-properties-detail'>"
                    + "<span class='aa-price'>" + vPrice + "</span>"
                    + "<a href='./properties-detail.html?id=" + data.id + "' class='aa-secondary-btn'>Xem chi tiết</a>"
                    + "</div>"
                    + "</div>"
                    + "</article>"
                    + "</div>"
                )
            } else {
                vElement.append(
                    "<div class='col mb-4'>"
                    + "<article class='card aa-properties-item'>"
                    + "<a href='./properties-detail.html?id=" + data.id + "' class='aa-properties-item-img'>"
                    + "<img src='./img/no-image.png' style='height: 220px; object-fit: cover;' alt='img'>"
                    + "</a>"
                    + vTypeRequest
                    + "<div class='aa-properties-item-content-body'>"
                    + "<div class='aa-properties-info'>"
                    + "<span><i class='far fa-clock'></i> " + data.createdAt + "</span>"
                    + vAcreage
                    + "</div>"
                    + "<div class='aa-properties-about'>"
                    + "<h4><a href='./properties-detail.html?id=" + data.id + "'>" + vAddress + "</a></h4>"
                    + "<p class='text'>" + vTitle + "</p>"
                    + "</div>"
                    + "</div>"
                    + "<div class='aa-properties-item-content'>"
                    + "<div class='aa-properties-detail'>"
                    + "<span class='aa-price'>" + vPrice + "</span>"
                    + "<a href='./properties-detail.html?id=" + data.id + "' class='aa-secondary-btn'>Xem chi tiết</a>"
                    + "</div>"
                    + "</div>"
                    + "</article>"
                    + "</div>"
                )
            }
        });
    }

    function stringTypeRequest(type, request) {
        "use strict";
        var typeString = "";
        switch (type) {
            case 0:
                typeString = "Đất";
                break;
            case 1:
                typeString = "Nhà ở";
                break;
            case 2:
                typeString = "Căn hộ/Chung cư";
                break;
            case 3:
                typeString = "Văn phòng/Mặt bằng";
                break;
            case 4:
                typeString = "Kinh doanh";
                break;
            case 5:
                typeString = "Phòng trọ";
                break;
            default:
                typeString = "";
        }
        var requestString = "";
        switch (request) {
            case 0:
                requestString = "Cần bán";
                break;
            case 2:
                requestString = "Cần mua";
                break;
            case 3:
                requestString = "Cho thuê";
                break;
            case 4:
                requestString = "Cần thuê";
                break;
            default:
                requestString = "";
        }
        return requestString + " " + typeString;
    }

    //Hàm load dữ liệu vào Select Tỉnh / Thành phố
    function loadDataToSelectProvince(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-province").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Quận / Huyện
    function loadDataToSelectDistrict(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-district").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Phường / Xã
    function loadDataToSelectWard(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-ward").append($("<option>").val(bId).text(bName));
        }
    }

    function areaValue(areaString) {
        if (areaString == "Trên 100000") {
            return "100000000";
        }
        return areaString;
    }

    function priceValue(priceString) {
        if (priceString == "0") {
            return "0";
        } else if (priceString.includes("triệu")) {
            return priceString.replace(" triệu", "000000");
        } else if (priceString.includes("tỉ") && !priceString.includes("Trên")) {
            return priceString.replace(" tỉ", "000000000");
        } else {
            return "10000000000000";
        }
    }
});