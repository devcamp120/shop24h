$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gToast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Kiểm tra dữ liệu nhập vào trên form login
    $("#form-login").validate({
        submitHandler: function () {
            loginFunction();
        },
        rules: {
            inp_login_username: {
                required: true
            },
            inp_login_password: {
                required: true,
                minlength: 6
            }
        },
        messages: {
            inp_login_username: {
                required: "Nhập Tên người dùng.",
            },
            inp_login_password: {
                required: "Nhập Mật khẩu.",
                minlength: "Mật khẩu gồm ít nhất 6 ký tự."
            }
        },
        errorElement: 'span',
        errorClass: 'invalid-feedback',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        checkCookie();
        if (sessionStorage.getItem("registerSuccess")) {
            if (sessionStorage.getItem("registerSuccess") == 1) {
                gToast.fire({
                    icon: "success",
                    title: "Tạo tài khoản thành công, vui lòng đăng nhập."
                });
            }
            sessionStorage.removeItem("registerSuccess");
        }
        if (sessionStorage.getItem("changePasswordSuccess")) {
            if (sessionStorage.getItem("changePasswordSuccess") == 1) {
                gToast.fire({
                    icon: "success",
                    title: "Đã đổi mật khẩu, vui lòng đăng nhập lại."
                });
            }
            sessionStorage.removeItem("changePasswordSuccess");
        }
    }

    //Hàm xử lý Đăng nhập
    function loginFunction() {
        "use strict";
        var vSignInData = {
            username: $("#inp-login-username").val().trim(),
            password: $("#inp-login-password").val().trim()
        }
        var vRemember = $("#check-remember").is(":checked");
        $.ajax({
            url: "http://localhost:8080/login",
            method: "POST",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(vSignInData),
            success: function (res) {
                setCookie("token", res, 10, vRemember);
                window.location.href = "index.html";
            },
            error: function (err) {
                gToast.fire({
                    icon: "error",
                    title: "Tên người dùng hoặc mật khẩu không chính xác."
                });
                console.log(err.responseText);
            }
        });
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm setCookie
    function setCookie(cname, cvalue, exdays, remeber) {
        "use strict";
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        if (remeber) {
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        } else {
            document.cookie = cname + "=" + cvalue + ";path=/";
        }
    }

    //Hàm get Cookie
    function getCookie(cname) {
        "use strict";
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm kiểm tra cookie
    function checkCookie() {
        "use strict";
        const token = getCookie("token");
        if (token) {
            window.location.href = "index.html";
        }
    }
});