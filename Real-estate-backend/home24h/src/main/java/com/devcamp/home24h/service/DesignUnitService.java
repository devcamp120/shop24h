package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.entity.AddressMap;
import com.devcamp.home24h.entity.DesignUnit;
import com.devcamp.home24h.repository.DesignUnitRepository;
import com.devcamp.home24h.repository.ProjectRepository;

@Service
public class DesignUnitService {
    @Autowired
    private DesignUnitRepository designUnitRepository;

    @Autowired
    private ProjectRepository projectRepository;

    public List<DesignUnit> getAllDesignUnits() {
        List<DesignUnit> designUnits = new ArrayList<>();
        designUnitRepository.findAll().forEach(designUnits::add);
        return designUnits;
    }

    public Page<DesignUnit> getDesignUnits(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<DesignUnit> designUnits = designUnitRepository.findAll(pageable);
        return designUnits;
    }

    public DesignUnit getDesignUnitById(Long id) {
        Optional<DesignUnit> optional = designUnitRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    public DesignUnit createDesignUnit(Long addressId,
            DesignUnit pDesignUnit) {
        pDesignUnit.setId(null);
        if (addressId == -1) {
            pDesignUnit.setAddress(null);
        } else {
            AddressMap addressMap = new AddressMap();
            addressMap.setId(addressId);
            pDesignUnit.setAddress(addressMap);
        }
        DesignUnit designUnit = designUnitRepository.save(pDesignUnit);
        return designUnit;
    }

    public DesignUnit updateDesignUnit(Long id, Long addressId,
            DesignUnit pDesignUnit) {
        Optional<DesignUnit> optional = designUnitRepository.findById(id);
        if (optional.isPresent()) {
            pDesignUnit.setId(id);
            if (addressId == -1) {
                pDesignUnit.setAddress(null);
            } else {
                AddressMap addressMap = new AddressMap();
                addressMap.setId(addressId);
                pDesignUnit.setAddress(addressMap);
            }
            return designUnitRepository.save(pDesignUnit);
        } else {
            return null;
        }
    }

    public boolean deleteDesignUnit(Long id) {
        Optional<DesignUnit> optional = designUnitRepository.findById(id);
        if (optional.isPresent()) {
            projectRepository.updateByDesignUnitId(id);
            designUnitRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
}
