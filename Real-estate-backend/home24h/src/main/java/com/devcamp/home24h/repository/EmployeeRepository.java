package com.devcamp.home24h.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.devcamp.home24h.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    @Query(value = "SELECT * FROM employees WHERE FirstName LIKE :keyword OR LastName LIKE :keyword OR Username LIKE :keyword", nativeQuery = true)
    public List<Employee> getEmployeeLike(String keyword);
}
