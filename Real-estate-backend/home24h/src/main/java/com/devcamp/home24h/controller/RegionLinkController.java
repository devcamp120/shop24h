package com.devcamp.home24h.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.home24h.entity.RegionLink;
import com.devcamp.home24h.model.Result;
import com.devcamp.home24h.service.RegionLinkService;
import com.devcamp.home24h.service.StorageService;

@CrossOrigin
@RestController
public class RegionLinkController {
    @Autowired
    private RegionLinkService regionLinkService;

    @Autowired
    private StorageService storageService;

    @GetMapping("/region-links/all")
    public ResponseEntity<List<RegionLink>> getAllRegionLinks() {
        try {
            List<RegionLink> regionLinks = regionLinkService.getAllRegionLinks();
            return new ResponseEntity<>(regionLinks, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/region-links")
    public ResponseEntity<Result> getRegionLinks(@RequestParam(defaultValue = "0") String page,
            @RequestParam(defaultValue = "10") String size) {
        try {
            Page<RegionLink> regionLinks = regionLinkService.getRegionLinks(Integer.parseInt(page),
                    Integer.parseInt(size));
            return new ResponseEntity<>(new Result(regionLinks.getTotalPages(), regionLinks.getContent()),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/region-links/{id}")
    public ResponseEntity<RegionLink> getRegionLinkById(@PathVariable Long id) {
        try {
            RegionLink regionLink = regionLinkService.getRegionLinkById(id);
            if (regionLink != null) {
                return new ResponseEntity<>(regionLink, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(regionLink, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/region-links")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> createRegionLink(@RequestBody RegionLink pRegionLink) {
        try {
            RegionLink regionLink = regionLinkService.createRegionLink(pRegionLink);
            return new ResponseEntity<>(regionLink, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified RegionLink: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/region-links/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateRegionLink(@PathVariable Long id, @RequestBody RegionLink pRegionLink) {
        try {
            RegionLink regionLink = regionLinkService.updateRegionLink(id, pRegionLink);
            if (regionLink != null) {
                return new ResponseEntity<>(regionLink, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(regionLink, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified RegionLink: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/region-links/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<RegionLink> deleteRegionLink(@PathVariable Long id) {
        try {
            boolean isDeleted = regionLinkService.deleteRegionLink(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/upload/region-link-photo")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<String>> uploadRegionLinkImages(@RequestParam("files") MultipartFile[] files) {
        try {
            List<String> fileNames = regionLinkService.uploadRegionLinkPhoto(files);
            return new ResponseEntity<>(fileNames, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/download/region-link-photo/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> downloadRegionLinkPhoto(@PathVariable String filename) {
        String folder = "/region-links/";
        Resource file = storageService.loadAsResource(folder, filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @DeleteMapping("/delete/region-link-photo/{filename:.+}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteRegionLinkPhoto(@PathVariable String filename, @RequestParam Long id) {
        try {
            regionLinkService.deleteRegionLinkPhoto(filename, id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
