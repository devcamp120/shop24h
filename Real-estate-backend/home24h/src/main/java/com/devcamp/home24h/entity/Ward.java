package com.devcamp.home24h.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "ward")
@Getter
@Setter
public class Ward {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "_name")
    private String name;

    @Column(name = "_prefix")
    private String prefix;

    @ManyToOne
    @JoinColumn(name = "_province_id")
    private Province province;

    @ManyToOne
    @JoinColumn(name = "_district_id")
    private District district;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ward")
    @JsonIgnore
    private Set<Realestate> realestates;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ward")
    @JsonIgnore
    private Set<Project> projects;

    public Ward() {
    }

    public Ward(Long id) {
        this.id = id;
    }
}
