package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.home24h.entity.ConstructionContractor;
import com.devcamp.home24h.entity.DesignUnit;
import com.devcamp.home24h.entity.District;
import com.devcamp.home24h.entity.Investor;
import com.devcamp.home24h.entity.Project;
import com.devcamp.home24h.entity.Province;
import com.devcamp.home24h.entity.Street;
import com.devcamp.home24h.entity.Ward;
import com.devcamp.home24h.repository.MasterLayoutRepository;
import com.devcamp.home24h.repository.ProjectRepository;
import com.devcamp.home24h.repository.RealestateRepository;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private RealestateRepository realestateRepository;

    @Autowired
    private MasterLayoutRepository masterLayoutRepository;

    @Autowired
    private StorageService storageService;

    public List<Project> getAllProjects() {
        List<Project> projects = new ArrayList<>();
        projectRepository.findAll().forEach(projects::add);
        return projects;
    }

    public Page<Project> getProjects(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Project> projects = projectRepository.findAll(pageable);
        return projects;
    }

    public Project getProjectById(Long id) {
        Optional<Project> optional = projectRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    public Project createProject(Long provinceId, Long districtId, Long wardId, Long streetId, Long investorId,
            Long constructionContractorId, Long designUnitId, Project pProject) {
        pProject.setId(null);
        if (provinceId == -1) {
            pProject.setProvince(null);
        } else {
            Province province = new Province(provinceId);
            pProject.setProvince(province);
            if (districtId == -1) {
                pProject.setDistrict(null);
            } else {
                District district = new District(districtId);
                pProject.setDistrict(district);
                if (wardId == -1) {
                    pProject.setWard(null);
                } else {
                    Ward ward = new Ward(wardId);
                    pProject.setWard(ward);
                }
                if (streetId == -1) {
                    pProject.setStreet(null);
                } else {
                    Street street = new Street(streetId);
                    pProject.setStreet(street);
                    ;
                }
            }
        }
        if (investorId == -1) {
            pProject.setInvestor(null);
        } else {
            Investor investor = new Investor();
            investor.setId(investorId);
            pProject.setInvestor(investor);
        }
        if (constructionContractorId == -1) {
            pProject.setConstructionContractor(null);
        } else {
            ConstructionContractor constructionContractor = new ConstructionContractor();
            constructionContractor.setId(constructionContractorId);
            pProject.setConstructionContractor(constructionContractor);
        }
        if (designUnitId == -1) {
            pProject.setDesignUnit(null);
        } else {
            DesignUnit designUnit = new DesignUnit();
            designUnit.setId(designUnitId);
            pProject.setDesignUnit(designUnit);
        }
        Project project = projectRepository.save(pProject);
        return project;
    }

    public Project updateProject(Long id, Long provinceId, Long districtId, Long wardId, Long streetId, Long investorId,
            Long constructionContractorId, Long designUnitId, Project pProject) {
        Optional<Project> optional = projectRepository.findById(id);
        if (optional.isPresent()) {
            pProject.setId(id);
            if (provinceId == -1) {
                pProject.setProvince(null);
            } else {
                Province province = new Province(provinceId);
                pProject.setProvince(province);
                if (districtId == -1) {
                    pProject.setDistrict(null);
                } else {
                    District district = new District(districtId);
                    pProject.setDistrict(district);
                    if (wardId == -1) {
                        pProject.setWard(null);
                    } else {
                        Ward ward = new Ward(wardId);
                        pProject.setWard(ward);
                    }
                    if (streetId == -1) {
                        pProject.setStreet(null);
                    } else {
                        Street street = new Street(streetId);
                        pProject.setStreet(street);
                        ;
                    }
                }
            }
            if (investorId == -1) {
                pProject.setInvestor(null);
            } else {
                Investor investor = new Investor();
                investor.setId(investorId);
                pProject.setInvestor(investor);
            }
            if (constructionContractorId == -1) {
                pProject.setConstructionContractor(null);
            } else {
                ConstructionContractor constructionContractor = new ConstructionContractor();
                constructionContractor.setId(constructionContractorId);
                pProject.setConstructionContractor(constructionContractor);
            }
            if (designUnitId == -1) {
                pProject.setDesignUnit(null);
            } else {
                DesignUnit designUnit = new DesignUnit();
                designUnit.setId(designUnitId);
                pProject.setDesignUnit(designUnit);
            }
            return projectRepository.save(pProject);
        } else {
            return null;
        }
    }

    public boolean deleteProject(Long id) {
        Optional<Project> optional = projectRepository.findById(id);
        if (optional.isPresent()) {
            String photo = optional.get().getPhoto();
            if (photo != null && photo != "") {
                String[] arr = photo.split(",");
                for (String filename : arr) {
                    storageService.delete("/projects/", filename);
                }
            }
            masterLayoutRepository.deleteByProjectId(id);
            realestateRepository.updateByProjectId(id);
            projectRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    public List<String> uploadProjectPhoto(MultipartFile[] files) {
        List<String> fileNames = new ArrayList<>();
        String folder = "/projects/";
        for (MultipartFile file : files) {
            String fileName = storageService.store(folder, file);
            fileNames.add(fileName);
        }
        return fileNames;
    }

    public void deleteProjectPhoto(String filename, Long id) {
        Optional<Project> optional = projectRepository.findById(id);
        if (optional.isPresent()) {
            String photo = optional.get().getPhoto();
            if (photo.contains(filename)) {
                storageService.delete("/projects/", filename);
            }
        }
    }
}
