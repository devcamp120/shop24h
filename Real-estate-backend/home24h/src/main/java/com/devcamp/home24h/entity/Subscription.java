package com.devcamp.home24h.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "subscriptions")
@Getter
@Setter
public class Subscription {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user", length = 255)
    private String user;

    @Column(name = "endpoint", nullable = false)
    private String endpoint;

    @Column(name = "publickey", length = 255, nullable = false)
    private String publickey;

    @Column(name = "authenticationtoken", length = 255, nullable = false)
    private String authenticationtoken;

    @Column(name = "contentencoding", length = 255, nullable = false)
    private String contentencoding;
}
