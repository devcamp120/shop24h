package com.devcamp.home24h.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "address_map")
@Getter
@Setter
public class AddressMap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "address", length = 5000, nullable = false)
    private String address;

    @Column(name = "_lat", nullable = false)
    private double lat;

    @Column(name = "_lng", nullable = false)
    private double lng;
}
