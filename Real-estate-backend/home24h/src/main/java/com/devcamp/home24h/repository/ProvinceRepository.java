package com.devcamp.home24h.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.home24h.entity.Province;

@Repository
public interface ProvinceRepository extends JpaRepository<Province, Long> {
    
}
