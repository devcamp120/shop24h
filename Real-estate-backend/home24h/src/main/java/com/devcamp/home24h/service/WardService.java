package com.devcamp.home24h.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.entity.District;
import com.devcamp.home24h.entity.Province;
import com.devcamp.home24h.entity.Ward;
import com.devcamp.home24h.repository.WardRepository;

@Service
public class WardService {
    @Autowired
    private WardRepository wardRepository;

    public Page<Ward> getWards(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Ward> wards = wardRepository.findAll(pageable);
        return wards;
    }

    public Ward getWardById(Long id) {
        Optional<Ward> optional = wardRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    public Ward createWard(Long provinceId, Long districtId, Ward pWard) {
        pWard.setId(null);
        Province province = new Province(provinceId);
        District district = new District(districtId);
        pWard.setProvince(province);
        pWard.setDistrict(district);
        Ward ward = wardRepository.save(pWard);
        return ward;
    }

    public Ward updateWard(Long id, Long provinceId, Long districtId, Ward pWard) {
        Optional<Ward> optional = wardRepository.findById(id);
        if (optional.isPresent()) {
            pWard.setId(id);
            Province province = new Province(provinceId);
            District district = new District(districtId);
            pWard.setProvince(province);
            pWard.setDistrict(district);
            return wardRepository.save(pWard);
        } else {
            return null;
        }
    }

    public boolean deleteWard(Long id) {
        Optional<Ward> optional = wardRepository.findById(id);
        if (optional.isPresent()) {
            wardRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
}
