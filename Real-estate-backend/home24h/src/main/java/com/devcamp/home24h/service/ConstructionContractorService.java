package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.entity.AddressMap;
import com.devcamp.home24h.entity.ConstructionContractor;
import com.devcamp.home24h.repository.ConstructionContractorRepository;
import com.devcamp.home24h.repository.ProjectRepository;

@Service
public class ConstructionContractorService {
    @Autowired
    private ConstructionContractorRepository constructionContractorRepository;

    @Autowired
    private ProjectRepository projectRepository;

    public List<ConstructionContractor> getAllConstructionContractors() {
        List<ConstructionContractor> constructionContractors = new ArrayList<>();
        constructionContractorRepository.findAll().forEach(constructionContractors::add);
        return constructionContractors;
    }

    public Page<ConstructionContractor> getConstructionContractors(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<ConstructionContractor> constructionContractors = constructionContractorRepository.findAll(pageable);
        return constructionContractors;
    }

    public ConstructionContractor getConstructionContractorById(Long id) {
        Optional<ConstructionContractor> optional = constructionContractorRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    public ConstructionContractor createConstructionContractor(Long addressId,
            ConstructionContractor pConstructionContractor) {
        pConstructionContractor.setId(null);
        if (addressId == -1) {
            pConstructionContractor.setAddress(null);
        } else {
            AddressMap addressMap = new AddressMap();
            addressMap.setId(addressId);
            pConstructionContractor.setAddress(addressMap);
        }
        ConstructionContractor constructionContractor = constructionContractorRepository.save(pConstructionContractor);
        return constructionContractor;
    }

    public ConstructionContractor updateConstructionContractor(Long id, Long addressId,
            ConstructionContractor pConstructionContractor) {
        Optional<ConstructionContractor> optional = constructionContractorRepository.findById(id);
        if (optional.isPresent()) {
            pConstructionContractor.setId(id);
            if (addressId == -1) {
                pConstructionContractor.setAddress(null);
            } else {
                AddressMap addressMap = new AddressMap();
                addressMap.setId(addressId);
                pConstructionContractor.setAddress(addressMap);
            }
            return constructionContractorRepository.save(pConstructionContractor);
        } else {
            return null;
        }
    }

    public boolean deleteConstructionContractor(Long id) {
        Optional<ConstructionContractor> optional = constructionContractorRepository.findById(id);
        if (optional.isPresent()) {
            projectRepository.updateByConstructionContractorId(id);
            constructionContractorRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
}
