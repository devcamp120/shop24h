package com.devcamp.home24h.service;

import com.devcamp.home24h.entity.Token;

public interface TokenService {

    Token createToken(Token token);

    Token findByToken(String token);

    void deleteTokenByCreatedBy(Long userId);
}
