package com.devcamp.home24h.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.home24h.entity.Project;
import com.devcamp.home24h.model.Result;
import com.devcamp.home24h.service.ProjectService;
import com.devcamp.home24h.service.StorageService;

@CrossOrigin
@RestController
public class ProjectController {
    @Autowired
    private ProjectService projectService;

    @Autowired
    private StorageService storageService;

    @GetMapping("/projects/all")
    public ResponseEntity<List<Project>> getAllProjects() {
        try {
            List<Project> projects = projectService.getAllProjects();
            return new ResponseEntity<>(projects, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/projects")
    public ResponseEntity<Result> getProjects(@RequestParam(defaultValue = "0") String page,
            @RequestParam(defaultValue = "10") String size) {
        try {
            Page<Project> projects = projectService.getProjects(Integer.parseInt(page),
                    Integer.parseInt(size));
            return new ResponseEntity<>(new Result(projects.getTotalPages(), projects.getContent()),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/projects/{id}")
    public ResponseEntity<Project> getProjectById(@PathVariable Long id) {
        try {
            Project project = projectService.getProjectById(id);
            if (project != null) {
                return new ResponseEntity<>(project, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(project, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/projects")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> createProject(@RequestParam Long provinceId, @RequestParam Long districtId,
            @RequestParam Long wardId, @RequestParam Long streetId, @RequestParam Long investorId,
            @RequestParam Long constructionContractorId, @RequestParam Long designUnitId,
            @RequestBody Project pProject) {
        try {
            Project project = projectService.createProject(provinceId, districtId, wardId, streetId, investorId,
                    constructionContractorId, designUnitId, pProject);
            return new ResponseEntity<>(project, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Project: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/projects/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateProject(@PathVariable Long id, @RequestParam Long provinceId,
            @RequestParam Long districtId,
            @RequestParam Long wardId, @RequestParam Long streetId, @RequestParam Long investorId,
            @RequestParam Long constructionContractorId, @RequestParam Long designUnitId,
            @RequestBody Project pProject) {
        try {
            Project project = projectService.updateProject(id, provinceId, districtId, wardId, streetId, investorId,
                    constructionContractorId, designUnitId, pProject);
            if (project != null) {
                return new ResponseEntity<>(project, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(project, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Project: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/projects/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Project> deleteProject(@PathVariable Long id) {
        try {
            boolean isDeleted = projectService.deleteProject(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/upload/project-photo")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<String>> uploadProjectImages(@RequestParam("files") MultipartFile[] files) {
        try {
            List<String> fileNames = projectService.uploadProjectPhoto(files);
            return new ResponseEntity<>(fileNames, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/download/project-photo/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> downloadProjectPhoto(@PathVariable String filename) {
        String folder = "/projects/";
        Resource file = storageService.loadAsResource(folder, filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @DeleteMapping("/delete/project-photo/{filename:.+}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteProjectPhoto(@PathVariable String filename, @RequestParam Long id) {
        try {
            projectService.deleteProjectPhoto(filename, id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
