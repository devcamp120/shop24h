package com.devcamp.home24h.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.entity.District;
import com.devcamp.home24h.entity.Province;
import com.devcamp.home24h.model.Result;
import com.devcamp.home24h.service.ProvinceService;

@CrossOrigin
@RestController
public class ProvinceController {
    @Autowired
    private ProvinceService provinceService;

    @GetMapping("/provinces/all")
    public ResponseEntity<List<Province>> getAllProvinces() {
        try {
            List<Province> provinces = provinceService.getAllProvinces();
            return new ResponseEntity<>(provinces, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/provinces")
    public ResponseEntity<Result> getProvinces(@RequestParam(defaultValue = "0") String page,
            @RequestParam(defaultValue = "10") String size) {
        try {
            Page<Province> provinces = provinceService.getProvinces(Integer.parseInt(page),
                    Integer.parseInt(size));
            return new ResponseEntity<>(new Result(provinces.getTotalPages(), provinces.getContent()),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/provinces/{id}")
    public ResponseEntity<Province> getProvinceById(@PathVariable Long id) {
        try {
            Province province = provinceService.getProvinceById(id);
            if (province != null) {
                return new ResponseEntity<>(province, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/provinces/{id}/districts")
    public ResponseEntity<Set<District>> getDistrictsByProvinceId(@PathVariable Long id) {
        try {
            Set<District> districts = provinceService.getDistrictsByProvinceId(id);
            if (districts != null) {
                return new ResponseEntity<>(districts, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/provinces")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> createProvince(@RequestBody Province pProvince) {
        try {
            Province province = provinceService.createProvince(pProvince);
            return new ResponseEntity<>(province, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Province: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/provinces/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateProvince(@PathVariable Long id, @RequestBody Province pProvince) {
        try {
            Province province = provinceService.updateProvince(id, pProvince);
            if (province != null) {
                return new ResponseEntity<>(province, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(province, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Province: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/provinces/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Province> deleteProvince(@PathVariable Long id) {
        try {
            boolean isDeleted = provinceService.deleteProvince(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
