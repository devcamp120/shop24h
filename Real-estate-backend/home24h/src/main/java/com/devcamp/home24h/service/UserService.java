package com.devcamp.home24h.service;

import java.util.List;

import com.devcamp.home24h.entity.User;
import com.devcamp.home24h.security.UserPrincipal;

public interface UserService {
    User createUser(User user, long role);

    User updateUser(String username, String newUsername, String newPassword, long role, boolean deleted);

    boolean changePassword(String username, String oldPassword, String newPassword);

    UserPrincipal findByUsername(String username);

    boolean existsByUsername(String username);

    List<User> getUsersAdminHomeSeller();
}
