package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.home24h.entity.RegionLink;
import com.devcamp.home24h.repository.RegionLinkRepository;

@Service
public class RegionLinkService {
    @Autowired
    private RegionLinkRepository regionLinkRepository;

    @Autowired
    private StorageService storageService;

    public List<RegionLink> getAllRegionLinks() {
        List<RegionLink> regionLinks = new ArrayList<>();
        regionLinkRepository.findAll().forEach(regionLinks::add);
        return regionLinks;
    }

    public Page<RegionLink> getRegionLinks(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<RegionLink> regionLinks = regionLinkRepository.findAll(pageable);
        return regionLinks;
    }

    public RegionLink getRegionLinkById(Long id) {
        Optional<RegionLink> optional = regionLinkRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    public RegionLink createRegionLink(RegionLink pRegionLink) {
        pRegionLink.setId(null);
        RegionLink regionLink = regionLinkRepository.save(pRegionLink);
        return regionLink;
    }

    public RegionLink updateRegionLink(Long id, RegionLink pRegionLink) {
        Optional<RegionLink> optional = regionLinkRepository.findById(id);
        if (optional.isPresent()) {
            pRegionLink.setId(id);
            return regionLinkRepository.save(pRegionLink);
        } else {
            return null;
        }
    }

    public boolean deleteRegionLink(Long id) {
        Optional<RegionLink> optional = regionLinkRepository.findById(id);
        if (optional.isPresent()) {
            regionLinkRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    public List<String> uploadRegionLinkPhoto(MultipartFile[] files) {
        List<String> fileNames = new ArrayList<>();
        String folder = "/region-links/";
        for (MultipartFile file : files) {
            String fileName = storageService.store(folder, file);
            fileNames.add(fileName);
        }
        return fileNames;
    }

    public void deleteRegionLinkPhoto(String filename, Long id) {
        Optional<RegionLink> optional = regionLinkRepository.findById(id);
        if (optional.isPresent()) {
            String photo = optional.get().getPhoto();
            if (photo.contains(filename)) {
                storageService.delete("/region-links/", filename);
            }
        }
    }
}
