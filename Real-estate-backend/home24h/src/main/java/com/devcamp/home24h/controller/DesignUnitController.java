package com.devcamp.home24h.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.entity.DesignUnit;
import com.devcamp.home24h.model.Result;
import com.devcamp.home24h.service.DesignUnitService;

@CrossOrigin
@RestController
public class DesignUnitController {
    @Autowired
    private DesignUnitService designUnitService;

    @GetMapping("/design-units/all")
    public ResponseEntity<List<DesignUnit>> getAllDesignUnits() {
        try {
            List<DesignUnit> designUnits = designUnitService.getAllDesignUnits();
            return new ResponseEntity<>(designUnits, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/design-units")
    public ResponseEntity<Result> getDesignUnits(@RequestParam(defaultValue = "0") String page,
            @RequestParam(defaultValue = "10") String size) {
        try {
            Page<DesignUnit> designUnits = designUnitService.getDesignUnits(Integer.parseInt(page),
                    Integer.parseInt(size));
            return new ResponseEntity<>(new Result(designUnits.getTotalPages(), designUnits.getContent()),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/design-units/{id}")
    public ResponseEntity<DesignUnit> getDesignUnitById(@PathVariable Long id) {
        try {
            DesignUnit designUnit = designUnitService.getDesignUnitById(id);
            if (designUnit != null) {
                return new ResponseEntity<>(designUnit, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(designUnit, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/design-units")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> createDesignUnit(@RequestParam Long addressId, @RequestBody DesignUnit pDesignUnit) {
        try {
            DesignUnit designUnit = designUnitService.createDesignUnit(addressId, pDesignUnit);
            return new ResponseEntity<>(designUnit, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified DesignUnit: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/design-units/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateDesignUnit(@PathVariable Long id, @RequestParam Long addressId,
            @RequestBody DesignUnit pDesignUnit) {
        try {
            DesignUnit designUnit = designUnitService.updateDesignUnit(id, addressId, pDesignUnit);
            if (designUnit != null) {
                return new ResponseEntity<>(designUnit, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(designUnit, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified DesignUnit: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/design-units/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<DesignUnit> deleteDesignUnit(@PathVariable Long id) {
        try {
            boolean isDeleted = designUnitService.deleteDesignUnit(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
