package com.devcamp.home24h.model;

public class Result {
    private int totalPages;
    private Object content;

    public Result() {
    }

    public Result(int totalPages, Object content) {
        this.totalPages = totalPages;
        this.content = content;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }
}
