package com.devcamp.home24h.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.home24h.entity.AddressMap;

@Repository
public interface AddressMapRepository extends JpaRepository<AddressMap, Long> {
    
}
