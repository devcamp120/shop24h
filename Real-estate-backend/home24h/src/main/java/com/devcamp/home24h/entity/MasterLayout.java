package com.devcamp.home24h.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "master_layout")
@Getter
@Setter
public class MasterLayout {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", length = 1000, nullable = false)
    private String name;

    @Column(name = "description", length = 5000)
    private String description;

    @ManyToOne
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;

    @Column(name = "acreage", precision = 12, scale = 2)
    private BigDecimal acreage;

    @Column(name = "apartment_list", length = 1000)
    private String apartmentList;

    @Column(name = "photo", length = 5000)
    private String photo;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @Column(name = "date_create", updatable = false, nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateCreate;

    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    @Column(name = "date_update")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateUpdate;
}
