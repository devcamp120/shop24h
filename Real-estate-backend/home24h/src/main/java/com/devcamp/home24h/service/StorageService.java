package com.devcamp.home24h.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageService {

    void init();

    String store(String folder, MultipartFile file);

    Stream<Path> loadAll();

    Resource loadAsResource(String folder, String filename);

    void delete(String folder, String filename);

    void deleteAll();

}