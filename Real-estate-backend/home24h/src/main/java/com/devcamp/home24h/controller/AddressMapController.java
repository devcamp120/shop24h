package com.devcamp.home24h.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.entity.AddressMap;
import com.devcamp.home24h.model.Result;
import com.devcamp.home24h.service.AddressMapService;

@CrossOrigin
@RestController
public class AddressMapController {
    @Autowired
    private AddressMapService addressMapService;

    @GetMapping("/address-maps/all")
    public ResponseEntity<List<AddressMap>> getAllAddressMaps() {
        try {
            List<AddressMap> addressMaps = addressMapService.getAllAddressMaps();
            return new ResponseEntity<>(addressMaps, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/address-maps")
    public ResponseEntity<Result> getAddressMaps(@RequestParam(defaultValue = "0") String page,
            @RequestParam(defaultValue = "10") String size) {
        try {
            Page<AddressMap> addressMaps = addressMapService.getAddressMaps(Integer.parseInt(page),
                    Integer.parseInt(size));
            return new ResponseEntity<>(new Result(addressMaps.getTotalPages(), addressMaps.getContent()),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/address-maps/{id}")
    public ResponseEntity<AddressMap> getAddressMapById(@PathVariable Long id) {
        try {
            AddressMap addressMap = addressMapService.getAddressMapById(id);
            if (addressMap != null) {
                return new ResponseEntity<>(addressMap, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(addressMap, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/address-maps")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOME_SELLER')")
    public ResponseEntity<Object> createAddressMap(@RequestBody AddressMap pAddressMap) {
        try {
            AddressMap addressMap = addressMapService.createAddressMap(pAddressMap);
            return new ResponseEntity<>(addressMap, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified AddressMap: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/address-maps/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOME_SELLER')")
    public ResponseEntity<Object> updateAddressMap(@PathVariable Long id, @RequestBody AddressMap pAddressMap) {
        try {
            AddressMap addressMap = addressMapService.updateAddressMap(id, pAddressMap);
            if (addressMap != null) {
                return new ResponseEntity<>(addressMap, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(addressMap, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified AddressMap: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/address-maps/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOME_SELLER')")
    public ResponseEntity<AddressMap> deleteAddressMap(@PathVariable Long id) {
        try {
            boolean isDeleted = addressMapService.deleteAddressMap(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
