package com.devcamp.home24h.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "t_token")
@Getter
@Setter
public class Token extends BaseEntity {

	@Column(length = 5000)
	private String token;

	private Date tokenExpDate;

	private Long createdBy;

	@Override
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
}
