package com.devcamp.home24h.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.home24h.entity.Customer;
import com.devcamp.home24h.entity.District;
import com.devcamp.home24h.entity.Project;
import com.devcamp.home24h.entity.Province;
import com.devcamp.home24h.entity.Realestate;
import com.devcamp.home24h.entity.Street;
import com.devcamp.home24h.entity.Ward;
import com.devcamp.home24h.repository.CustomerRepository;
import com.devcamp.home24h.repository.RealestateRepository;
import com.devcamp.home24h.security.UserPrincipal;

@Service
public class RealestateService {
    @Autowired
    private RealestateRepository realestateRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private StorageService storageService;

    public Page<Realestate> getAllRealestates(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Realestate> realestates = realestateRepository.findAll(pageable);
        return realestates;
    }

    public Realestate getRealestateById(Long id) {
        Optional<Realestate> optional = realestateRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    public Page<Realestate> getRealestatesByUsername(String username, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Realestate> realestates = realestateRepository.findByUsername(username, pageable);
        return realestates;
    }

    public Page<Realestate> searchRealestates(Long provinceId, Long districtId, Long wardId, Long type,
            Long request, Long minPrice, Long maxPrice, BigDecimal minArea, BigDecimal maxArea, String sort, int page,
            int size) {
        if (sort.equals("1")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(Direction.DESC, "created_at"));
            return realestateRepository.searchRealestates(provinceId, districtId, wardId, type, request, minPrice,
                    maxPrice, minArea, maxArea, pageable);
        } else if (sort.equals("2")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(Direction.ASC, "created_at"));
            return realestateRepository.searchRealestates(provinceId, districtId, wardId, type, request, minPrice,
                    maxPrice, minArea, maxArea, pageable);
        } else if (sort.equals("3")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(Direction.DESC, "price"));
            return realestateRepository.searchRealestates(provinceId, districtId, wardId, type, request, minPrice,
                    maxPrice, minArea, maxArea, pageable);
        } else if (sort.equals("4")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(Direction.ASC, "price"));
            return realestateRepository.searchRealestates(provinceId, districtId, wardId, type, request, minPrice,
                    maxPrice, minArea, maxArea, pageable);
        } else {
            Pageable pageable = PageRequest.of(page, size);
            return realestateRepository.searchRealestates(provinceId, districtId, wardId, type, request, minPrice,
                    maxPrice, minArea, maxArea, pageable);
        }
    }

    public Page<Realestate> approveRealestates(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return realestateRepository.approveRealestates(pageable);
    }

    public Realestate createRealestate(String username, Long provinceId, Long districtId, Long wardId, Long streetId,
            Long projectId, Long customerId, Realestate pRealestate) {
        pRealestate.setId(null);
        if (provinceId == -1) {
            pRealestate.setProvince(null);
        } else {
            Province province = new Province(provinceId);
            pRealestate.setProvince(province);
            if (districtId == -1) {
                pRealestate.setDistrict(null);
            } else {
                District district = new District(districtId);
                pRealestate.setDistrict(district);
                if (wardId == -1) {
                    pRealestate.setWard(null);
                } else {
                    Ward ward = new Ward(wardId);
                    pRealestate.setWard(ward);
                }
                if (streetId == -1) {
                    pRealestate.setStreet(null);
                } else {
                    Street street = new Street(streetId);
                    pRealestate.setStreet(street);
                }
            }
        }
        if (projectId == -1) {
            pRealestate.setProject(null);
        } else {
            Project project = new Project(projectId);
            pRealestate.setProject(project);
        }
        if (customerId == -1) {
            pRealestate.setCustomer(null);
        } else {
            Customer customer = new Customer();
            customer.setId(customerId);
            pRealestate.setCustomer(customer);
        }
        UserPrincipal user = userService.findByUsername(username);
        Collection authorities = user.getAuthorities();
        if (authorities.contains("ROLE_ADMIN") || authorities.contains("ROLE_HOME_SELLER")) {
            pRealestate.setDeleted(false);
        } else {
            pRealestate.setDeleted(true);
            Customer _customer = customerRepository.findByCreatedBy(user.getUserId());
            if (_customer != null) {
                pRealestate.setCustomer(_customer);
            } else {
                pRealestate.setCustomer(null);
            }
        }
        Realestate realestate = realestateRepository.save(pRealestate);
        return realestate;
    }

    public boolean editableRealestate(String username, Long id) {
        UserPrincipal user = userService.findByUsername(username);
        Optional<Realestate> optional = realestateRepository.findById(id);
        if (user != null && optional.isPresent()) {
            Long userId = user.getUserId();
            Collection authorities = user.getAuthorities();
            List<Long> idsAdminHomeSeller = new ArrayList<>();
            userService.getUsersAdminHomeSeller()
                    .forEach(userAdminHomeSeller -> idsAdminHomeSeller.add(userAdminHomeSeller.getId()));
            Long createdBy = optional.get().getCreatedBy();
            if (authorities.contains("ROLE_ADMIN")
                    || (authorities.contains("ROLE_HOME_SELLER") && !idsAdminHomeSeller.contains(createdBy))
                    || userId == createdBy) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean deleteableRealestate(String username, Long id) {
        UserPrincipal user = userService.findByUsername(username);
        Optional<Realestate> optional = realestateRepository.findById(id);
        if (user != null && optional.isPresent()) {
            Long userId = user.getUserId();
            Collection authorities = user.getAuthorities();
            Long createdBy = optional.get().getCreatedBy();
            if (authorities.contains("ROLE_ADMIN") || userId == createdBy) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public Realestate updateRealestate(String username, Long id, Long provinceId, Long districtId, Long wardId,
            Long streetId, Long projectId, Long customerId, Realestate pRealestate) {
        Optional<Realestate> optional = realestateRepository.findById(id);
        if (optional.isPresent()) {
            if (editableRealestate(username, id)) {
                if (provinceId == -1) {
                    pRealestate.setProvince(null);
                } else {
                    Province province = new Province(provinceId);
                    pRealestate.setProvince(province);
                    if (districtId == -1) {
                        pRealestate.setDistrict(null);
                    } else {
                        District district = new District(districtId);
                        pRealestate.setDistrict(district);
                        if (wardId == -1) {
                            pRealestate.setWard(null);
                        } else {
                            Ward ward = new Ward(wardId);
                            pRealestate.setWard(ward);
                        }
                        if (streetId == -1) {
                            pRealestate.setStreet(null);
                        } else {
                            Street street = new Street(streetId);
                            pRealestate.setStreet(street);
                            ;
                        }
                    }
                }
                if (projectId == -1) {
                    pRealestate.setProject(null);
                } else {
                    Project project = new Project(projectId);
                    pRealestate.setProject(project);
                }
                if (customerId == -1) {
                    pRealestate.setCustomer(null);
                } else {
                    Customer customer = new Customer();
                    customer.setId(customerId);
                    pRealestate.setCustomer(customer);
                }
                pRealestate.setId(id);
                UserPrincipal user = userService.findByUsername(username);
                Collection authorities = user.getAuthorities();
                if (authorities.contains("ROLE_ADMIN") || authorities.contains("ROLE_HOME_SELLER")) {
                    pRealestate.setDeleted(false);
                } else {
                    pRealestate.setDeleted(true);
                    pRealestate.setCustomer(customerRepository.findByCreatedBy(user.getUserId()));
                }
                Realestate realestate = realestateRepository.saveAndFlush(pRealestate);
                return realestate;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public boolean deleteRealestate(String username, Long id) {
        Optional<Realestate> optional = realestateRepository.findById(id);
        if (optional.isPresent()) {
            if (deleteableRealestate(username, id)) {
                String photo = optional.get().getPhoto();
                if (photo != null && photo != "") {
                    String[] arr = photo.split(",");
                    for (String filename : arr) {
                        storageService.delete("/realestates/", filename);
                    }
                }
                realestateRepository.deleteById(id);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public List<String> uploadRealestatePhoto(MultipartFile[] files) {
        List<String> fileNames = new ArrayList<>();
        String folder = "/realestates/";
        for (MultipartFile file : files) {
            String fileName = storageService.store(folder, file);
            fileNames.add(fileName);
        }
        return fileNames;
    }

    public List<String> updateRealestatePhoto(String username, Long id, MultipartFile[] files) {
        Optional<Realestate> optional = realestateRepository.findById(id);
        if (optional.isPresent()) {
            if (editableRealestate(username, id)) {
                List<String> fileNames = new ArrayList<>();
                String folder = "/realestates/";
                for (MultipartFile file : files) {
                    String fileName = storageService.store(folder, file);
                    fileNames.add(fileName);
                }
                return fileNames;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public void deleteRealestatePhoto(String username, String filename, Long id) {
        Optional<Realestate> optional = realestateRepository.findById(id);
        if (optional.isPresent()) {
            if (deleteableRealestate(username, id)) {
                String photo = optional.get().getPhoto();
                if (photo.contains(filename)) {
                    storageService.delete("/realestates/", filename);
                }
            }
        }
    }
}
