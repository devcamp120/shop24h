package com.devcamp.home24h.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.home24h.entity.MasterLayout;
import com.devcamp.home24h.model.Result;
import com.devcamp.home24h.service.MasterLayoutService;
import com.devcamp.home24h.service.StorageService;

@CrossOrigin
@RestController
public class MasterLayoutController {
    @Autowired
    private MasterLayoutService masterLayoutService;

    @Autowired
    private StorageService storageService;

    @GetMapping("/master-layouts/all")
    public ResponseEntity<List<MasterLayout>> getAllMasterLayouts() {
        try {
            List<MasterLayout> masterLayouts = masterLayoutService.getAllMasterLayouts();
            return new ResponseEntity<>(masterLayouts, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/master-layouts")
    public ResponseEntity<Result> getMasterLayouts(@RequestParam(defaultValue = "0") String page,
            @RequestParam(defaultValue = "10") String size) {
        try {
            Page<MasterLayout> masterLayouts = masterLayoutService.getMasterLayouts(Integer.parseInt(page),
                    Integer.parseInt(size));
            return new ResponseEntity<>(new Result(masterLayouts.getTotalPages(), masterLayouts.getContent()),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/master-layouts/{id}")
    public ResponseEntity<MasterLayout> getMasterLayoutById(@PathVariable Long id) {
        try {
            MasterLayout masterLayout = masterLayoutService.getMasterLayoutById(id);
            if (masterLayout != null) {
                return new ResponseEntity<>(masterLayout, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(masterLayout, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/master-layouts")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> createMasterLayout(@RequestParam Long projectId,
            @RequestBody MasterLayout pMasterLayout) {
        try {
            MasterLayout masterLayout = masterLayoutService.createMasterLayout(projectId, pMasterLayout);
            return new ResponseEntity<>(masterLayout, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified MasterLayout: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/master-layouts/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateMasterLayout(@PathVariable Long id, @RequestParam Long projectId,
            @RequestBody MasterLayout pMasterLayout) {
        try {
            MasterLayout masterLayout = masterLayoutService.updateMasterLayout(id, projectId, pMasterLayout);
            if (masterLayout != null) {
                return new ResponseEntity<>(masterLayout, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(masterLayout, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified MasterLayout: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/master-layouts/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<MasterLayout> deleteMasterLayout(@PathVariable Long id) {
        try {
            boolean isDeleted = masterLayoutService.deleteMasterLayout(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/upload/master-layout-photo")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<String>> uploadMasterLayoutImages(@RequestParam("files") MultipartFile[] files) {
        try {
            List<String> fileNames = masterLayoutService.uploadMasterLayoutPhoto(files);
            return new ResponseEntity<>(fileNames, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/download/master-layout-photo/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> downloadMasterLayoutPhoto(@PathVariable String filename) {
        String folder = "/master-layouts/";
        Resource file = storageService.loadAsResource(folder, filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @DeleteMapping("/delete/master-layout-photo/{filename:.+}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteMasterLayoutPhoto(@PathVariable String filename, @RequestParam Long id) {
        try {
            masterLayoutService.deleteMasterLayoutPhoto(filename, id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
