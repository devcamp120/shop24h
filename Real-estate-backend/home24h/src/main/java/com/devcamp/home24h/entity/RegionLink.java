package com.devcamp.home24h.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "region_link")
@Getter
@Setter
public class RegionLink {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", length = 1000, nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "photo", length = 5000)
    private String photo;

    @Column(name = "address", length = 5000)
    private String address;

    @Column(name = "_lat")
    private Double lat;

    @Column(name = "_lng")
    private Double lng;
}
