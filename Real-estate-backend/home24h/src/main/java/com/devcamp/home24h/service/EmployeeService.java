package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.home24h.entity.Employee;
import com.devcamp.home24h.entity.User;
import com.devcamp.home24h.model.Activated;
import com.devcamp.home24h.repository.EmployeeRepository;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private StorageService storageService;

    public Page<Employee> getEmployees(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Employee> employees = employeeRepository.findAll(pageable);
        return employees;
    }

    public Employee getEmployeeById(Long id) {
        Optional<Employee> optional = employeeRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    public List<Employee> getEmployeeLike(String keyword) {
        List<Employee> employees = new ArrayList<>();
        employeeRepository.getEmployeeLike(keyword).forEach(employees::add);
        return employees;
    }

    public Employee createEmployee(Long reportsTo, Employee pEmployee) {
        pEmployee.setId(null);
        if (reportsTo == -1) {
            pEmployee.setReportsTo(null);
        } else {
            Employee employee = new Employee();
            employee.setId(reportsTo);
            pEmployee.setReportsTo(employee);
        }
        if (pEmployee.getUserLevel() != 1 && pEmployee.getUserLevel() != 2 && pEmployee.getUserLevel() != 3) {
            pEmployee.setUserLevel(3);
        }
        Employee employee = employeeRepository.save(pEmployee);
        User user = new User();
        user.setUsername(employee.getUsername());
        user.setPassword(new BCryptPasswordEncoder().encode(employee.getPassword()));
        boolean deleted = false;
        if (employee.getActivated() == Activated.Y) {
            deleted = false;
        } else {
            deleted = true;
        }
        user.setDeleted(deleted);
        userService.createUser(user, employee.getUserLevel());
        return employee;
    }

    public Employee updateEmployee(Long id, Long reportsTo, Employee pEmployee) {
        Optional<Employee> optional = employeeRepository.findById(id);
        if (optional.isPresent()) {
            pEmployee.setId(id);
            if (reportsTo == -1) {
                pEmployee.setReportsTo(null);
            } else {
                Employee employee = new Employee();
                employee.setId(reportsTo);
                pEmployee.setReportsTo(employee);
            }
            if (pEmployee.getUserLevel() != 1 && pEmployee.getUserLevel() != 2 && pEmployee.getUserLevel() != 3) {
                pEmployee.setUserLevel(3);
            }
            String username = optional.get().getUsername();
            String newUsername = pEmployee.getUsername();
            String newPassword = pEmployee.getPassword();
            int userLevel = pEmployee.getUserLevel();
            boolean deleted = false;
            if (pEmployee.getActivated() == Activated.Y) {
                deleted = false;
            } else {
                deleted = true;
            }
            userService.updateUser(username, newUsername, newPassword, userLevel, deleted);
            Employee employee = employeeRepository.save(pEmployee);
            return employee;
        } else {
            return null;
        }
    }

    public boolean deleteEmployee(Long id) {
        Optional<Employee> optional = employeeRepository.findById(id);
        if (optional.isPresent()) {
            String photo = optional.get().getPhoto();
            if (photo != null && photo != "") {
                storageService.delete("/employees/", photo);
            }
            String username = optional.get().getUsername();
            String password = optional.get().getPassword();
            int userLevel = optional.get().getUserLevel();
            boolean deleted = true;
            userService.updateUser(username, username, password, userLevel, deleted);
            Set<Employee> employees = optional.get().getEmployees();
            employees.forEach(employee -> {
                employee.setReportsTo(null);
                employeeRepository.save(employee);
            });
            employeeRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    public String uploadEmployeePhoto(MultipartFile file) {
        String folder = "/employees/";
        String fileName = storageService.store(folder, file);
        return fileName;
    }

    public void deleteEmployeePhoto(String filename, Long id) {
        Optional<Employee> optional = employeeRepository.findById(id);
        if (optional.isPresent()) {
            String photo = optional.get().getPhoto();
            if (photo.contains(filename)) {
                storageService.delete("/employees/", filename);
            }
        }
    }
}
