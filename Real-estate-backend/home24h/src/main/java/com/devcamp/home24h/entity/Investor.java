package com.devcamp.home24h.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "investor")
@Getter
@Setter
public class Investor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", length = 1000, nullable = false)
    private String name;

    @Column(name = "description", length = 5000)
    private String description;

    @OneToOne
    @JoinColumn(name = "address")
    private AddressMap address;

    @Column(name = "phone", length = 50)
    private String phone;

    @Column(name = "phone2", length = 50)
    private String phone2;

    @Column(name = "fax", length = 50)
    private String fax;

    @Column(name = "email", length = 200)
    private String email;

    @Column(name = "website", length = 1000)
    private String website;

    @Column(name = "note")
    private String note;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "investor")
    @JsonIgnore
    private Set<Project> projects;
}
