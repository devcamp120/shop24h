package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.entity.District;
import com.devcamp.home24h.entity.Province;
import com.devcamp.home24h.repository.ProvinceRepository;

@Service
public class ProvinceService {
    @Autowired
    private ProvinceRepository provinceRepository;

    public List<Province> getAllProvinces() {
        List<Province> provinces = new ArrayList<>();
        provinceRepository.findAll().forEach(provinces::add);
        return provinces;
    }

    public Page<Province> getProvinces(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Province> provinces = provinceRepository.findAll(pageable);
        return provinces;
    }

    public Province getProvinceById(Long id) {
        Optional<Province> optional = provinceRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    public Set<District> getDistrictsByProvinceId(Long id) {
        Optional<Province> optional = provinceRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get().getDistricts();
        } else {
            return null;
        }
    }

    public Province createProvince(Province pProvince) {
        pProvince.setId(null);
        Province province = provinceRepository.save(pProvince);
        return province;
    }

    public Province updateProvince(Long id, Province pProvince) {
        Optional<Province> optional = provinceRepository.findById(id);
        if (optional.isPresent()) {
            pProvince.setId(id);
            return provinceRepository.save(pProvince);
        } else {
            return null;
        }
    }

    public boolean deleteProvince(Long id) {
        Optional<Province> optional = provinceRepository.findById(id);
        if (optional.isPresent()) {
            provinceRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
}
