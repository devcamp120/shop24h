package com.devcamp.home24h.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "province")
@Getter
@Setter
public class Province {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "_name")
    private String name;

    @Column(name = "_code")
    private String code;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "province")
    @JsonIgnore
    private Set<District> districts;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "province")
    @JsonIgnore
    private Set<Ward> wards;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "province")
    @JsonIgnore
    private Set<Street> streets;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "province")
    @JsonIgnore
    private Set<Realestate> realestates;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "province")
    @JsonIgnore
    private Set<Project> projects;

    public Province() {
    }

    public Province(Long id) {
        this.id = id;
    }
}
