package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.home24h.entity.MasterLayout;
import com.devcamp.home24h.entity.Project;
import com.devcamp.home24h.repository.MasterLayoutRepository;

@Service
public class MasterLayoutService {
    @Autowired
    private MasterLayoutRepository masterLayoutRepository;

    @Autowired
    private StorageService storageService;

    public List<MasterLayout> getAllMasterLayouts() {
        List<MasterLayout> masterLayouts = new ArrayList<>();
        masterLayoutRepository.findAll().forEach(masterLayouts::add);
        return masterLayouts;
    }

    public Page<MasterLayout> getMasterLayouts(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<MasterLayout> masterLayouts = masterLayoutRepository.findAll(pageable);
        return masterLayouts;
    }

    public MasterLayout getMasterLayoutById(Long id) {
        Optional<MasterLayout> optional = masterLayoutRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    public MasterLayout createMasterLayout(Long projectId, MasterLayout pMasterLayout) {
        pMasterLayout.setId(null);
        if (projectId == -1) {
            pMasterLayout.setProject(null);
        } else {
            Project project = new Project();
            project.setId(projectId);
            pMasterLayout.setProject(project);
        }
        pMasterLayout.setDateCreate(new Date());
        MasterLayout masterLayout = masterLayoutRepository.save(pMasterLayout);
        return masterLayout;
    }

    public MasterLayout updateMasterLayout(Long id, Long projectId, MasterLayout pMasterLayout) {
        Optional<MasterLayout> optional = masterLayoutRepository.findById(id);
        if (optional.isPresent()) {
            pMasterLayout.setId(id);
            if (projectId == -1) {
                pMasterLayout.setProject(null);
            } else {
                Project project = new Project();
                project.setId(projectId);
                pMasterLayout.setProject(project);
            }
            pMasterLayout.setDateUpdate(new Date());
            return masterLayoutRepository.save(pMasterLayout);
        } else {
            return null;
        }
    }

    public boolean deleteMasterLayout(Long id) {
        Optional<MasterLayout> optional = masterLayoutRepository.findById(id);
        if (optional.isPresent()) {
            String photo = optional.get().getPhoto();
            if (photo != null && photo != "") {
                String[] arr = photo.split(",");
                for (String filename : arr) {
                    storageService.delete("/master-layouts/", filename);
                }
            }
            masterLayoutRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    public List<String> uploadMasterLayoutPhoto(MultipartFile[] files) {
        List<String> fileNames = new ArrayList<>();
        String folder = "/master-layouts/";
        for (MultipartFile file : files) {
            String fileName = storageService.store(folder, file);
            fileNames.add(fileName);
        }
        return fileNames;
    }

    public void deleteMasterLayoutPhoto(String filename, Long id) {
        Optional<MasterLayout> optional = masterLayoutRepository.findById(id);
        if (optional.isPresent()) {
            String photo = optional.get().getPhoto();
            if (photo.contains(filename)) {
                storageService.delete("/master-layouts/", filename);
            }
        }
    }
}
