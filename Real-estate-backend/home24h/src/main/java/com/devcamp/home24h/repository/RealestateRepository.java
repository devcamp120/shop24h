package com.devcamp.home24h.repository;

import java.math.BigDecimal;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.home24h.entity.Realestate;

@Repository
public interface RealestateRepository extends JpaRepository<Realestate, Long> {
    @Query(value = "SELECT * FROM realestate WHERE (:provinceId = province_id OR :provinceId = -1) AND (:districtId = district_id OR :districtId = -1) AND (:wardId = wards_id OR :wardId = -1) AND (:type = type OR :type = -1) AND (:request = request OR :request = -1) AND (price BETWEEN :minPrice AND :maxPrice) AND (acreage BETWEEN :minArea AND :maxArea) AND deleted = 0", countQuery = "SELECT count(*) FROM realestate WHERE (:provinceId = province_id OR :provinceId = -1) AND (:districtId = district_id OR :districtId = -1) AND (:wardId = wards_id OR :wardId = -1) AND (:type = type OR :type = -1) AND (:request = request OR :request = -1) AND (price BETWEEN :minPrice AND :maxPrice) AND (acreage BETWEEN :minArea AND :maxArea) AND deleted = 0", nativeQuery = true)
    Page<Realestate> searchRealestates(Long provinceId, Long districtId, Long wardId, Long type, Long request,
            Long minPrice, Long maxPrice, BigDecimal minArea, BigDecimal maxArea, Pageable pageable);

    @Query(value = "SELECT realestate.* FROM realestate JOIN t_user ON realestate.created_by = t_user.id WHERE t_user.username = :username ORDER BY realestate.created_at DESC", countQuery = "SELECT count(*) FROM realestate JOIN t_user ON realestate.created_by = t_user.id WHERE t_user.username = :username", nativeQuery = true)
    Page<Realestate> findByUsername(String username, Pageable pageable);

    @Query(value = "SELECT * FROM realestate WHERE deleted = 1", countQuery = "SELECT count(*) FROM realestate WHERE deleted = 1", nativeQuery = true)
    Page<Realestate> approveRealestates(Pageable pageable);

    @Transactional
    @Modifying
    @Query(value = "UPDATE realestate SET project_id = NULL WHERE project_id = :id", nativeQuery = true)
    void updateByProjectId(Long id);
}
