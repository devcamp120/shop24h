package com.devcamp.home24h;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class Home24hApplication {

    public static void main(String[] args) {
        SpringApplication.run(Home24hApplication.class, args);
    }

}
