package com.devcamp.home24h.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.entity.Customer;
import com.devcamp.home24h.entity.Token;
import com.devcamp.home24h.entity.User;
import com.devcamp.home24h.model.UserInfo;
import com.devcamp.home24h.model.UserRegister;
import com.devcamp.home24h.security.JwtUtil;
import com.devcamp.home24h.security.UserPrincipal;
import com.devcamp.home24h.service.CustomerService;
import com.devcamp.home24h.service.TokenService;
import com.devcamp.home24h.service.UserService;

@CrossOrigin
@RestController
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping("/register")
    public ResponseEntity<Object> register(@RequestBody UserRegister userRegister) {
        try {
            User user = new User();
            user.setUsername(userRegister.getUsername());
            user.setPassword(new BCryptPasswordEncoder().encode(userRegister.getPassword()));
            User _user = userService.createUser(user, 3);
            Customer customer = new Customer();
            customer.setCreatedBy(_user.getId());
            customer.setContactName(_user.getUsername());
            customer.setMobile(userRegister.getMobile());
            customer.setEmail(userRegister.getEmail());
            customerService.createCustomer(customer);
            return new ResponseEntity<>(_user, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Register: " + e.getCause().getCause().getMessage());
        }

    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody User user) {
        try {
            UserPrincipal userPrincipal = userService.findByUsername(user.getUsername());
            if (null == user || !new BCryptPasswordEncoder().matches(user.getPassword(), userPrincipal.getPassword())
                    || userPrincipal.isDeleted()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Tài khoản hoặc mật khẩu không chính xác, hoặc chưa được kích hoạt.");
            }
            Token token = new Token();
            token.setToken(jwtUtil.generateToken(userPrincipal));
            token.setTokenExpDate(jwtUtil.generateExpirationDate());
            token.setCreatedBy(userPrincipal.getUserId());
            tokenService.createToken(token);
            return ResponseEntity.ok(token.getToken());
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Login: " + e.getCause().getCause().getMessage());
        }

    }

    @PostMapping("/admin-seller/login")
    public ResponseEntity<?> loginAdmin(@RequestBody User user) {
        try {
            UserPrincipal userPrincipal = userService.findByUsername(user.getUsername());
            boolean isAdminSeller = (userPrincipal.getAuthorities().contains("ROLE_ADMIN")
                    || userPrincipal.getAuthorities().contains("ROLE_HOME_SELLER"));
            if (null == user || !new BCryptPasswordEncoder().matches(user.getPassword(), userPrincipal.getPassword())
                    || !isAdminSeller || userPrincipal.isDeleted()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Tài khoản hoặc mật khẩu không chính xác, hoặc không có quyền truy cập, chưa được kích hoạt.");
            }
            Token token = new Token();
            token.setToken(jwtUtil.generateToken(userPrincipal));
            token.setTokenExpDate(jwtUtil.generateExpirationDate());
            token.setCreatedBy(userPrincipal.getUserId());
            tokenService.createToken(token);
            return ResponseEntity.ok(token.getToken());
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Login: " + e.getCause().getCause().getMessage());
        }

    }

    @GetMapping("/users/me")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOME_SELLER', 'CUSTOMER')")
    public ResponseEntity<String> getUsername() {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            return new ResponseEntity<>(username, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/admin-seller/users/me")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOME_SELLER')")
    public ResponseEntity<UserInfo> getAdminSellerUsername() {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            long role = 0;
            UserPrincipal userPrincipal = userService.findByUsername(username);
            if (userPrincipal.getAuthorities().contains("ROLE_ADMIN")) {
                role = 1;
            } else if (userPrincipal.getAuthorities().contains("ROLE_HOME_SELLER")) {
                role = 2;
            }
            return new ResponseEntity<>(new UserInfo(userPrincipal.getUserId(), username, role), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users/{username}/exists")
    public ResponseEntity<Boolean> checkUserByUsername(@PathVariable String username) {
        try {
            boolean check = userService.existsByUsername(username);
            return new ResponseEntity<>(check, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/users/change-password")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOME_SELLER', 'CUSTOMER')")
    public ResponseEntity<Boolean> changePassword(@RequestHeader(name = "Authorization") String authorizationHeader,
            @RequestParam String oldPassword, @RequestParam String newPassword) {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            boolean isChanged = userService.changePassword(username, oldPassword, newPassword);
            if (isChanged) {
                tokenService.deleteTokenByCreatedBy(userService.findByUsername(username).getUserId());
            }
            return new ResponseEntity<>(isChanged, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
