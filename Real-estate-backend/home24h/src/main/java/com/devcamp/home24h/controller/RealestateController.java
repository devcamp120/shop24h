package com.devcamp.home24h.controller;

import java.math.BigDecimal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.home24h.entity.Realestate;
import com.devcamp.home24h.model.Result;
import com.devcamp.home24h.service.RealestateService;
import com.devcamp.home24h.service.StorageService;

@CrossOrigin
@RestController
public class RealestateController {
    @Autowired
    private RealestateService realestateService;

    @Autowired
    private StorageService storageService;

    @GetMapping("/realestates")
    public ResponseEntity<Result> getAllRealestates(@RequestParam(defaultValue = "0") String page,
            @RequestParam(defaultValue = "10") String size) {
        try {
            Page<Realestate> realestates = realestateService.getAllRealestates(Integer.parseInt(page),
                    Integer.parseInt(size));
            return new ResponseEntity<>(new Result(realestates.getTotalPages(), realestates.getContent()),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestates/{id}")
    public ResponseEntity<Realestate> getRealestateById(@PathVariable Long id) {
        try {
            Realestate realestate = realestateService.getRealestateById(id);
            if (realestate != null) {
                return new ResponseEntity<>(realestate, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestates/search")
    public ResponseEntity<Result> searchRealestates(@RequestParam Long provinceId,
            @RequestParam Long districtId, @RequestParam Long wardId, @RequestParam Long type,
            @RequestParam Long request, @RequestParam(defaultValue = "0") Long minPrice,
            @RequestParam(defaultValue = "10000000000000") Long maxPrice,
            @RequestParam(defaultValue = "0") BigDecimal minArea,
            @RequestParam(defaultValue = "100000000") BigDecimal maxArea, @RequestParam String sort,
            @RequestParam(defaultValue = "0") String page,
            @RequestParam(defaultValue = "12") String size) {
        try {
            Page<Realestate> realestates = realestateService.searchRealestates(provinceId, districtId, wardId, type,
                    request, minPrice, maxPrice, minArea, maxArea, sort, Integer.parseInt(page),
                    Integer.parseInt(size));
            return new ResponseEntity<>(new Result(realestates.getTotalPages(), realestates.getContent()),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestates/approve")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOME_SELLER')")
    public ResponseEntity<Result> approveRealestates(@RequestParam(defaultValue = "0") String page,
            @RequestParam(defaultValue = "10") String size) {
        try {
            Page<Realestate> realestates = realestateService.approveRealestates(Integer.parseInt(page),
                    Integer.parseInt(size));
            return new ResponseEntity<>(new Result(realestates.getTotalPages(), realestates.getContent()),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestates/posts")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOME_SELLER', 'CUSTOMER')")
    public ResponseEntity<Result> getRealestatesByUsername(@RequestParam(defaultValue = "0") String page,
            @RequestParam(defaultValue = "12") String size) {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            Page<Realestate> realestates = realestateService.getRealestatesByUsername(username, Integer.parseInt(page),
                    Integer.parseInt(size));

            return new ResponseEntity<>(new Result(realestates.getTotalPages(), realestates.getContent()),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/realestates")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOME_SELLER', 'CUSTOMER')")
    public ResponseEntity<Object> createRealestate(@RequestParam Long provinceId, @RequestParam Long districtId,
            @RequestParam Long wardId, @RequestParam Long streetId, @RequestParam Long projectId,
            @RequestParam(defaultValue = "-1") Long customerId, @RequestBody Realestate pRealestate) {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            Realestate realestate = realestateService.createRealestate(username, provinceId, districtId, wardId,
                    streetId, projectId, customerId, pRealestate);
            return new ResponseEntity<>(realestate, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Realestate: " + e.getCause().getCause().getMessage());
        }
    }

    @GetMapping("/realestates/{id}/editable")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOME_SELLER', 'CUSTOMER')")
    public ResponseEntity<Boolean> editableRealestate(@PathVariable Long id) {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            if (realestateService.editableRealestate(username, id)) {
                return new ResponseEntity<>(true, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(false, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestates/{id}/deleteable")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOME_SELLER', 'CUSTOMER')")
    public ResponseEntity<Boolean> deleteableRealestate(@PathVariable Long id) {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            if (realestateService.deleteableRealestate(username, id)) {
                return new ResponseEntity<>(true, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(false, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/realestates/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOME_SELLER', 'CUSTOMER')")
    public ResponseEntity<Object> updateRealestate(@PathVariable Long id, @RequestParam Long provinceId,
            @RequestParam Long districtId, @RequestParam Long wardId, @RequestParam Long streetId,
            @RequestParam Long projectId, @RequestParam(defaultValue = "-1") Long customerId,
            @RequestBody Realestate pRealestate) {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            Realestate realestate = realestateService.updateRealestate(username, id, provinceId, districtId, wardId,
                    streetId, projectId, customerId, pRealestate);
            if (realestate != null) {
                return new ResponseEntity<>(realestate, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(realestate, HttpStatus.FORBIDDEN);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Realestate: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/realestates/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOME_SELLER', 'CUSTOMER')")
    public ResponseEntity<Realestate> deleteRealestate(@PathVariable Long id) {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            boolean isDeleted = realestateService.deleteRealestate(username, id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/upload/realestate-photo")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOME_SELLER', 'CUSTOMER')")
    public ResponseEntity<List<String>> uploadRealestateImages(@RequestParam("files") MultipartFile[] files) {
        try {
            List<String> fileNames = realestateService.uploadRealestatePhoto(files);
            return new ResponseEntity<>(fileNames, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/realestate-photo/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOME_SELLER', 'CUSTOMER')")
    public ResponseEntity<List<String>> updateRealestateImages(@PathVariable Long id,
            @RequestParam("files") MultipartFile[] files) {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            List<String> fileNames = realestateService.updateRealestatePhoto(username, id, files);
            if (fileNames == null) {
                return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
            }
            return new ResponseEntity<>(fileNames, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/download/realestate-photo/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> downloadRealestatePhoto(@PathVariable String filename) {
        String folder = "/realestates/";
        Resource file = storageService.loadAsResource(folder, filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @DeleteMapping("/delete/reastate-photo/{filename:.+}")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOME_SELLER', 'CUSTOMER')")
    public void deleteRealestatePhoto(@PathVariable String filename, @RequestParam Long id) {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            realestateService.deleteRealestatePhoto(username, filename, id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
