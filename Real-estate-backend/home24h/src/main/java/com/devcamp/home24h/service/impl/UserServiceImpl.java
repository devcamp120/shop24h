package com.devcamp.home24h.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.entity.Role;
import com.devcamp.home24h.entity.User;
import com.devcamp.home24h.repository.UserRepository;
import com.devcamp.home24h.security.UserPrincipal;
import com.devcamp.home24h.service.UserService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public User createUser(User user, long role) {
        user.setId(null);
        Set<Role> roles = new HashSet<>();
        Role r = new Role();
        if (role == 1 || role == 2 && role == 3) {
            r.setId(role);
        } else {
            r.setId(3l);
        }
        roles.add(r);
        user.setRoles(roles);
        return userRepository.saveAndFlush(user);
    }

    @Override
    public User updateUser(String username, String newUsername, String newPassword, long role, boolean deleted) {
        User user = userRepository.findByUsername(username);
        if (user != null) {
            user.setUsername(newUsername);
            user.setPassword(new BCryptPasswordEncoder().encode(newPassword));
            user.setDeleted(deleted);
            Set<Role> roles = new HashSet<>();
            Role r = new Role();
            r.setId(role);
            roles.add(r);
            user.setRoles(roles);
            return userRepository.saveAndFlush(user);
        } else {
            user = new User();
            user.setUsername(newUsername);
            user.setPassword(new BCryptPasswordEncoder().encode(newPassword));
            user.setDeleted(deleted);
            Set<Role> roles = new HashSet<>();
            Role r = new Role();
            r.setId(role);
            roles.add(r);
            user.setRoles(roles);
            return userRepository.saveAndFlush(user);
        }

    }

    @Override
    public boolean changePassword(String username, String oldPassword, String newPassword) {
        User user = userRepository.findByUsername(username);
        if (user != null && new BCryptPasswordEncoder().matches(oldPassword, user.getPassword())) {
            user.setPassword(new BCryptPasswordEncoder().encode(newPassword));
            userRepository.saveAndFlush(user);
            return true;
        }
        return false;
    }

    @Override
    public UserPrincipal findByUsername(String username) {
        User user = userRepository.findByUsername(username);
        UserPrincipal userPrincipal = new UserPrincipal();
        if (null != user) {
            Set<String> authorities = new HashSet<>();
            if (null != user.getRoles())
                user.getRoles().forEach(r -> {
                    authorities.add(r.getRoleKey());
                    r.getPermissions().forEach(p -> authorities.add(p.getPermissionKey()));
                });

            userPrincipal.setUserId(user.getId());
            userPrincipal.setUsername(user.getUsername());
            userPrincipal.setPassword(user.getPassword());
            userPrincipal.setDeleted(user.isDeleted());
            userPrincipal.setAuthorities(authorities);
        }
        return userPrincipal;
    }

    @Override
    public boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    @Override
    public List<User> getUsersAdminHomeSeller() {
        List<User> users = new ArrayList<>();
        userRepository.getUsersAdminHomeSeller().forEach(users::add);
        return users;
    }
}