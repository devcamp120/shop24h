package com.devcamp.home24h.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.entity.ConstructionContractor;
import com.devcamp.home24h.model.Result;
import com.devcamp.home24h.service.ConstructionContractorService;

@CrossOrigin
@RestController
public class ConstructionContractorController {
    @Autowired
    private ConstructionContractorService constructionContractorService;

    @GetMapping("/construction-contractors/all")
    public ResponseEntity<List<ConstructionContractor>> getAllConstructionContractors() {
        try {
            List<ConstructionContractor> constructionContractors = constructionContractorService
                    .getAllConstructionContractors();
            return new ResponseEntity<>(constructionContractors, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/construction-contractors")
    public ResponseEntity<Result> getConstructionContractors(@RequestParam(defaultValue = "0") String page,
            @RequestParam(defaultValue = "10") String size) {
        try {
            Page<ConstructionContractor> constructionContractors = constructionContractorService
                    .getConstructionContractors(Integer.parseInt(page),
                            Integer.parseInt(size));
            return new ResponseEntity<>(
                    new Result(constructionContractors.getTotalPages(), constructionContractors.getContent()),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/construction-contractors/{id}")
    public ResponseEntity<ConstructionContractor> getConstructionContractorById(@PathVariable Long id) {
        try {
            ConstructionContractor constructionContractor = constructionContractorService
                    .getConstructionContractorById(id);
            if (constructionContractor != null) {
                return new ResponseEntity<>(constructionContractor, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(constructionContractor, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/construction-contractors")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> createConstructionContractor(@RequestParam Long addressId,
            @RequestBody ConstructionContractor pConstructionContractor) {
        try {
            ConstructionContractor constructionContractor = constructionContractorService
                    .createConstructionContractor(addressId, pConstructionContractor);
            return new ResponseEntity<>(constructionContractor, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified ConstructionContractor: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/construction-contractors/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateConstructionContractor(@PathVariable Long id, @RequestParam Long addressId,
            @RequestBody ConstructionContractor pConstructionContractor) {
        try {
            ConstructionContractor constructionContractor = constructionContractorService
                    .updateConstructionContractor(id, addressId, pConstructionContractor);
            if (constructionContractor != null) {
                return new ResponseEntity<>(constructionContractor, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(constructionContractor, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified ConstructionContractor: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/construction-contractors/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ConstructionContractor> deleteConstructionContractor(@PathVariable Long id) {
        try {
            boolean isDeleted = constructionContractorService.deleteConstructionContractor(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
