package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.entity.AddressMap;
import com.devcamp.home24h.entity.Investor;
import com.devcamp.home24h.entity.Project;
import com.devcamp.home24h.repository.InvestorRepository;

@Service
public class InvestorService {
    @Autowired
    private InvestorRepository investorRepository;

    @Autowired
    private ProjectService projectService;

    public List<Investor> getAllInvestors() {
        List<Investor> investors = new ArrayList<>();
        investorRepository.findAll().forEach(investors::add);
        return investors;
    }

    public Page<Investor> getInvestors(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Investor> investors = investorRepository.findAll(pageable);
        return investors;
    }

    public Investor getInvestorById(Long id) {
        Optional<Investor> optional = investorRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    public Investor createInvestor(Long addressId, Investor pInvestor) {
        pInvestor.setId(null);
        if (addressId == -1) {
            pInvestor.setAddress(null);
        } else {
            AddressMap addressMap = new AddressMap();
            addressMap.setId(addressId);
            pInvestor.setAddress(addressMap);
        }
        Investor investor = investorRepository.save(pInvestor);
        return investor;
    }

    public Investor updateInvestor(Long id, Long addressId, Investor pInvestor) {
        Optional<Investor> optional = investorRepository.findById(id);
        if (optional.isPresent()) {
            pInvestor.setId(id);
            if (addressId == -1) {
                pInvestor.setAddress(null);
            } else {
                AddressMap addressMap = new AddressMap();
                addressMap.setId(addressId);
                pInvestor.setAddress(addressMap);
            }
            return investorRepository.save(pInvestor);
        } else {
            return null;
        }
    }

    public boolean deleteInvestor(Long id) {
        Optional<Investor> optional = investorRepository.findById(id);
        if (optional.isPresent()) {
            Set<Project> projects = optional.get().getProjects();
            if (projects != null) {
                projects.forEach(project -> {
                    projectService.deleteProject(project.getId());
                });
            }
            investorRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
}
