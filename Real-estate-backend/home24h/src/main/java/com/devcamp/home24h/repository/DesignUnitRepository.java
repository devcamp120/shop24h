package com.devcamp.home24h.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.home24h.entity.DesignUnit;

@Repository
public interface DesignUnitRepository extends JpaRepository<DesignUnit, Long> {
    
}
