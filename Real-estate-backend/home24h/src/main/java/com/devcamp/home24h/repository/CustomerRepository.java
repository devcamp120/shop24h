package com.devcamp.home24h.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.devcamp.home24h.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query(value = "SELECT * FROM customers WHERE contact_name LIKE :keyword OR mobile LIKE :keyword", nativeQuery = true)
    public List<Customer> getCustomersByNameOrPhoneLike(String keyword);

    @Query(value = "SELECT * FROM customers WHERE created_by = :id", nativeQuery = true)
    public Customer findByCreatedBy(Long id);
}
