package com.devcamp.home24h.model;

public class UserInfo {
    private long id;
    private String username;
    private long role;

    public UserInfo() {
    }

    public UserInfo(long id, String username, long role) {
        this.id = id;
        this.username = username;
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getRole() {
        return role;
    }

    public void setRole(long role) {
        this.role = role;
    }
}
