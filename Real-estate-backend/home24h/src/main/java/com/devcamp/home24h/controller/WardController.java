package com.devcamp.home24h.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.entity.Ward;
import com.devcamp.home24h.model.Result;
import com.devcamp.home24h.service.WardService;

@CrossOrigin
@RestController
public class WardController {
    @Autowired
    private WardService wardService;

    @GetMapping("/wards")
    public ResponseEntity<Result> getWards(@RequestParam(defaultValue = "0") String page,
            @RequestParam(defaultValue = "10") String size) {
        try {
            Page<Ward> wards = wardService.getWards(Integer.parseInt(page),
                    Integer.parseInt(size));
            return new ResponseEntity<>(new Result(wards.getTotalPages(), wards.getContent()),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/wards/{id}")
    public ResponseEntity<Ward> getWardById(@PathVariable Long id) {
        try {
            Ward ward = wardService.getWardById(id);
            if (ward != null) {
                return new ResponseEntity<>(ward, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/wards")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> createWard(@RequestParam Long provinceId, @RequestParam Long districtId,
            @RequestBody Ward pWard) {
        try {
            Ward ward = wardService.createWard(provinceId, districtId, pWard);
            return new ResponseEntity<>(ward, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Ward: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/wards/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateWard(@PathVariable Long id, @RequestParam Long provinceId,
            @RequestParam Long districtId, @RequestBody Ward pWard) {
        try {
            Ward ward = wardService.updateWard(id, provinceId, districtId, pWard);
            if (ward != null) {
                return new ResponseEntity<>(ward, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(ward, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Ward: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/wards/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Ward> deleteWard(@PathVariable Long id) {
        try {
            boolean isDeleted = wardService.deleteWard(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
