package com.devcamp.home24h.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "customers")
@Getter
@Setter
public class Customer extends BaseEntity {

    @Column(name = "contact_name", length = 255)
    private String contactName;

    @Column(name = "contact_title", length = 30)
    private String contactTitle;

    @Column(name = "address", length = 200)
    private String address;

    @Column(name = "mobile", length = 80, unique = true)
    private String mobile;

    @Column(name = "email", length = 255, unique = true)
    private String email;

    @Column(name = "note", length = 5000)
    private String note;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
    @JsonIgnore
    private Set<Realestate> realestates;

    private Long createdBy;

	@Override
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
}
