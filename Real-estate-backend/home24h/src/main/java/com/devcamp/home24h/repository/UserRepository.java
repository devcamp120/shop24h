package com.devcamp.home24h.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.home24h.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

    boolean existsByUsername(String username);

    @Query(value = "SELECT * FROM t_user JOIN t_user_role ON t_user.id = t_user_role.user_id JOIN t_role ON t_user_role.role_id = t_role.id WHERE (t_role.role_key = 'ROLE_ADMIN' OR t_role.role_key = 'ROLE_HOME_SELLER')", nativeQuery = true)
    List<User> getUsersAdminHomeSeller();
}
