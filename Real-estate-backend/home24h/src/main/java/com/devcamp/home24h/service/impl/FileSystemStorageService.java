package com.devcamp.home24h.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;
import java.util.stream.Stream;

import org.apache.commons.io.FilenameUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.home24h.exception.StorageException;
import com.devcamp.home24h.exception.StorageFileNotFoundException;
import com.devcamp.home24h.service.StorageService;

@Service
public class FileSystemStorageService implements StorageService {
    private final Path rootLocation1 = Paths.get("upload-dir");
    private final String uploadPath = "upload-dir";

    @Override
    public String store(String folder, MultipartFile file) {
        try {
            Path path = Paths.get(uploadPath + folder);
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file.");
            }
            if (!Files.exists(path)) {
                Files.createDirectories(path);
            }
            String fileName = UUID.randomUUID().toString() + "."
                    + FilenameUtils.getExtension(file.getOriginalFilename());
            Path destinationFile = path.resolve(Paths.get(fileName)).normalize().toAbsolutePath();
            if (!destinationFile.getParent().equals(path.toAbsolutePath())) {
                // This is a security check
                throw new StorageException(
                        "Cannot store file outside current directory.");
            }
            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, destinationFile, StandardCopyOption.REPLACE_EXISTING);
                return fileName;
            }
        } catch (IOException e) {
            throw new StorageException("Failed to store file.", e);
        }
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.rootLocation1, 1)
                    .filter(path -> !path.equals(this.rootLocation1))
                    .map(this.rootLocation1::relativize);
        } catch (IOException e) {
            throw new StorageException("Failed to read stored files", e);
        }

    }

    @Override
    public Resource loadAsResource(String folder, String filename) {
        try {
            Path path = Paths.get(uploadPath + folder);
            Path file = path.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new StorageFileNotFoundException(
                        "Could not read file: " + filename);

            }
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    public void delete(String folder, String filename) {
        Path path = Paths.get(uploadPath + folder + filename);
        try {
            Files.delete(path);
        } catch (NoSuchFileException ex) {
            System.out.printf("No such file or directory: %s\n", path);
        } catch (DirectoryNotEmptyException ex) {
            System.out.printf("Directory %s is not empty\n", path);
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation1.toFile());
    }

    @Override
    public void init() {
        try {
            Files.createDirectories(rootLocation1);
        } catch (IOException e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }
}