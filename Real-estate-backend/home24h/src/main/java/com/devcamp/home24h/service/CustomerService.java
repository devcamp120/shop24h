package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.entity.Customer;
import com.devcamp.home24h.repository.CustomerRepository;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    public Page<Customer> getCustomers(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Customer> customers = customerRepository.findAll(pageable);
        return customers;
    }

    public Customer getCustomerById(Long id) {
        Optional<Customer> optional = customerRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    public List<Customer> getCustomersByNameOrPhoneLike(String keyword) {
        List<Customer> customers = new ArrayList<>();
        customerRepository.getCustomersByNameOrPhoneLike("%" + keyword + "%").forEach(customers::add);
        return customers;
    }

    public Customer createCustomer(Customer pCustomer) {
        pCustomer.setId(null);
        Customer customer = customerRepository.save(pCustomer);
        return customer;
    }

    public Customer updateCustomer(Long id, Customer pCustomer) {
        Optional<Customer> optional = customerRepository.findById(id);
        if (optional.isPresent()) {
            pCustomer.setId(id);
            return customerRepository.save(pCustomer);
        } else {
            return null;
        }
    }

    public boolean deleteCustomer(Long id) {
        Optional<Customer> optional = customerRepository.findById(id);
        if (optional.isPresent()) {
            customerRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
}
