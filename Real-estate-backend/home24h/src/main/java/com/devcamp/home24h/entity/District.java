package com.devcamp.home24h.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "district")
@Getter
@Setter
public class District {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "_name", length = 100)
    private String name;

    @Column(name = "_prefix", length = 20)
    private String prefix;

    @ManyToOne
    @JoinColumn(name = "_province_id")
    private Province province;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "district")
    @JsonIgnore
    private Set<Ward> wards;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "district")
    @JsonIgnore
    private Set<Street> streets;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "district")
    @JsonIgnore
    private Set<Realestate> realestates;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "district")
    @JsonIgnore
    private Set<Project> projects;

    public District() {
    }

    public District(Long id) {
        this.id = id;
    }
}
