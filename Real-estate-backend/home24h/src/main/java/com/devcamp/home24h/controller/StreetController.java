package com.devcamp.home24h.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.entity.Street;
import com.devcamp.home24h.model.Result;
import com.devcamp.home24h.service.StreetService;

@CrossOrigin
@RestController
public class StreetController {
    @Autowired
    private StreetService streetService;

    @GetMapping("/streets")
    public ResponseEntity<Result> getStreets(@RequestParam(defaultValue = "0") String page,
            @RequestParam(defaultValue = "10") String size) {
        try {
            Page<Street> streets = streetService.getStreets(Integer.parseInt(page),
                    Integer.parseInt(size));
            return new ResponseEntity<>(new Result(streets.getTotalPages(), streets.getContent()),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/streets/{id}")
    public ResponseEntity<Street> getStreetById(@PathVariable Long id) {
        try {
            Street street = streetService.getStreetById(id);
            if (street != null) {
                return new ResponseEntity<>(street, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/streets")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> createStreet(@RequestParam Long provinceId, @RequestParam Long districtId,
            @RequestBody Street pStreet) {
        try {
            Street street = streetService.createStreet(provinceId, districtId, pStreet);
            return new ResponseEntity<>(street, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Street: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/streets/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateStreet(@PathVariable Long id, @RequestParam Long provinceId,
            @RequestParam Long districtId, @RequestBody Street pStreet) {
        try {
            Street street = streetService.updateStreet(id, provinceId, districtId, pStreet);
            if (street != null) {
                return new ResponseEntity<>(street, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(street, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Street: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/streets/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Street> deleteStreet(@PathVariable Long id) {
        try {
            boolean isDeleted = streetService.deleteStreet(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
