package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.home24h.entity.Utility;
import com.devcamp.home24h.repository.UtilityRepository;

@Service
public class UtilityService {
    @Autowired
    private UtilityRepository utilityRepository;

    @Autowired
    private StorageService storageService;

    public List<Utility> getAllUtilitys() {
        List<Utility> utilitys = new ArrayList<>();
        utilityRepository.findAll().forEach(utilitys::add);
        return utilitys;
    }

    public Page<Utility> getUtilitys(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Utility> utilitys = utilityRepository.findAll(pageable);
        return utilitys;
    }

    public Utility getUtilityById(Long id) {
        Optional<Utility> optional = utilityRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    public Utility createUtility(Utility pUtility) {
        pUtility.setId(null);
        Utility utility = utilityRepository.save(pUtility);
        return utility;
    }

    public Utility updateUtility(Long id, Utility pUtility) {
        Optional<Utility> optional = utilityRepository.findById(id);
        if (optional.isPresent()) {
            pUtility.setId(id);
            return utilityRepository.save(pUtility);
        } else {
            return null;
        }
    }

    public boolean deleteUtility(Long id) {
        Optional<Utility> optional = utilityRepository.findById(id);
        if (optional.isPresent()) {
            utilityRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    public List<String> uploadUtilityPhoto(MultipartFile[] files) {
        List<String> fileNames = new ArrayList<>();
        String folder = "/utilities/";
        for (MultipartFile file : files) {
            String fileName = storageService.store(folder, file);
            fileNames.add(fileName);
        }
        return fileNames;
    }

    public void deleteUtilityPhoto(String filename, Long id) {
        Optional<Utility> optional = utilityRepository.findById(id);
        if (optional.isPresent()) {
            String photo = optional.get().getPhoto();
            if (photo.contains(filename)) {
                storageService.delete("/utilities/", filename);
            }
        }
    }
}
