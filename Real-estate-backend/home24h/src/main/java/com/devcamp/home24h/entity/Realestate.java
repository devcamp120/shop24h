package com.devcamp.home24h.entity;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "realestate")
@Getter
@Setter
public class Realestate extends BaseEntity {

    @Column(name = "title", length = 2000)
    private String title;

    @Column(name = "type")
    private Integer type;

    @Column(name = "request")
    private Integer request;

    @ManyToOne
    @JoinColumn(name = "province_id")
    private Province province;

    @ManyToOne
    @JoinColumn(name = "district_id")
    private District district;

    @ManyToOne
    @JoinColumn(name = "wards_id")
    private Ward ward;

    @ManyToOne
    @JoinColumn(name = "street_id")
    private Street street;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    @Column(name = "address", length = 2000, nullable = false)
    private String address;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @Column(name = "price")
    private Long price;

    @Column(name = "price_min")
    private Long priceMin;

    @Column(name = "price_time")
    private Integer priceTime;

    @Column(name = "acreage", precision = 12, scale = 2)
    private BigDecimal acreage;

    @Column(name = "direction")
    private Integer direction;

    @Column(name = "total_floors")
    private Integer totalFloors;

    @Column(name = "number_floors")
    private Integer numberFloors;

    @Column(name = "bath")
    private Integer bath;

    @Column(name = "apart_code")
    private String apartCode;

    @Column(name = "wall_area")
    private BigDecimal wallArea;

    @Column(name = "bedroom")
    private Integer bedroom;

    @Column(name = "balcony")
    private Integer balcony;

    @Column(name = "landscape_view", length = 255)
    private String landscapeView;

    @Column(name = "apart_loca")
    private Integer apartLoca;

    @Column(name = "apart_type")
    private Integer apartType;

    @Column(name = "furniture_type")
    private Integer furnitureType;

    @Column(name = "price_rent")
    private Integer priceRent;

    @Column(name = "return_rate")
    private Double returnRate;

    @Column(name = "legal_doc")
    private Double legalDoc;

    @Column(name = "description", length = 2000)
    private String description;

    @Column(name = "width_y")
    private Integer widthY;

    @Column(name = "long_x")
    private Integer longX;

    @Column(name = "street_house")
    private Boolean streetHouse;

    @Column(name = "fsbo")
    private Boolean fsbo;

    @Column(name = "view_num")
    private Integer viewNum;

    @Column(name = "shape", length = 200)
    private String shape;

    @Column(name = "distance2facade")
    private Integer distance2Facade;

    @Column(name = "adjacent_facade_num")
    private Integer adjacentFacadeNum;

    @Column(name = "adjacent_road")
    private String adjacentRoad;

    @Column(name = "alley_min_width")
    private Integer alleyMinWidth;

    @Column(name = "adjacent_alley_min_width")
    private Integer adjacentAlleyMinWidth;

    @Column(name = "factor")
    private Integer factor;

    @Column(name = "structure", length = 2000)
    private String structure;

    @Column(name = "dtsxd")
    private Integer dtsxd;

    @Column(name = "clcl")
    private Integer clcl;

    @Column(name = "ctxd_price")
    private Integer clxdPrice;

    @Column(name = "ctxd_value")
    private Integer ctxdValue;

    @Column(name = "photo", length = 2000)
    private String photo;

    @Column(name = "_lat")
    private Double lat;

    @Column(name = "_lng")
    private Double lng;
}
