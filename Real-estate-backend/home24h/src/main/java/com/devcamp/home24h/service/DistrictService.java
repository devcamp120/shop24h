package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.entity.District;
import com.devcamp.home24h.entity.Province;
import com.devcamp.home24h.entity.Street;
import com.devcamp.home24h.entity.Ward;
import com.devcamp.home24h.repository.DistrictRepository;

@Service
public class DistrictService {
    @Autowired
    private DistrictRepository districtRepository;

    public List<District> getAllDistricts() {
        List<District> districts = new ArrayList<>();
        districtRepository.findAll().forEach(districts::add);
        return districts;
    }

    public Page<District> getDistricts(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<District> districts = districtRepository.findAll(pageable);
        return districts;
    }

    public District getDistrictById(Long id) {
        Optional<District> optional = districtRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    public Set<Ward> getWardsByDistrictId(Long id) {
        Optional<District> optional = districtRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get().getWards();
        } else {
            return null;
        }
    }

    public Set<Street> getStreetsByDistrictId(Long id) {
        Optional<District> optional = districtRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get().getStreets();
        } else {
            return null;
        }
    }

    public District createDistrict(Long provinceId, District pDistrict) {
        pDistrict.setId(null);
        Province province = new Province(provinceId);
        pDistrict.setProvince(province);
        District district = districtRepository.save(pDistrict);
        return district;
    }

    public District updateDistrict(Long id, Long provinceId, District pDistrict) {
        Optional<District> optional = districtRepository.findById(id);
        if (optional.isPresent()) {
            pDistrict.setId(id);
            Province province = new Province(provinceId);
            pDistrict.setProvince(province);
            return districtRepository.save(pDistrict);
        } else {
            return null;
        }
    }

    public boolean deleteDistrict(Long id) {
        Optional<District> optional = districtRepository.findById(id);
        if (optional.isPresent()) {
            districtRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
}
