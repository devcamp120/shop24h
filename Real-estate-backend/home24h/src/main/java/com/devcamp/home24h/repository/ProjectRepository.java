package com.devcamp.home24h.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.home24h.entity.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
    @Transactional
    @Modifying
    @Query(value = "UPDATE project SET construction_contractor = NULL WHERE construction_contractor = :id", nativeQuery = true)
    void updateByConstructionContractorId(Long id);

    @Transactional
    @Modifying
    @Query(value = "UPDATE project SET design_unit = NULL WHERE design_unit = :id", nativeQuery = true)
    void updateByDesignUnitId(Long id);
}
