package com.devcamp.home24h.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.entity.District;
import com.devcamp.home24h.entity.Province;
import com.devcamp.home24h.entity.Street;
import com.devcamp.home24h.repository.StreetRepository;

@Service
public class StreetService {
    @Autowired
    private StreetRepository streetRepository;

    public Page<Street> getStreets(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Street> streets = streetRepository.findAll(pageable);
        return streets;
    }

    public Street getStreetById(Long id) {
        Optional<Street> optional = streetRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    public Street createStreet(Long provinceId, Long districtId, Street pStreet) {
        pStreet.setId(null);
        Province province = new Province(provinceId);
        District district = new District(districtId);
        pStreet.setProvince(province);
        pStreet.setDistrict(district);
        Street street = streetRepository.save(pStreet);
        return street;
    }

    public Street updateStreet(Long id, Long provinceId, Long districtId, Street pStreet) {
        Optional<Street> optional = streetRepository.findById(id);
        if (optional.isPresent()) {
            pStreet.setId(id);
            Province province = new Province(provinceId);
            District district = new District(districtId);
            pStreet.setProvince(province);
            pStreet.setDistrict(district);
            return streetRepository.save(pStreet);
        } else {
            return null;
        }
    }

    public boolean deleteStreet(Long id) {
        Optional<Street> optional = streetRepository.findById(id);
        if (optional.isPresent()) {
            streetRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
}
