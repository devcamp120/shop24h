package com.devcamp.home24h.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.entity.Investor;
import com.devcamp.home24h.model.Result;
import com.devcamp.home24h.service.InvestorService;

@CrossOrigin
@RestController
public class InvestorController {
    @Autowired
    private InvestorService investorService;

    @GetMapping("/investors/all")
    public ResponseEntity<List<Investor>> getAllInvestors() {
        try {
            List<Investor> investors = investorService.getAllInvestors();
            return new ResponseEntity<>(investors, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/investors")
    public ResponseEntity<Result> getInvestors(@RequestParam(defaultValue = "0") String page,
            @RequestParam(defaultValue = "10") String size) {
        try {
            Page<Investor> investors = investorService.getInvestors(Integer.parseInt(page),
                    Integer.parseInt(size));
            return new ResponseEntity<>(new Result(investors.getTotalPages(), investors.getContent()),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/investors/{id}")
    public ResponseEntity<Investor> getInvestorById(@PathVariable Long id) {
        try {
            Investor investor = investorService.getInvestorById(id);
            if (investor != null) {
                return new ResponseEntity<>(investor, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(investor, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/investors")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> createInvestor(@RequestParam Long addressId, @RequestBody Investor pInvestor) {
        try {
            Investor investor = investorService.createInvestor(addressId, pInvestor);
            return new ResponseEntity<>(investor, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Investor: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/investors/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateInvestor(@PathVariable Long id, @RequestParam Long addressId,
            @RequestBody Investor pInvestor) {
        try {
            Investor investor = investorService.updateInvestor(id, addressId, pInvestor);
            if (investor != null) {
                return new ResponseEntity<>(investor, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(investor, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Investor: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/investors/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Investor> deleteInvestor(@PathVariable Long id) {
        try {
            boolean isDeleted = investorService.deleteInvestor(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
