package com.devcamp.home24h.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.entity.District;
import com.devcamp.home24h.entity.Street;
import com.devcamp.home24h.entity.Ward;
import com.devcamp.home24h.model.Result;
import com.devcamp.home24h.service.DistrictService;

@CrossOrigin
@RestController
public class DistrictController {
    @Autowired
    private DistrictService districtService;

    @GetMapping("/districts/all")
    public ResponseEntity<List<District>> getAllDistricts() {
        try {
            List<District> districts = districtService.getAllDistricts();
            return new ResponseEntity<>(districts, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts")
    public ResponseEntity<Result> getDistricts(@RequestParam(defaultValue = "0") String page,
            @RequestParam(defaultValue = "10") String size) {
        try {
            Page<District> districts = districtService.getDistricts(Integer.parseInt(page),
                    Integer.parseInt(size));
            return new ResponseEntity<>(new Result(districts.getTotalPages(), districts.getContent()),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts/{id}")
    public ResponseEntity<District> getDistrictById(@PathVariable Long id) {
        try {
            District district = districtService.getDistrictById(id);
            if (district != null) {
                return new ResponseEntity<>(district, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts/{id}/wards")
    public ResponseEntity<Set<Ward>> getWardssByDistrictId(@PathVariable Long id) {
        try {
            Set<Ward> wards = districtService.getWardsByDistrictId(id);
            if (wards != null) {
                return new ResponseEntity<>(wards, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts/{id}/streets")
    public ResponseEntity<Set<Street>> getStreetsByDistrictId(@PathVariable Long id) {
        try {
            Set<Street> streets = districtService.getStreetsByDistrictId(id);
            if (streets != null) {
                return new ResponseEntity<>(streets, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/districts")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> createDistrict(@RequestParam Long provinceId, @RequestBody District pDistrict) {
        try {
            District district = districtService.createDistrict(provinceId, pDistrict);
            return new ResponseEntity<>(district, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified District: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/districts/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateDistrict(@PathVariable Long id, @RequestParam Long provinceId,
            @RequestBody District pDistrict) {
        try {
            District district = districtService.updateDistrict(id, provinceId, pDistrict);
            if (district != null) {
                return new ResponseEntity<>(district, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(district, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified District: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/districts/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<District> deleteDistrict(@PathVariable Long id) {
        try {
            boolean isDeleted = districtService.deleteDistrict(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
