package com.devcamp.home24h.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.home24h.entity.MasterLayout;

@Repository
public interface MasterLayoutRepository extends JpaRepository<MasterLayout, Long> {
    @Transactional
    @Modifying
    @Query(value = "DELETE FROM master_layout WHERE project_id = :id", nativeQuery = true)
    void deleteByProjectId(Long id);
}
