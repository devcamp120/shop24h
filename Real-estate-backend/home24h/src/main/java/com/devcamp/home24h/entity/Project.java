package com.devcamp.home24h.entity;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "project")
@Getter
@Setter
public class Project {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(columnDefinition = "INT(11)")
        private Long id;

        @Column(name = "_name", length = 200)
        private String name;

        @ManyToOne
        @JoinColumn(name = "_province_id")
        private Province province;

        @ManyToOne
        @JoinColumn(name = "_district_id")
        private District district;

        @ManyToOne
        @JoinColumn(name = "_ward_id")
        private Ward ward;

        @ManyToOne
        @JoinColumn(name = "_street_id")
        private Street street;

        @Column(name = "address", length = 1000)
        private String address;

        @Column(name = "slogan")
        private String slogan;

        @Column(name = "description")
        private String description;

        @Column(name = "acreage", precision = 20, scale = 2)
        private BigDecimal acreage;

        @Column(name = "construct_area", precision = 20, scale = 2)
        private BigDecimal constructArea;

        @Column(name = "num_block")
        private Integer numBlock;

        @Column(name = "num_floors", length = 500)
        private String numFloors;

        @Column(name = "num_apartment")
        private Integer numApartment;

        @Column(name = "apartmentt_area", length = 500)
        private String apartmenttArea;

        @ManyToOne
        @JoinColumn(name = "investor", nullable = false)
        private Investor investor;

        @OneToMany(cascade = CascadeType.ALL, mappedBy = "project")
        @JsonIgnore
        private Set<MasterLayout> masterLayouts;

        @ManyToOne
        @JoinColumn(name = "construction_contractor")
        private ConstructionContractor constructionContractor;

        @ManyToOne
        @JoinColumn(name = "design_unit")
        private DesignUnit designUnit;

        @Column(name = "utilities", length = 1000)
        private String utilities;

        @Column(name = "region_link", length = 1000)
        private String regionLink;

        @Column(name = "photo", length = 5000)
        private String photo;

        @Column(name = "_lat")
        private Double lat;

        @Column(name = "_lng")
        private Double lng;

        @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "project")
        @JsonIgnore
        private Set<Realestate> realestates;

        public Project() {
        }

        public Project(Long id) {
                this.id = id;
        }
}
