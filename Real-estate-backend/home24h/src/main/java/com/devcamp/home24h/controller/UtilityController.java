package com.devcamp.home24h.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.home24h.entity.Utility;
import com.devcamp.home24h.model.Result;
import com.devcamp.home24h.service.StorageService;
import com.devcamp.home24h.service.UtilityService;

@CrossOrigin
@RestController
public class UtilityController {
    @Autowired
    private UtilityService utilityService;

    @Autowired
    private StorageService storageService;

    @GetMapping("/utilities/all")
    public ResponseEntity<List<Utility>> getAllUtilitys() {
        try {
            List<Utility> utilities = utilityService.getAllUtilitys();
            return new ResponseEntity<>(utilities, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/utilities")
    public ResponseEntity<Result> getUtilitys(@RequestParam(defaultValue = "0") String page,
            @RequestParam(defaultValue = "10") String size) {
        try {
            Page<Utility> utilities = utilityService.getUtilitys(Integer.parseInt(page),
                    Integer.parseInt(size));
            return new ResponseEntity<>(new Result(utilities.getTotalPages(), utilities.getContent()),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/utilities/{id}")
    public ResponseEntity<Utility> getUtilityById(@PathVariable Long id) {
        try {
            Utility utility = utilityService.getUtilityById(id);
            if (utility != null) {
                return new ResponseEntity<>(utility, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(utility, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/utilities")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> createUtility(@RequestBody Utility pUtility) {
        try {
            Utility utility = utilityService.createUtility(pUtility);
            return new ResponseEntity<>(utility, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Utility: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/utilities/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateUtility(@PathVariable Long id, @RequestBody Utility pUtility) {
        try {
            Utility utility = utilityService.updateUtility(id, pUtility);
            if (utility != null) {
                return new ResponseEntity<>(utility, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(utility, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Utility: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/utilities/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Utility> deleteUtility(@PathVariable Long id) {
        try {
            boolean isDeleted = utilityService.deleteUtility(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/upload/utility-photo")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<String>> uploadUtilityImages(@RequestParam("files") MultipartFile[] files) {
        try {
            List<String> fileNames = utilityService.uploadUtilityPhoto(files);
            return new ResponseEntity<>(fileNames, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/download/utility-photo/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> downloadUtilityPhoto(@PathVariable String filename) {
        String folder = "/utilities/";
        Resource file = storageService.loadAsResource(folder, filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @DeleteMapping("/delete/utility-photo/{filename:.+}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteUtilityPhoto(@PathVariable String filename, @RequestParam Long id) {
        try {
            utilityService.deleteUtilityPhoto(filename, id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
