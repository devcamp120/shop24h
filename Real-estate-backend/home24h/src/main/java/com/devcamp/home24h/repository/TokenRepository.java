package com.devcamp.home24h.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.home24h.entity.Token;

public interface TokenRepository extends JpaRepository<Token, Long> {

    Token findByToken(String token);

    @Transactional
    @Modifying
    @Query(value = "UPDATE t_token SET deleted = 1 WHERE created_by = :userId", nativeQuery = true)
    void deleteTokenByCreatedBy(Long userId);
}
