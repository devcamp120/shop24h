package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.entity.AddressMap;
import com.devcamp.home24h.repository.AddressMapRepository;

@Service
public class AddressMapService {
    @Autowired
    private AddressMapRepository addressMapRepository;

    public List<AddressMap> getAllAddressMaps() {
        List<AddressMap> addressMaps = new ArrayList<>();
        addressMapRepository.findAll().forEach(addressMaps::add);
        return addressMaps;
    }

    public Page<AddressMap> getAddressMaps(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<AddressMap> addressMaps = addressMapRepository.findAll(pageable);
        return addressMaps;
    }

    public AddressMap getAddressMapById(Long id) {
        Optional<AddressMap> optional = addressMapRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    public AddressMap createAddressMap(AddressMap pAddressMap) {
        pAddressMap.setId(null);
        AddressMap addressMap = addressMapRepository.save(pAddressMap);
        return addressMap;
    }

    public AddressMap updateAddressMap(Long id, AddressMap pAddressMap) {
        Optional<AddressMap> optional = addressMapRepository.findById(id);
        if (optional.isPresent()) {
            pAddressMap.setId(id);
            return addressMapRepository.save(pAddressMap);
        } else {
            return null;
        }
    }

    public boolean deleteAddressMap(Long id) {
        Optional<AddressMap> optional = addressMapRepository.findById(id);
        if (optional.isPresent()) {
            addressMapRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
}
